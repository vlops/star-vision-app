import 'package:app_shoe_shop/datas/product_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'product_detail.dart';

class ProductsScreen extends StatelessWidget {

  final QuerySnapshot data;

  ProductsScreen(this.data);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          SizedBox(height: 15.0),
          Container(
              padding: EdgeInsets.only(right: 15.0),
              width: MediaQuery.of(context).size.width - 30.0,
              height: MediaQuery.of(context).size.height - 50.0,
              child: GridView.count(
                crossAxisCount: 2,
                primary: false,
                crossAxisSpacing: 10.0,
                mainAxisSpacing: 15.0,
                childAspectRatio: 0.8,
                children: data.documents.map((e){
                    return _buildCard(e["title"], 'R\$ ${e["price"]}',
                        e["images"], e["model"], e["color"], e["description"], e["material"], e["category"], false, false, context);
                  }).toList(),
              )),
          SizedBox(height: 15.0)
        ],
      ),
    );
  }

  Widget _buildCard(String name, String price, List<dynamic> imgPath, String model, String color, String description,
      String material, String category, bool added, bool isFavorite, context) {
    return Padding(
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0, left: 5.0, right: 5.0),
        child: InkWell(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ProductDetail(
                      productImages: imgPath,
                      productPrice: price,
                      productName: name,
                      productModel: model,
                      productColor: color,
                      productDescription: description,
                      productMaterial: material,
                      productCategory: category,
                  )));
            },
            child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 3.0,
                          blurRadius: 5.0)
                    ],
                    color: Colors.white),
                child: Column(children: [
                  Padding(
                      padding: EdgeInsets.all(5.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            isFavorite
                                ? Icon(Icons.favorite, color: Color(0xFF73AEF5))
                                : Icon(Icons.favorite_border,
                                    color: Color(0xFF73AEF5))
                          ])),
                  Hero(
                      tag: "imgPath${DateTime.now()}",
                      child: Container(
                          height: 75.0,
                          width: 75.0,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: NetworkImage(imgPath[0]),
                                  fit: BoxFit.contain)))),
                  SizedBox(height: 7.0),
                  Text(price,
                      style: TextStyle(
                          color: Color(0xFF73AEF5),
                          fontFamily: 'Varela',
                          fontSize: 14.0)),
                  Text(name,
                      style: TextStyle(
                          color: Color(0xFF575E67),
                          fontFamily: 'Varela',
                          fontSize: 14.0)),
                  Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Container(color: Color(0xFFEBEBEB), height: 1.0)),
                  Padding(
                      padding: EdgeInsets.only(left: 5.0, right: 5.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                              Icon(Icons.shopping_basket,
                                  color: Color(0xFF73AEF5), size: 12.0),
                              Text('Adicionar ao carrinho',
                                  style: TextStyle(
                                      fontFamily: 'Varela',
                                      color: Color(0xFF73AEF5),
                                      fontSize: 12.0))
                            ],
                          ))
                ]))));
  }
}
