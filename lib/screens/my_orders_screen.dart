import 'package:flutter/material.dart';

import '../widgets/bloc.navigation_bloc/navigation_bloc.dart';

class MyOrdersScreen extends StatelessWidget with NavigationStates {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        "Meus Pedidos",
        style: TextStyle(fontWeight: FontWeight.w900, fontSize: 28),
      ),
    );
  }
}
