import 'dart:io';

import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/lens_suggestion.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/process_suggestion.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:app_shoe_shop/components/custom_surfix_icon.dart';
import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/components/form_error.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

import '../../../../size_config.dart';

//import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';

// ignore: must_be_immutable
class VirtualRecipeForm extends StatefulWidget {
  int clientType;
  String urlSelfie;
  Product product;

  VirtualRecipeForm(this.clientType, this.urlSelfie, this.product);
  @override
  _VirtualRecipeFormState createState() => _VirtualRecipeFormState();
}

class _VirtualRecipeFormState extends State<VirtualRecipeForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];

  String urlRecipe;
  String urlDocument;
  int recipe = 0;
  int doc = 0;

  double _radioEsfericValue = 1;
  int _radioValue = 0;
  double _leftSliderValue = 0;
  double _rightSliderValue = 0;
  double _leftSliderCilValue = 0;
  double _rightSliderCilValue = 0;
  double _axisLeftSliderValue = 0;
  double _axisRightSliderValue = 0;
  double _additionSliderValue = 0;

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            buildTextRecipe("Esférico"),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildVirtualRecipe(),
            buildRadioEsferic(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildTextRecipe("Cilindríco"),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildSliderCilRecipe(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildTextEixo(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildEixo(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildTextAdicao(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildSliderAdicaoRecipe(),
            SizedBox(height: getProportionateScreenHeight(10)),
            DefaultButton(
              text: "Continuar",
              press: () {
                // Navigator.pushNamed(context, LensSuggestionScreen.routeName,
                //     arguments: LensSuggestionArguments(
                //       product: widget.product,
                //       urlArgs: 'teste',

                //     ));
                ProcessSuggestion processSuggestion = ProcessSuggestion(
                    _leftSliderValue,
                    _rightSliderValue,
                    _leftSliderCilValue,
                    _rightSliderCilValue,
                    _axisLeftSliderValue,
                    _axisRightSliderValue,
                    _additionSliderValue,
                    _radioEsfericValue,
                    context,
                    widget.product);
                processSuggestion.show();
              },
            ),
            SizedBox(height: getProportionateScreenHeight(20)),
          ],
        ));
  }

  Widget buildCheckbox() {
    return CheckboxListTile(
      title: const Text('Sua receita possui adição?'),
      value: timeDilation != 1.0,
      onChanged: (bool value) {
        setState(() {
          timeDilation = value ? 5.0 : 1.0;
        });
      },
      // secondary: const Icon(Icons.hourglass_empty),
    );
  }

  Widget buildVirtualRecipe() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.D"),
            ),
            Expanded(
              flex: 6,
              child: Slider(
                activeColor: kPrimaryColor,
                value: _rightSliderValue,
                min: -15,
                max: 8,
                divisions: 100,
                label: _rightSliderValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _rightSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(_rightSliderValue.toStringAsFixed(2)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.E"),
            ),
            Expanded(
              flex: 6,
              child: Slider(
                activeColor: kPrimaryColor,
                value: _leftSliderValue,
                min: -15,
                max: 8,
                divisions: 100,
                label: _leftSliderValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _leftSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(_leftSliderValue.toStringAsFixed(2)),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildRadioEsferic() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        new Text('Perto', style: TextStyle(fontSize: 12)),
        new Radio(
          activeColor: kPrimaryColor,
          value: 0,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),
        new Text(
          'Longe',
          style: TextStyle(fontSize: 12),
        ),
        new Radio(
          activeColor: kPrimaryColor,
          value: 1,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),
      ],
    );
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _radioEsfericValue = 1;
          break;
        case 1:
          _radioEsfericValue = 2;
          break;
      }
    });
  }

  Widget buildSliderCilRecipe() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.D"),
            ),
            Expanded(
              flex: 6,
              child: Slider(
                activeColor: kPrimaryColor,
                value: _rightSliderCilValue,
                min: -15,
                max: 8,
                divisions: 100,
                label: _rightSliderCilValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _rightSliderCilValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(_rightSliderCilValue.toStringAsFixed(2)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.E"),
            ),
            Expanded(
              flex: 6,
              child: Slider(
                activeColor: kPrimaryColor,
                value: _leftSliderCilValue,
                min: -15,
                max: 10,
                divisions: 100,
                label: _leftSliderCilValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _leftSliderCilValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(_leftSliderCilValue.toStringAsFixed(2)),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildSliderAdicaoRecipe() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("Adição"),
            ),
            Expanded(
              flex: 6,
              child: Slider(
                activeColor: kPrimaryColor,
                value: _additionSliderValue,
                min: 0,
                max: 10,
                divisions: 50,
                label: _additionSliderValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _additionSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Text("+ ${_additionSliderValue.toStringAsFixed(2)}"),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildEixo() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.D"),
            ),
            Expanded(
              flex: 6,
              child: Slider(
                activeColor: kPrimaryColor,
                value: _axisRightSliderValue,
                min: 0,
                max: 360,
                divisions: 360,
                label: _axisRightSliderValue.toStringAsFixed(0),
                onChanged: (double value) {
                  setState(() {
                    _axisRightSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(_axisRightSliderValue.toStringAsFixed(0)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.E"),
            ),
            Expanded(
              flex: 6,
              child: Slider(
                activeColor: kPrimaryColor,
                value: _axisLeftSliderValue,
                min: 0,
                max: 360,
                divisions: 360,
                label: _axisLeftSliderValue.toStringAsFixed(0),
                onChanged: (double value) {
                  setState(() {
                    _axisLeftSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(_axisLeftSliderValue.toStringAsFixed(0)),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildTextRecipe(String type) {
    return Row(
      children: [
        Text("Selecione o grau (${type})"),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
      ],
    );
  }

  Widget buildTextEixo() {
    return Row(
      children: [
        Text("Selecione o valor do Eixo"),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
      ],
    );
  }

  Widget buildTextAdicao() {
    return Row(
      children: [
        Text("Selecione o valor da Adição"),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
      ],
    );
  }
}
