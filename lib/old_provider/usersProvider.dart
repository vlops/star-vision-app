// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';

// class UserOptic {
//   String id;
//   String userId;
//   int opticType;
//   String cpfCpnj;
//   String endereco;
//   int telefone;
//   String email;

//   UserOptic(this.opticType, this.cpfCpnj, this.endereco, this.telefone, this.email){
//     FirebaseAuth auth = FirebaseAuth.instance;
//     if (auth.currentUser != null) {
//       this.userId = auth.currentUser.uid;
//     }
//   }

//   UserOptic.FromMap(Map<String, dynamic> map) {
//     this.opticType = map["opticType"];
//     this.cpfCpnj = map["cpfCpnj"];
//     this.endereco = map["endereco"];
//     this.telefone = map["telefone"];
//     this.email = map["email"];
//   }

//   Map<String, dynamic> toMap(){
//     return{
//       "uid" : userId,
//       "opticType" : opticType,
//       "cpfCpnj" : cpfCpnj,
//       "endereco" : endereco,
//       "telefone" : telefone,
//       "email" : email
//     };
//   }
// }

// class UserProvider{
//   CollectionReference users = FirebaseFirestore.instance.collection('users');

//   Future<void> addUser(UserOptic user) {
//     return users.add(
//         user.toMap()
//     ).then(
//             (value) => print("Usuario adicionado"))
//         .catchError(
//             (error) => print("Falha ao adicionar usuario. Erro: $error")
//     );
//   }

//   Future<void> updateUser(UserOptic user){
//     users
//         .doc(user.id)
//         .update(user.toMap())
//         .then((value) => print("Usuarios atualizados"))
//         .catchError((error) => print("Falha ao atualizar usuario. Erro: $error"));
//   }

//   Future<UserOptic> getUser(String userId) async {
//     UserOptic user;

//     await users.where("uid", isEqualTo: userId).
//       get().then((value) => {
//         value.docs.forEach((element) {
//            user =  UserOptic(
//               element["opticType"],
//               element["cpfCpnj"],
//               element["endereco"],
//               element["telefone"],
//               element["email"]
//           );
//            user.id = element.id;
//         })
//     });

//     if (user == null){
//       user = UserOptic(1, "", "", 0, "");
//       user.userId = userId;
//       await addUser(user);
//       user = null;
//       await users.where("uid", isEqualTo: userId).
//       get().then((value) => {
//         value.docs.forEach((element) {
//           user =  UserOptic(
//               element["opticType"],
//               element["cpfCpnj"],
//               element["endereco"],
//               element["telefone"],
//               element["email"]
//           );
//           user.id = element.id;
//         })
//       });
//     }

//     user.userId = userId;
//     return user;
//   }
// }
