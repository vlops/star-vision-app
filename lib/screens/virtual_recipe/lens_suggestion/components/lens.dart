import 'package:flutter/material.dart';

class Lens {
  String cardTitle;
  bool recommended;
  double value;

  Lens(this.cardTitle, this.recommended, this.value);
}
