import 'dart:io';

import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:app_shoe_shop/components/custom_surfix_icon.dart';
import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/components/form_error.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

import '../../../../size_config.dart';

//import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';

// ignore: must_be_immutable
class LensTreatmentForm extends StatefulWidget {
  int clientType;
  String urlSelfie;
  Product product;

  LensTreatmentForm(this.clientType, this.urlSelfie, this.product);
  @override
  _LensTreatmentFormState createState() => _LensTreatmentFormState();
}

class _LensTreatmentFormState extends State<LensTreatmentForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];

  String urlRecipe;
  String urlDocument;
  int recipe = 0;
  int doc = 0;

  double _result = 1;
  int _radioValue = 0;
  double _leftSliderValue = 0;
  double _rightSliderValue = 0;
  double _leftSliderCilValue = 0;
  double _rightSliderCilValue = 0;
  double _axisLeftSliderValue = 0;
  double _axisRightSliderValue = 0;
  double _additionSliderValue = 0;

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: [
            buildTextRecipe("Esférico"),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildVirtualRecipe(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildTextRecipe("Cilindríco"),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildSliderCilRecipe(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildTextEixo(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildEixo(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildTextAdicao(),
            SizedBox(height: getProportionateScreenHeight(10)),
            buildSliderAdicaoRecipe(),
            SizedBox(height: getProportionateScreenHeight(10)),
          ],
        ));
  }

  // Widget _statePermissionVirtualRecipe() {
  //   if (widget.urlSelfie == "empty") {
  //     return Column(
  //       children: [
  //         SizedBox(height: SizeConfig.screenHeight * 0.01),
  //         _stateSelfie(),
  //         SizedBox(height: getProportionateScreenHeight(30)),
  //         _stateDocument(),
  //         FormError(errors: errors),
  //         SizedBox(height: getProportionateScreenHeight(40)),
  //         DefaultButton(
  //           text: "Continuar",
  //           press: () {
  //             if (_formKey.currentState.validate()) {
  //               _formKey.currentState.save();
  //               Navigator.pushNamed(context, CartScreen.routeName,
  //                   arguments: CheckoutArguments(
  //                       product: widget.product,
  //                       urlDocument: urlDocument,
  //                       urlSelfie: widget.urlSelfie,
  //                       urlRecipe: urlRecipe));
  //             }
  //           },
  //         ),
  //       ],
  //     );
  //   } else {
  //     return Column(
  //       children: [
  //         SizedBox(height: SizeConfig.screenHeight * 0.01),
  //         _stateSelfie(),
  //         SizedBox(height: getProportionateScreenHeight(30)),

  //         _stateRecipe(),
  //         SizedBox(height: getProportionateScreenHeight(30)),

  //         Row(
  //           children: [
  //             Text("Selecione o tipo de Receita"),
  //             SizedBox(
  //               width: 10.0,
  //             ),
  //             Expanded(
  //                 child: Divider(
  //               color: Colors.black,
  //             )),
  //           ],
  //         ),
  //         SizedBox(height: getProportionateScreenHeight(10)),
  //         buildRadioRecipe(),
  //         SizedBox(height: getProportionateScreenHeight(30)),
  //         _stateTextVirtualRecipe(),
  //         //SizedBox(height: getProportionateScreenHeight(30)),
  //         //_stateDocument(),
  //         FormError(errors: errors),
  //         SizedBox(height: getProportionateScreenHeight(40)),
  //         DefaultButton(
  //           text: "Continuar",
  //           press: () {
  //             if (_formKey.currentState.validate()) {
  //               _formKey.currentState.save();
  //               Navigator.pushNamed(context, CartScreen.routeName,
  //                   arguments: CheckoutArguments(
  //                       product: widget.product,
  //                       urlDocument: urlDocument,
  //                       urlSelfie: widget.urlSelfie,
  //                       urlRecipe: urlRecipe));
  //             }
  //           },
  //         ),
  //       ],
  //     );
  //   }
  // }
  Widget buildCheckbox() {
    return CheckboxListTile(
      title: const Text('Sua receita possui adição?'),
      value: timeDilation != 1.0,
      onChanged: (bool value) {
        setState(() {
          timeDilation = value ? 5.0 : 1.0;
        });
      },
      // secondary: const Icon(Icons.hourglass_empty),
    );
  }

  Widget buildVirtualRecipe() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.D"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _rightSliderValue,
                min: -10,
                max: 10,
                divisions: 100,
                label: _rightSliderValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _rightSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(_rightSliderValue.toStringAsFixed(2)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.E"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _leftSliderValue,
                min: -10,
                max: 10,
                divisions: 100,
                label: _leftSliderValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _leftSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(_leftSliderValue.toStringAsFixed(2)),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildSliderCilRecipe() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.D"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _rightSliderCilValue,
                min: -10,
                max: 10,
                divisions: 100,
                label: _rightSliderCilValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _rightSliderCilValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(_rightSliderCilValue.toStringAsFixed(2)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.E"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _leftSliderCilValue,
                min: -10,
                max: 10,
                divisions: 100,
                label: _leftSliderCilValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _leftSliderCilValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(_leftSliderCilValue.toStringAsFixed(2)),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildSliderAdicaoRecipe() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("Adição"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _additionSliderValue,
                min: 0,
                max: 10,
                divisions: 50,
                label: _additionSliderValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _additionSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 2,
              child: Text("+ ${_additionSliderValue.toStringAsFixed(2)}"),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildEixo() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.D"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _axisRightSliderValue,
                min: 0,
                max: 360,
                divisions: 360,
                label: _axisRightSliderValue.toStringAsFixed(0),
                onChanged: (double value) {
                  setState(() {
                    _axisRightSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(_axisRightSliderValue.toStringAsFixed(0)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("O.E"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _axisLeftSliderValue,
                min: 0,
                max: 360,
                divisions: 360,
                label: _axisLeftSliderValue.toStringAsFixed(0),
                onChanged: (double value) {
                  setState(() {
                    _axisLeftSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(_axisLeftSliderValue.toStringAsFixed(0)),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildTextRecipe(String type) {
    return Row(
      children: [
        Text("Selecione o grau (${type})"),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
      ],
    );
  }

  Widget buildTextEixo() {
    return Row(
      children: [
        Text("Selecione o valor do Eixo"),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
      ],
    );
  }

  Widget buildTextAdicao() {
    return Row(
      children: [
        Text("Selecione o valor da Adição"),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
      ],
    );
  }
}
