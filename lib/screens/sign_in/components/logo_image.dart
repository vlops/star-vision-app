import 'package:flutter/material.dart';
import 'package:app_shoe_shop/models/Product.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class LogoImage extends StatefulWidget {
  const LogoImage({
    Key key,
  }) : super(key: key);

  @override
  _LogoImageState createState() => _LogoImageState();
}

class _LogoImageState extends State<LogoImage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
            child: SizedBox(
          width: getProportionateScreenWidth(248),
          child: AspectRatio(
            aspectRatio: 1,
            child: Hero(
              tag: "${DateTime.now()}",
              child: Image.asset(
                "assets/images/logo_star.png",
              ),
            ),
          ),
        ))
      ],
    );
  }
}
