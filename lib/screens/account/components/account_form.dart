import 'package:app_shoe_shop/components/custom_surfix_icon.dart';
import 'package:app_shoe_shop/components/form_error.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/forgot_password/forgot_password_screen.dart';
import 'package:app_shoe_shop/screens/home/home_screen2.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../components/default_button.dart';
import '../../../constants.dart';
import '../../../size_config.dart';

class AccountForm extends StatefulWidget {
  @override
  _AccountForm createState() => _AccountForm();
}

class _AccountForm extends State<AccountForm> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _cpfController = TextEditingController();
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  Widget buildTextField(
      {@required String labelText,
      @required String placeholder,
      @required TextEditingController controller,
      @required bool isPhone,
      @required bool isEmail}) {
    return TextFormField(
      controller: controller,
      enabled: isEmail ? false : true,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: "Preencha todos os campos");
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: labelText,
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          //suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg"),
          suffixIcon: isPhone
              ? CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg")
              : isEmail
                  ? CustomSurffixIcon(svgIcon: "assets/icons/Mail.svg")
                  : CustomSurffixIcon(svgIcon: "assets/icons/User.svg")),
      inputFormatters: isPhone
          ? [
              MaskTextInputFormatter(
                  mask: "(##)#-####-####", filter: {"#": RegExp(r'[0-9]')})
            ]
          : [],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ScopedModelDescendant<UserModel>(builder: (context, child, model) {
        _nameController.text = model.userData["name"];
        _emailController.text = model.userData["email"];
        _phoneController.text = model.userData["phone"];
        _cpfController.text = model.userData["cpf"];

        model.loadCurrentUser();

        if (model.isLoading)
          return Center(
            child: CircularProgressIndicator(),
          );

        return Form(
          key: _formKey,
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20),
                  vertical: 10,
                ),
                child: buildTextField(
                    labelText: "Nome",
                    placeholder: model.userData["name"] != null
                        ? model.userData["name"]
                        : "",
                    controller: _nameController,
                    isPhone: false,
                    isEmail: false),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20),
                  vertical: 10,
                ),
                child: buildTextField(
                    labelText: "Email",
                    placeholder: model.userData["email"] != null
                        ? model.userData["email"]
                        : "",
                    controller: _emailController,
                    isPhone: false,
                    isEmail: true),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20),
                  vertical: 10,
                ),
                child: buildTextField(
                    labelText: "Celular",
                    placeholder: model.userData["phone"] != null
                        ? model.userData["phone"]
                        : "",
                    controller: _phoneController,
                    isPhone: true,
                    isEmail: false),
              ),
              FormError(errors: errors),
              SizedBox(height: getProportionateScreenHeight(20)),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: DefaultButton(
                  text: "Salvar Alterações",
                  press: () {
                    if (_formKey.currentState.validate()) {
                      Map<String, dynamic> userData = {
                        "email": _emailController.text,
                        "phone": _phoneController.text,
                        "name": _nameController.text,
                        "cpf": _cpfController.text,
                        "clientType": 2
                      };
                      model.updateUser(userData, context, _onFail, _onSuccess);
                    }
                  },
                ),
              ),
              SizedBox(height: getProportionateScreenHeight(20)),
              Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: getProportionateScreenWidth(20)),
                  child: GestureDetector(
                    onTap: () => {model.signOut(context)},
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Encerrar Sessão",
                        ),
                        const SizedBox(width: 10),
                        Icon(
                          Icons.arrow_forward_ios,
                          size: 12,
                          color: kTextColor,
                        )
                      ],
                    ),
                  ))
            ],
          ),
        );
      }),
    ]);
  }

  void _onSuccess() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => HomeScreen2(),
    ));
  }

  void _onFail() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao realizar login"),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 2),
    ));
  }
}
