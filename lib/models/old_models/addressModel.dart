class AddressModel {
  String streetName;
  String addressNumber;
  String neighborhood;
  String cityName;

  AddressModel({
    this.streetName,
    this.addressNumber,
    this.neighborhood,
    this.cityName,
  });
}