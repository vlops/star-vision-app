import 'package:app_shoe_shop/models/Product.dart';
import 'package:flutter/material.dart';

import 'components/body.dart';

class LensSuggestionScreen extends StatelessWidget {
  static String routeName = "/lens_suggestion";
  String parameter = "";

  @override
  Widget build(BuildContext context) {
    final LensSuggestionArguments args =
        ModalRoute.of(context).settings.arguments;

    if (args.urlArgs == null) {
      this.parameter = "empty";
    } else {
      this.parameter = args.urlArgs;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Receita Virtual"),
      ),
      body: Body(parameter, args.product, args.recommendations),
    );
  }
}

class LensSuggestionArguments {
  final String urlArgs;
  final Product product;
  final List<bool> recommendations;

  LensSuggestionArguments(
      {@required this.urlArgs,
      @required this.product,
      @required this.recommendations});
}
