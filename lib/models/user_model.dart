import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/home/home_screen2.dart';
import 'package:app_shoe_shop/screens/login_screen.dart';
import 'package:app_shoe_shop/screens/login_star/login_star.dart';
import 'package:app_shoe_shop/screens/login_success/login_success_screen.dart';
import 'package:app_shoe_shop/screens/sign_in/sign_in_screen.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/components/lens.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_treatment/components/treatments.dart';
import 'package:app_shoe_shop/sidebar/sidebar_layout.dart';
import 'package:app_shoe_shop/utils/mailer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

class UserModel extends Model {
  FirebaseAuth _auth = FirebaseAuth.instance;

  final GoogleSignIn googleSignIn = GoogleSignIn();

  FacebookLogin fbLogin = new FacebookLogin();

  Mailer mailer = Mailer();

  // ignore: deprecated_member_use
  FirebaseUser firebaseUser;
  Map<String, dynamic> userData = Map();

  bool isLoading = false;

  void signUpWithGoogle(BuildContext context, int clientType) async {
    try {
      await googleSignIn.signOut();
      isLoading = true;
      notifyListeners();

      final GoogleSignInAccount googleSignInAccount =
          await googleSignIn.signIn();

      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);

      final AuthResult authResult =
          await FirebaseAuth.instance.signInWithCredential(credential);

      firebaseUser = authResult.user;

      Map<String, dynamic> data = {
        "email": firebaseUser.email,
        "cpf": null,
        "phone": firebaseUser.phoneNumber,
        "name": firebaseUser.displayName,
        "photoUrl": firebaseUser.photoUrl,
        "clientType": clientType
      };
      await _saveUserData(data);

      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => HomeScreen2(),
      ));
      isLoading = false;
      notifyListeners();
    } catch (error) {
      isLoading = false;
      notifyListeners();
    }
  }

  void signUpwithFacebook(BuildContext context, int clientType) async {
    await fbLogin.logOut();
    isLoading = true;
    notifyListeners();

    fbLogin.logIn(['email', 'public_profile']).then((result) {
      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final AuthCredential credential = FacebookAuthProvider.getCredential(
            accessToken: result.accessToken.token,
          );

          FirebaseAuth.instance
              .signInWithCredential(credential)
              .then((signedInUser) async {
            firebaseUser = signedInUser.user;

            Map<String, dynamic> data = {
              "email": firebaseUser.email,
              "cpf": null,
              "phone": firebaseUser.phoneNumber,
              "name": firebaseUser.displayName,
              "photoUrl": firebaseUser.photoUrl,
              "clientType": clientType
            };
            await _saveUserData(data);

            Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (context) => HomeScreen2(),
            ));
            isLoading = false;
            notifyListeners();
          }).catchError((e) {});
      }
    }).catchError((e) {});
  }

  void signUp(
      {@required Map<String, dynamic> userData,
      @required String pass,
      @required BuildContext context}) {
    isLoading = true;
    notifyListeners();

    _auth
        .createUserWithEmailAndPassword(
            email: userData["email"], password: pass)
        .then((user) async {
      firebaseUser = user.user;

      await _saveUserData(userData);

      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => HomeScreen2(),
      ));
      isLoading = false;
      notifyListeners();
    }).catchError((e) {
      isLoading = false;
      notifyListeners();
    });
  }

  void signInWithGoogle(BuildContext context, VoidCallback onFail) async {
    try {
      isLoading = true;
      notifyListeners();

      final GoogleSignInAccount googleSignInAccount =
          await googleSignIn.signIn();

      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;

      final AuthCredential credential = GoogleAuthProvider.getCredential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);

      final AuthResult authResult =
          await FirebaseAuth.instance.signInWithCredential(credential);

      firebaseUser = authResult.user;

      _loadCurrentUserWithSocial(firebaseUser, context, onFail);
    } catch (error) {
      isLoading = false;
      notifyListeners();
    }
  }

  void signInWithFacebook(BuildContext context, VoidCallback onFail) {
    fbLogin.logIn(['email', 'public_profile']).then((result) {
      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final AuthCredential credential = FacebookAuthProvider.getCredential(
            accessToken: result.accessToken.token,
          );

          FirebaseAuth.instance
              .signInWithCredential(credential)
              .then((signedInUser) {
            firebaseUser = signedInUser.user;
            _loadCurrentUserWithSocial(firebaseUser, context, onFail);
          }).catchError((e) {
            onFail();
            isLoading = false;
            notifyListeners();
          });
      }
    }).catchError((e) {
      onFail();
      isLoading = false;
      notifyListeners();
    });
  }

  void signIn(
      {@required String email,
      @required String pass,
      @required BuildContext context}) {
    isLoading = true;
    notifyListeners();

    _auth
        .signInWithEmailAndPassword(email: email, password: pass)
        .then((user) async {
      firebaseUser = user.user;

      await loadCurrentUser();

      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => HomeScreen2(),
      ));
      isLoading = false;
      notifyListeners();
    }).catchError((e) {
      isLoading = false;
      notifyListeners();
    });
  }

  void recoverPass(String email) {
    _auth.sendPasswordResetEmail(email: email);
  }

  bool isLoggedIn() {
    return firebaseUser != null;
  }

  bool isImage() {
    return firebaseUser.photoUrl != null;
  }

  void signOut(BuildContext context) async {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(LoginStarScreen.routeName, (route) => false);

    await _auth.signOut();
    await googleSignIn.signOut();

    userData = Map();
    firebaseUser = null;

    notifyListeners();
  }

  Future<Null> _saveUserData(Map<String, dynamic> userData) async {
    this.userData = userData;
    Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .setData(userData);
  }

  Future<Null> updateUser(Map<String, dynamic> userData, BuildContext context,
      VoidCallback onFail, VoidCallback onSuccess) async {
    isLoading = true;
    notifyListeners();
    try {
      FirebaseUser user = await _auth.currentUser();
      user.updateEmail(userData["email"]);

      Firestore.instance
          .collection("users")
          .document(firebaseUser.uid)
          .updateData({
        'name': userData["name"],
        'phone': userData["phone"],
        'email': userData["email"],
        'cpf': userData["cpf"],
        'clientType': userData["clientType"]
      });

      DocumentSnapshot doc = await Firestore.instance
          .collection("users")
          .document(firebaseUser.uid)
          .get();

      this.userData = doc.data;
      isLoading = false;
      notifyListeners();
      onSuccess();
    } catch (e) {
      isLoading = false;
      notifyListeners();
      onFail();
    }
  }

  void createAddress(
      {@required Map<String, dynamic> addressData,
      @required BuildContext context,
      @required Product product,
      @required Lens lens,
      @required Treatment treatment}) async {
    isLoading = true;
    notifyListeners();

    await Firestore.instance
        .collection("address")
        .add(addressData)
        .then((val) async {
      List<DocumentReference> cep = [];

      cep.add(val);

      Map<String, dynamic> data = {"address": cep};

      await Firestore.instance
          .collection("users")
          .document(firebaseUser.uid)
          .get()
          .then((value) async {
        await Firestore.instance
            .collection("users")
            .document(value.documentID)
            .updateData(data)
            .then((res) {
          Navigator.pushNamed(context, CartScreen.routeName,
              arguments: CheckoutArguments(
                  product: product,
                  urlDocument: 'urlDocument',
                  urlSelfie: 'widget.urlSelfie',
                  urlRecipe: 'urlRecipe',
                  entrega: "Entrega",
                  lens: lens,
                  treatment: treatment));
        }).catchError((e) {
          isLoading = false;
          notifyListeners();
          print("error 3");
        });
      }).catchError((e) {
        isLoading = false;
        notifyListeners();
        print("error 2");
      });
    }).catchError((e) {
      isLoading = false;
      notifyListeners();
      print("error 1");
    });
  }

  Future<Null> updateAddress(
      DocumentReference doc,
      Map<String, dynamic> addressData,
      Product product,
      BuildContext context,
      Lens lens,
      Treatment treatment) async {
    isLoading = true;
    notifyListeners();
    try {
      await Firestore.instance
          .collection("address")
          .document(doc.documentID)
          .updateData(addressData)
          .then((value) {
        Navigator.pushNamed(context, CartScreen.routeName,
            arguments: CheckoutArguments(
                product: product,
                urlDocument: 'urlDocument',
                urlSelfie: 'widget.urlSelfie',
                urlRecipe: 'urlRecipe',
                entrega: "Entrega",
                lens: lens,
                treatment: treatment));
      }).catchError((e) {
        isLoading = false;
        notifyListeners();
        print("error 1");
      });
      isLoading = false;
      notifyListeners();
    } catch (e) {
      isLoading = false;
      notifyListeners();
    }
  }

  Future<Null> _loadCurrentUserWithSocial(FirebaseUser firebaseUser,
      BuildContext context, VoidCallback onFail) async {
    DocumentSnapshot docUser = await Firestore.instance
        .collection("users")
        .document(firebaseUser.uid)
        .get();
    if (docUser.data != null) {
      userData = docUser.data;
      isLoading = false;
      notifyListeners();
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => HomeScreen2()));
    } else {
      firebaseUser.delete();
      await fbLogin.logOut();
      await googleSignIn.signOut();
      onFail();
      isLoading = false;
      notifyListeners();
    }
  }

  Future<Null> loadCurrentUser() async {
    if (firebaseUser == null) {
      firebaseUser = await _auth.currentUser();
    }
    if (firebaseUser != null) {
      if (userData["name"] == null) {
        DocumentSnapshot docUser = await Firestore.instance
            .collection("users")
            .document(firebaseUser.uid)
            .get();
        userData = docUser.data;
        notifyListeners();
      }
    }
  }
}
