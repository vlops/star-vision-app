// import 'package:app_shoe_shop/Provider/usersProvider.dart';
// import 'package:flutter/material.dart';
// import 'package:app_shoe_shop/Provider/firebaseProvider.dart';
// import 'package:email_validator/email_validator.dart';
// import 'package:firebase_auth/firebase_auth.dart';

// class UserProfile extends StatefulWidget {
//   @override
//   _UserProfileState createState() => _UserProfileState();
// }

// class _UserProfileState extends State<UserProfile> {
//   final _controllerEmail = TextEditingController();
//   final _controllerTelefone = TextEditingController();
//   final _formKey = GlobalKey<FormState>();
//   UserOptic _user;

//   Future<bool> _loadUser() async{
//     if (_user != null)
//       return true;

//     FirebaseAuth auth = FirebaseAuth.instance;
//     _user = await UserProvider().getUser(auth.currentUser.uid);

//     setState(() {
//       _controllerEmail.text = _user.email;
//       _controllerTelefone.text = _user.telefone.toString();
//     });

//     return (_user != null);
//   }

//   Widget _buildUserProfileForm(){
//     return Padding(
//       padding: const EdgeInsets.only(top: 20),
//       child: Form(
//         key: _formKey,
//         child: Column(
//           children: [
//             Text("Email"),
//             Padding(
//               padding: const EdgeInsets.all(15.0),
//               child: TextFormField(
//                 decoration: InputDecoration(
//                     hintText: "Digite seu email"
//                 ),
//                 validator: (value){
//                   if (!EmailValidator.validate(value)) {
//                     return "Por favor digite um email valido";
//                   }
//                   return null;
//                 },
//                 controller: _controllerEmail,
//               ),
//             ),
//             Text("Celular"),
//             Padding(
//               padding: const EdgeInsets.all(15.0),
//               child: TextFormField(
//                 decoration: InputDecoration(
//                     hintText: "Digite seu telefone"
//                 ),
//                 validator: (value){
//                   if (value.isEmpty) {
//                     return "Esse campo é obrigatório";
//                   }
//                   return null;
//                 },
//                 controller: _controllerTelefone,
//               ),
//             ),
//             Padding(
//               padding: const EdgeInsets.symmetric(vertical: 16.0),
//               child: ElevatedButton(
//                 onPressed: () async {
//                   if (_formKey.currentState.validate()) {
//                     setState(() {
//                       _user.email = _controllerEmail.text;
//                       _user.telefone = int.parse(_controllerTelefone.text);
//                     });

//                     await UserProvider().updateUser(_user);
//                   }
//                 },
//                 child: Text('Atualizar'),
//               ),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return ListView(
//       children: [
//         FutureBuilder(
//           future: _loadUser(),
//           builder: (context, snapshot){
//             if(snapshot.hasData){
//               return _buildUserProfileForm();
//             }
//             else{
//               return Center(
//                 child: CircularProgressIndicator(),
//               );
//             }
//           },
//         ),
//         Center(
//           child: FlatButton(
//             child: Text("Sair"),
//             color: Colors.blue,
//             onPressed: () async {
//               await FirebaseProvider().signOutFirebaseUser();
//               Navigator.pop(context);
//             },
//           ),
//         ),
//       ],
//     );
//   }
// }
