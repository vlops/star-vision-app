// import 'package:firebase_core/firebase_core.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';

// class ProductOptic {
//   String id;
//   String brand;
//   String category;
//   String desc;
//   String price;
//   String productName;
//   int quantity;
//   String tags;
//   String image;

//   ProductOptic(this.brand, this.category, this.desc, this.price, this.productName, this.quantity, this.tags);

//   ProductOptic.FromMap(Map<String, dynamic> map) {
//     this.brand = map["brand"];
//     this.category = map["category"];
//     this.desc = map["desc"];
//     this.price = map["price"];
//     this.productName = map["productName"];
//     this.quantity = map["quantity"];
//     this.tags = map["tags"];
//     this.image = map["image"];
//   }

//   Map<String, dynamic> toMap(){
//     return{
//       "brand" : brand,
//       "categoryName" : category,
//       "desc" : desc,
//       "price" : price,
//       "productName" : productName,
//       "quantity" : quantity,
//       "tags" : tags,
//       "image" : image
//     };
//   }
// }

// class ProductProvider {
//   CollectionReference products = FirebaseFirestore.instance.collection("products");

//   Future<List<ProductOptic>> getProductByCategory(String categoryName) async {
//     var _products = List<ProductOptic>();

//     await products.where("category", isEqualTo: categoryName).get().then((value) => {
//       value.docs.forEach((element) {
//         var product = ProductOptic.FromMap(element.data());
//         _products.add(product);
//       })
//     });

//     return _products;
//   }
// }
