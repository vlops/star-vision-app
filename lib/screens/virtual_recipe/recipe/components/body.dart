import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/recipe/components/virtual_recipe_form.dart';
import 'package:flutter/material.dart';
import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/size_config.dart';
import 'package:scoped_model/scoped_model.dart';

// ignore: must_be_immutable
class Body extends StatefulWidget {
  String url;
  Product product;

  Body(this.url, this.product);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int _clientType = 2;

  Row _buildSubtitle() {
    return Row(
      children: [
        Container(
            // set width equal to height to make a square
            width: 30,
            height: 30,
            child: RaisedButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  // set the value to a very big number like 100, 1000...
                  borderRadius: BorderRadius.circular(100),
                  side: BorderSide(color: kPrimaryColor)),
              child: Text(
                '1',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () {},
            )),
        SizedBox(
          width: 10,
        ),
        Text("Receita", style: TextStyle(fontSize: 12)),
        SizedBox(
          width: 10,
        ),
        Container(
            // set width equal to height to make a square
            width: 30,
            height: 30,
            child: RaisedButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                // set the value to a very big number like 100, 1000...
                borderRadius: BorderRadius.circular(100),
              ),
              child: Text(
                '2',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () {},
            )),
        SizedBox(
          width: 10,
        ),
        Text(
          "Espessura",
          style: TextStyle(fontSize: 12),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
            // set width equal to height to make a square
            width: 30,
            height: 30,
            child: RaisedButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  // set the value to a very big number like 100, 1000...
                  borderRadius: BorderRadius.circular(100)),
              child: Text(
                '3',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () {},
            )),
        SizedBox(
          width: 10,
        ),
        Text("Tratamento", style: TextStyle(fontSize: 12)),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );
      return SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text("Receita Virtual", style: headingStyle),
                  SizedBox(height: SizeConfig.screenHeight * 0.01),
                  _buildSubtitle(),
                  SizedBox(height: SizeConfig.screenHeight * 0.04),
                  VirtualRecipeForm(_clientType, widget.url, widget.product),
                  // DefaultButton(
                  //   text: "Continuar",
                  //   press: () {},
                  // ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
