import 'package:app_shoe_shop/screens/account/account_screen.dart';
import 'package:app_shoe_shop/screens/all_categories/all_categories.dart';
import 'package:app_shoe_shop/screens/brands/brands_screen.dart';
import 'package:app_shoe_shop/screens/buy_additional/buy_additional_screen.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';
import 'package:app_shoe_shop/screens/delivery/delivery.dart';
import 'package:app_shoe_shop/screens/details/details_screen.dart';
import 'package:app_shoe_shop/screens/forgot_password/forgot_password_screen.dart';
import 'package:app_shoe_shop/screens/login_screen.dart';
import 'package:app_shoe_shop/screens/login_star/login_star.dart';
import 'package:app_shoe_shop/screens/login_success/login_success_screen.dart';
import 'package:app_shoe_shop/screens/product_filter/product_filter_screen.dart';
import 'package:app_shoe_shop/screens/sign_in/sign_in_screen.dart';
import 'package:app_shoe_shop/screens/sign_up/sign_up_screen.dart';
import 'package:app_shoe_shop/screens/splash/splash_screen.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/lens_suggestion.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_treatment/lens_treatment.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/recipe/virtual_recipe_screen.dart';
import 'package:flutter/widgets.dart';
import 'package:app_shoe_shop/screens/home/home_screen2.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  HomeScreen2.routeName: (context) => HomeScreen2(), //*
  SplashScreen.routeName: (context) => SplashScreen(), //*
  SignInScreen.routeName: (context) => SignInScreen(), //*
  ForgotPasswordScreen.routeName: (context) => ForgotPasswordScreen(), //*
  LoginSuccessScreen.routeName: (context) => LoginSuccessScreen(), //*
  LoginScreen.routeName: (context) => LoginScreen(), //*
  SignUpScreen2.routeName: (context) => SignUpScreen2(), //*
  CompleteProfileScreen.routeName: (context) => CompleteProfileScreen(), //*
  // OtpScreen.routeName: (context) => OtpScreen(),
  DetailsScreen.routeName: (context) => DetailsScreen(), //*
  CartScreen.routeName: (context) => CartScreen(), //*
  ProductFilterScreen.routeName: (context) => ProductFilterScreen(),
  AccountScreen.routeName: (context) => AccountScreen(),
  BuyAdditionalScreen.routeName: (context) => BuyAdditionalScreen(),
  AllCategoriesScreen.routeName: (context) => AllCategoriesScreen(),
  BrandsScreen.routeName: (context) => BrandsScreen(),
  VirtualRecipeScreen.routeName: (context) => VirtualRecipeScreen(),
  LensSuggestionScreen.routeName: (context) => LensSuggestionScreen(),
  LensTreatmentScreen.routeName: (context) => LensTreatmentScreen(),
  DeliveryScreen.routeName: (context) => DeliveryScreen(),
  LoginStarScreen.routeName: (context) => LoginStarScreen()
};
