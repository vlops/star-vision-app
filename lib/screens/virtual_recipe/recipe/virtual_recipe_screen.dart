import 'package:app_shoe_shop/models/Product.dart';
import 'package:flutter/material.dart';

import 'components/body.dart';

class VirtualRecipeScreen extends StatelessWidget {
  static String routeName = "/virtual_recipe";
  String parameter = "";

  @override
  Widget build(BuildContext context) {
    final VirtualRecipeArguments args =
        ModalRoute.of(context).settings.arguments;

    if (args.urlArgs == null) {
      this.parameter = "empty";
    } else {
      this.parameter = args.urlArgs;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Receita Virtual"),
      ),
      body: Body(parameter, args.product),
    );
  }
}

class VirtualRecipeArguments {
  final String urlArgs;
  final Product product;

  VirtualRecipeArguments({@required this.urlArgs, @required this.product});
}
