import 'package:app_shoe_shop/components/custom_surfix_icon.dart';
import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/components/form_error.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/home/home_screen2.dart';
import 'package:app_shoe_shop/utils/mailer.dart';
import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../constants.dart';
import '../../../size_config.dart';
import '../../login_success/login_success_screen.dart';

class CompleteProfileFormTypeOne extends StatefulWidget {
  String email;
  String password;

  CompleteProfileFormTypeOne(this.email, this.password);

  @override
  _CompleteProfileFormTypeOneState createState() =>
      _CompleteProfileFormTypeOneState();
}

class _CompleteProfileFormTypeOneState
    extends State<CompleteProfileFormTypeOne> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  String name;
  String cnpj;
  String phoneNumber;
  String address;

  TextEditingController _nameController = TextEditingController();
  TextEditingController _cnpjController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();
  TextEditingController _addressController = TextEditingController();

  final Mailer mailer = Mailer();

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );
      return Form(
        key: _formKey,
        child: Column(
          children: [
            buildNameFormField(_nameController),
            SizedBox(height: getProportionateScreenHeight(30)),
            buildCNPJFormField(_cnpjController),
            SizedBox(height: getProportionateScreenHeight(30)),
            buildPhoneNumberFormField(_phoneNumberController),
            SizedBox(height: getProportionateScreenHeight(30)),
            buildAddressFormField(_addressController),
            FormError(errors: errors),
            SizedBox(height: getProportionateScreenHeight(40)),
            DefaultButton(
              text: "Criar Conta",
              press: () {
                if (_formKey.currentState.validate()) {
                  Map<String, dynamic> userData = {
                    "email": widget.email,
                    "cnpj": _cnpjController.text,
                    "phone": _phoneNumberController.text,
                    "name": _nameController.text,
                    "address": _addressController.text,
                    "photoUrl": "",
                    "clientType": 1
                  };
                  model.signUp(
                      userData: userData,
                      pass: widget.password,
                      context: context);
                  mailer.sender(
                      cnpj: _cnpjController.text,
                      name: _nameController.text,
                      email: widget.email,
                      phone: _phoneNumberController.text,
                      address: _addressController.text);
                }
              },
            ),
          ],
        ),
      );
    });
  }

  TextFormField buildPhoneNumberFormField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phoneNumber = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Telefone",
          hintText: "Digite o numero de telefone",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg"),
          labelStyle: TextStyle(color: kPrimaryColor)),
      inputFormatters: [
        MaskTextInputFormatter(
            mask: "(##)#-####-####", filter: {"#": RegExp(r'[0-9]')})
      ],
    );
  }

  TextFormField buildCNPJFormField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      keyboardType: TextInputType.number,
      onSaved: (newValue) => cnpj = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kCNPJInvalidError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty || !CNPJ.isValid(value)) {
          addError(error: kCNPJInvalidError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "CNPJ",
          hintText: "Digite o CNPJ",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          labelStyle: TextStyle(color: kPrimaryColor)),
      inputFormatters: [
        MaskTextInputFormatter(
            mask: "##.###.###/####-##", filter: {"#": RegExp(r'[0-9]')})
      ],
    );
  }

  TextFormField buildNameFormField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      onSaved: (newValue) => name = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Nome Fantasia",
          hintText: "Digite o nome da empresa",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          labelStyle: TextStyle(color: kPrimaryColor)),
    );
  }

  TextFormField buildAddressFormField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      onSaved: (newValue) => name = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kAddressNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kAddressNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Endereço",
          hintText: "Digite o endereço da empresa",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          labelStyle: TextStyle(color: kPrimaryColor)),
    );
  }
}
