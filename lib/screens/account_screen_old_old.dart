import 'package:app_shoe_shop/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:scoped_model/scoped_model.dart';

import '../widgets/bloc.navigation_bloc/navigation_bloc.dart';

class MyAccountScreen extends StatefulWidget with NavigationStates {
  static String routeName = "/my_account";

  @override
  _MyAccountScreenState createState() => _MyAccountScreenState();
}

class _MyAccountScreenState extends State<MyAccountScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  bool showPassword = false;

  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _cpfController = TextEditingController();

  Widget buildTextField(
      {@required String labelText,
      @required String placeholder,
      @required TextEditingController controller,
      @required bool isPhone,
      @required bool isEmail}) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextFormField(
        controller: controller,
        enabled: isEmail ? false : true,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(bottom: 3),
          labelText: labelText,
          floatingLabelBehavior: FloatingLabelBehavior.always,
        ),
        validator: (text) {
          if (text.isEmpty) return "Preencha este campo";
        },
        inputFormatters: isPhone
            ? [
                MaskTextInputFormatter(
                    mask: "(##)#-####-####", filter: {"#": RegExp(r'[0-9]')})
              ]
            : [],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Scaffold(
          key: _scaffoldKey,
          body: ScopedModelDescendant<UserModel>(
              builder: (context, child, model) {
            _nameController.text = model.userData["name"];
            _emailController.text = model.userData["email"];
            _phoneController.text = model.userData["phone"];
            _cpfController.text = model.userData["cpf"];

            model.loadCurrentUser();

            if (model.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );

            return Form(
              key: _formKey,
              child: Container(
                padding: EdgeInsets.only(left: 25, top: 25, right: 16),
                child: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).unfocus();
                  },
                  child: ListView(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 50),
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Text(
                            "Minha Conta",
                            style: TextStyle(
                                fontSize: 25, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Center(
                        child: Stack(
                          children: [
                            Container(
                              width: 130,
                              height: 130,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 4,
                                      color: Theme.of(context)
                                          .scaffoldBackgroundColor),
                                  boxShadow: [
                                    BoxShadow(
                                        spreadRadius: 2,
                                        blurRadius: 10,
                                        color: Colors.black.withOpacity(0.1),
                                        offset: Offset(0, 10))
                                  ],
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: NetworkImage(
                                        "https://lh6.googleusercontent.com/proxy/s7o0T8xVjJKFQiQSgy2YcZZVeoR-NcKnp7C5_EbLExNyHP74mk9EMdfhRhW1mxuF3pKvoHFdS2Pii4aWzJOVYHh0r-S9OSUtcK3R=s0-d",
                                      ))),
                            ),
                            Positioned(
                                bottom: 0,
                                right: 0,
                                child: Container(
                                  height: 40,
                                  width: 40,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                      width: 4,
                                      color: Theme.of(context)
                                          .scaffoldBackgroundColor,
                                    ),
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                  ),
                                )),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      buildTextField(
                          labelText: "Nome",
                          placeholder: model.userData["name"],
                          controller: _nameController,
                          isPhone: false,
                          isEmail: false),
                      buildTextField(
                          labelText: "Email",
                          placeholder: model.userData["email"],
                          controller: _emailController,
                          isPhone: false,
                          isEmail: true),
                      buildTextField(
                          labelText: "Celular",
                          placeholder: model.userData["phone"],
                          controller: _phoneController,
                          isPhone: true,
                          isEmail: false),
                      SizedBox(
                        height: 35,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          RaisedButton(
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                Map<String, dynamic> userData = {
                                  "email": _emailController.text,
                                  "phone": _phoneController.text,
                                  "name": _nameController.text,
                                  "cpf": _cpfController.text,
                                  "clientType": model.userData
                                };
                                model.updateUser(
                                    userData, context, _onFail, _onSuccess);
                              }
                            },
                            color: Theme.of(context).primaryColor,
                            padding: EdgeInsets.symmetric(horizontal: 50),
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            child: Text(
                              "SALVAR",
                              style: TextStyle(
                                  fontSize: 14,
                                  letterSpacing: 2.2,
                                  color: Colors.white),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          })),
    );
  }

  void _onSuccess() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Dados atualizados com sucesso"),
      backgroundColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 2),
    ));
  }

  void _onFail() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao atualizar informações do usuário"),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 2),
    ));
  }
}
