import 'package:app_shoe_shop/components/product_card.dart';
import 'package:app_shoe_shop/models/Cart.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/all_categories/all_categories.dart';
import 'package:app_shoe_shop/screens/home/components/search_field.dart';
import 'package:app_shoe_shop/screens/product_filter/components/product_filter_card_grid.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../size_config.dart';
import 'product_filter_card.dart';

// ignore: must_be_immutable
class Body extends StatefulWidget {
  String category;
  int grid;

  Body(this.category, this.grid);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<Product> filterListProducts = [];
  Product product;

  Widget _getProductForCategory(AsyncSnapshot<DocumentSnapshot> snapshotItem) {
    return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("glasses").getDocuments(),
        builder: (context, snap) {
          if (!snap.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            int qtd = snapshotItem.data.data["glasses"].length;

            for (var e in snap.data.documents) {
              for (int i = 0; i < qtd; i++) {
                DocumentReference doc = snapshotItem.data.data["glasses"][i];
                bool contain = false;
                if (e.documentID == doc.documentID) {
                  product = Product(
                      images: e.data['images'],
                      colors: [
                        Color(0xFFF6625E),
                        Color(0xFF836DB8),
                        Color(0xFFDECB9C),
                        Colors.white
                      ],
                      rating: "${0.0}",
                      isFavourite: false,
                      title: e.data['title'],
                      price: e.data['price'],
                      model: e.data['model'],
                      description: e.data['description'],
                      material: e.data['material'],
                      category: "A fazer",
                      obj: e.data['obj']);
                  for (int i = 0; i < filterListProducts.length; i++) {
                    if (product.title == filterListProducts[i].title) {
                      contain = true;
                    }
                  }
                  if (contain == false) {
                    filterListProducts.add(product);
                  }
                }
              }
            }

            if (filterListProducts.isEmpty) {
              return Center(
                child: Text("Esta categoria não possui produtos."),
              );
            }

            // _stateGridOrList();
            if (widget.grid == 0) {
              return GridView.count(
                crossAxisCount: 2,
                crossAxisSpacing: 1,
                mainAxisSpacing: 1,
                childAspectRatio: (0.7),
                children: List.generate(filterListProducts.length, (index) {
                  return ProductCardGrid(product: filterListProducts[index]);
                }),
              );
            } else {
              return Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: ListView.builder(
                  itemCount: filterListProducts.length,
                  itemBuilder: (context, index) => Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Dismissible(
                      key: Key(filterListProducts[index].title),
                      direction: DismissDirection.endToStart,
                      onDismissed: (direction) {
                        setState(() {
                          demoCarts.removeAt(index);
                        });
                      },
                      background: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                          color: Color(0xFFFFE6E6),
                          borderRadius: BorderRadius.circular(15),
                        ),
                        child: Row(
                          children: [
                            Spacer(),
                            SvgPicture.asset("assets/icons/Heart Icon.svg"),
                          ],
                        ),
                      ),
                      child:
                          ProductFilterCard(product: filterListProducts[index]),
                    ),
                  ),
                ),
              );
            }
          }
        });
  }

  Widget _getCategory(String category) {
    return FutureBuilder<DocumentSnapshot>(
        future: Firestore.instance
            .collection("categories")
            .document(category)
            .get(),
        builder: (context, snapshotItem) {
          if (!snapshotItem.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            return _getProductForCategory(snapshotItem);
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("products").getDocuments(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            //return _getCategoryPopular(snapshot.data.documents[0]);
            if (widget.category.contains("Destaques"))
              return _getCategory("destaques");
            else if (widget.category.contains("Promoções"))
              return _getCategory("promocoes");
            else if (widget.category.contains("Outros"))
              return _getCategory("outros");
            else if (widget.category.contains("Retangular"))
              return _getCategory("retangular");
            else
              return Row();
          }
        });
  }
}
