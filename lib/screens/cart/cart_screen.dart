import 'package:app_shoe_shop/models/Cart.dart';
import 'package:app_shoe_shop/models/Endereco.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/components/lens.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_treatment/components/treatments.dart';
import 'package:flutter/material.dart';

import 'components/body.dart';
import 'components/check_out_card.dart';

class CartScreen extends StatelessWidget {
  static String routeName = "/cart";
  String verifySelfie;
  String verifyEntrega;

  @override
  Widget build(BuildContext context) {
    final CheckoutArguments args = ModalRoute.of(context).settings.arguments;
    if (args.urlSelfie == null) {
      verifySelfie = "empty";
    } else {
      verifySelfie = args.urlSelfie;
    }
    if (args.entrega == null) {
      verifyEntrega = "empty";
    } else {
      verifyEntrega = args.entrega;
    }
    return Scaffold(
      appBar: buildAppBar(context),
      body: Body(args.product, args.urlDocument, args.urlRecipe, args.urlSelfie,
          verifyEntrega, args.lens, args.treatment),
      bottomNavigationBar: CheckoutCard(
        product: args.product,
        entrega: verifyEntrega,
        lens: args.lens,
        treatment: args.treatment,
      ),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Column(
        children: [
          Text(
            "Checkout",
            style: TextStyle(color: Colors.black),
          ),
          Text(
            "1 item",
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}

class CheckoutArguments {
  final Product product;
  final Lens lens;
  final Treatment treatment;
  final String urlSelfie, urlRecipe, urlDocument;
  final String entrega;

  CheckoutArguments({
    @required this.product,
    @required this.entrega,
    @required this.treatment,
    @required this.lens,
    this.urlDocument,
    this.urlRecipe,
    this.urlSelfie,
  });
}
