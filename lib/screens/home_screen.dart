import 'package:app_shoe_shop/datas/product_data.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../widgets/bloc.navigation_bloc/navigation_bloc.dart';
import 'bottom_bar.dart';
import 'products_screen.dart';

class HomeScreen extends StatefulWidget with NavigationStates {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  QuerySnapshot queryDestaques;
  QuerySnapshot queryOutros;
  QuerySnapshot queryPromocoes;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  Widget _getItem(DocumentSnapshot documentSnapshot){
    return FutureBuilder<QuerySnapshot>(
      future: Firestore.instance.collection("products").document(documentSnapshot.documentID)
        .collection("items").getDocuments(),
      builder: (context, snapshotItem){
        if(!snapshotItem.hasData)
          return Center(child: CircularProgressIndicator(),);
        else
          return ProductsScreen(snapshotItem.data);
      }
    );
  }

  Widget _getItemForCategory(DocumentSnapshot documentSnapshot, String category){
    return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("products").document(documentSnapshot.documentID)
            .collection("items").where(category, isEqualTo: "Quadrado").getDocuments(),
        builder: (context, snapshotItem){
          if(!snapshotItem.hasData)
            return Center(child: CircularProgressIndicator(),);
          else
            return ProductsScreen(snapshotItem.data);
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
      future: Firestore.instance.collection("products").getDocuments(),
      builder: (context, snapshot){
        if(!snapshot.hasData)
          return Center(child: CircularProgressIndicator(),);
        else{
          return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
            model.loadCurrentUser();
            return Scaffold(
              backgroundColor: Colors.white,
              body: ListView(
                padding: EdgeInsets.only(left: 20.0),
                children: <Widget>[
                  SizedBox(height: 15.0),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 50.0, 20.0, 20.20),
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Text('Categorias',
                          style: TextStyle(
                              fontFamily: 'Varela',
                              fontSize: 42.0,
                              fontWeight: FontWeight.bold)),
                    ),
                  ),
                  SizedBox(height: 15.0),
                  TabBar(
                      controller: _tabController,
                      indicatorColor: Colors.transparent,
                      labelColor: Color(0xFF73AEF5),
                      isScrollable: true,
                      labelPadding: EdgeInsets.only(right: 45.0),
                      unselectedLabelColor: Color(0xFFCDCDCD),
                      tabs:[
                            Tab(
                            child: Text("Destaques",
                                style: TextStyle(
                                  fontFamily: 'Varela',
                                  fontSize: 21.0,
                                )),
                          ),
                        Tab(
                          child: Text("Outros",
                              style: TextStyle(
                                fontFamily: 'Varela',
                                fontSize: 21.0,
                              )),
                        )
                        ,
                        Tab(
                          child: Text("Promoções",
                              style: TextStyle(
                                fontFamily: 'Varela',
                                fontSize: 21.0,
                              )),
                        ),
                        // ,Tab(
                        //   child: Text("Quadrado",
                        //       style: TextStyle(
                        //         fontFamily: 'Varela',
                        //         fontSize: 21.0,
                        //       )),
                        // ),
                        // Tab(
                        //   child: Text("Retangular",
                        //       style: TextStyle(
                        //         fontFamily: 'Varela',
                        //         fontSize: 21.0,
                        //       )),
                        // )
                      ]
                      ),
                      Container(
                            height: MediaQuery.of(context).size.height - 50.0,
                            width: double.infinity,
                            child: TabBarView(controller: _tabController, children:[
                                    _getItem(snapshot.data.documents[0]),
                                    _getItem(snapshot.data.documents[1]),
                                    _getItem(snapshot.data.documents[2]),
                                    // _getItem(snapshot.data.documents[3]),
                                    // _getItem(snapshot.data.documents[4]),

                                  ],),
                            )

                ],
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {},
                backgroundColor: Color(0xFF73AEF5),
                child: Icon(Icons.face),
              ),
              floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
              bottomNavigationBar: BottomHomeBar(),
            );
          });
        }
      }
    );


  }
}
