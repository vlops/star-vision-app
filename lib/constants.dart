import 'package:app_shoe_shop/size_config.dart';
import 'package:flutter/material.dart';

//const kPrimaryColor = Color(0xFFFF7643);
const kPrimaryColor = Color(0xFF388E3C);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFFFA53E), Color(0xFFFF7643)],
);
const kSecondaryColor = Color(0xFF979797);
const kTextColor = Color(0xFF757575);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: getProportionateScreenWidth(28),
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);

const defaultDuration = Duration(milliseconds: 250);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

const String kCPFInvalidError = "CPF Inválido";
const String kCNPJInvalidError = "CNPJ Inválido";
const String kPhoneNumberNullError = "Número de celular não pode ser vazio";
const String kCEPNullError = "CEP Inválido";
const String kLogradouroNullError = "Logradouro Inválido";
const String kBairroNullError = "Bairro Inválido";
const String kCityNullError = "Cidade Inválido";
const String kStateNullError = "Estado Inválido";
const String kNamelNullError = "Nome não pode ser vazio";
const String kMatchPassError = "Senhas não conferem";
const String kShortPassError = "Senha possui menos de 6 caracteres";
const String kEmailNullError = "Email não pode ser vazio";
const String kInvalidEmailError = "Email Inválido";
const String kPassNullError = "Senha não pode ser vazio";
const String kAddressNullError = "Endereço não pode ser vazio";
const String kRecipeNullError = "Foto da receita não pode ser vazio";
const String kDocumentNullError = "Foto do documento não pode ser vazio";

final otpInputDecoration = InputDecoration(
  contentPadding:
      EdgeInsets.symmetric(vertical: getProportionateScreenWidth(15)),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(getProportionateScreenWidth(15)),
    borderSide: BorderSide(color: kTextColor),
  );
}
