import 'package:app_shoe_shop/components/custom_dialog.dart';
import 'package:app_shoe_shop/screens/buy_additional/buy_additional_screen.dart';
import 'package:app_shoe_shop/screens/virtual_fitting_room/fitting_room2.dart';
import 'package:flutter/material.dart';
import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/size_config.dart';

import 'color_dots.dart';
import 'product_description.dart';
import 'top_rounded_container.dart';
import 'product_images.dart';

class Body extends StatelessWidget {
  final Product product;

  const Body({Key key, @required this.product}) : super(key: key);

  Widget stateButton() {
    if (product.obj.length != 0) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.center,
            child: Image.asset(
              "assets/images/star_icon.png",
              height: 20.0,
              width: 20.0,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Text("Provador Virtual"),
          ),
        ],
      );
    }
    return Row();
  }

  Widget stateBuyButton(BuildContext context) {
    if (product.obj.length != 1) {
      return DefaultButton(
        text: "Comprar",
        press: () {
          Navigator.pushNamed(context, BuyAdditionalScreen.routeName,
              arguments:
                  BuyAdditionalArguments(urlArgs: null, product: this.product));
        },
      );
    }
    return DefaultButton(
      text: "Comprar",
      press: () {
        showDialog(
            context: context,
            builder: (BuildContext context) {
              return CustomDialog(
                title: "Opção de Compra",
                description:
                    "Você está prestes a realizar uma compra. Caso também deseje efetuar a solicitação das lentes, clique no botão selfie para medição de lentes.",
                buttonText1: "Comprar armação e lentes",
                buttonText2: "Comprar apenas armação",
                product: product,
              );
            });
        //Navigator.pushNamed(context, CartScreen.routeName);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ProductImages(product: product),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              ProductDescription(
                product: product,
                pressOnSeeMore: () {},
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    ColorDots(product: product),
                    TopRoundedContainer(
                        color: Colors.white,
                        child: Column(
                          children: [
                            GestureDetector(
                                onTap: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => UnityDemoScreen2(
                                      obj: product.obj,
                                      product: this.product,
                                    ),
                                  ));
                                },
                                child: stateButton()),
                            Padding(
                              padding: EdgeInsets.only(
                                left: SizeConfig.screenWidth * 0.15,
                                right: SizeConfig.screenWidth * 0.15,
                                bottom: getProportionateScreenWidth(15),
                                top: getProportionateScreenWidth(15),
                              ),
                              child: stateBuyButton(context),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
