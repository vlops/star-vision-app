import 'dart:io';

import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/buy_additional/buy_additional_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_unity_widget/flutter_unity_widget.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:screenshot/screenshot.dart';
import 'package:transparent_image/transparent_image.dart';

class UnityDemoScreen extends StatefulWidget {
  String obj;
  Product product;
  // qUnityDemoScreen({Key key}) : super(key: key);

  UnityDemoScreen({this.obj, this.product});

  @override
  _UnityDemoScreenState createState() => _UnityDemoScreenState();
}

class _UnityDemoScreenState extends State<UnityDemoScreen> {
  GlobalKey _globalKey = new GlobalKey();

  ScreenshotController screenshotController = ScreenshotController();

  static final GlobalKey<ScaffoldState> _scaffoldKey =
      GlobalKey<ScaffoldState>();
  UnityWidgetController _unityWidgetController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Future<Uint8List> _capturePng() async {
    try {
      print('inside');
      RenderRepaintBoundary boundary =
          _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      var pngBytes = byteData.buffer.asUint8List();
      var bs64 = base64Encode(pngBytes);
      print(pngBytes);
      print(bs64);
      setState(() {});
      final result = await ImageGallerySaver.saveImage(pngBytes);
      print("File Saved to Gallery");
      return pngBytes;
    } catch (e) {
      print(e);
    }
  }

  Widget build(BuildContext context) {
    return RepaintBoundary(
      key: _globalKey,
      child: SafeArea(
          bottom: false,
          child: WillPopScope(
            onWillPop: () {
              // Pop the category page if Android back button is pressed.
            },
            child: Stack(
              children: <Widget>[
                UnityWidget(
                  onUnityMessage: onUnityMessage,
                  onUnityCreated: onUnityCreated,
                  onUnitySceneLoaded: onUnitySceneLoaded,
                  isARScene: true,
                  fullscreen: true,
                ),
                Positioned(
                    top: 10,
                    left: 0,
                    right: 0,
                    child: Row(
                      children: [
                        Flexible(
                          flex: 3,
                          child: Container(),
                        ),
                        Flexible(
                            flex: 2,
                            child: Column(
                              children: [
                                Image.asset('assets/images/logo_star.png'),
                              ],
                            )),
                        Flexible(
                          flex: 3,
                          child: Container(),
                        ),
                      ],
                    )),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Flexible(
                              flex: 5,
                              child: GestureDetector(
                                onTap: () {
                                  _unityWidgetController.unload();
                                  _unityWidgetController.quit();
                                  Navigator.pop(context);
                                },
                                child: FlatButton(
                                  color: Colors.transparent,
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                    size: 30,
                                  ),
                                ),
                              ),
                            ),
                            Flexible(
                              flex: 5,
                              child: GestureDetector(
                                onTap: () {
                                  Navigator.pushReplacementNamed(
                                      context, BuyAdditionalScreen.routeName,
                                      arguments: BuyAdditionalArguments(
                                          urlArgs: "teste",
                                          product: widget.product));
                                },
                                child: FlatButton(
                                  color: Colors.transparent,
                                  child: Icon(
                                    Icons.camera,
                                    color: Colors.white,
                                    size: 70,
                                  ),
                                ),
                              ),
                            ),
                            Flexible(flex: 3, child: Container()),
                          ],
                        )
                      ],
                    ),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.center,
                        end: Alignment.topCenter,
                        colors: <Color>[Colors.black, Colors.transparent],
                      ),
                    ),
                  ),
                )
              ],
            ),
          )),
    );
  }

  // Callback that connects the created controller to the unity controller
  void onUnityCreated(controller) {
    this._unityWidgetController = controller;
    //int armacao = 2;
    unitySetGlass(widget.obj);
  }

  void onUnitySceneLoaded(SceneLoaded sceneInfo) {
    unitySetGlass(widget.obj);
  }

  void unitySetGlass(String index) {
    _unityWidgetController.postMessage(
      'Manager',
      'mudarModeloFace',
      index,
    );
  }

  void unityTakePicture() {
    _unityWidgetController.postMessage(
        'Manager', 'CaptureScreeshot', 'takePicture');
  }

  void onUnityMessage(message) {
    print('Received message from unity: ${message.toString()}');
  }
}
