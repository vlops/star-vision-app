import 'package:app_shoe_shop/screens/account/components/account_form.dart';
import 'package:app_shoe_shop/screens/account/components/account_image.dart';
import 'package:flutter/material.dart';

import '../../../size_config.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        AccountImage(),
        SizedBox(height: getProportionateScreenHeight(30)),
        AccountForm()
      ]),
    ));
  }
}
