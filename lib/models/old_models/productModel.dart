class ProductModel {
  String productName;
  String quantity;
  String price;
  String desc;
  String brand;
  String category;
  String tags;

  ProductModel({
    this.productName,
    this.quantity,
    this.price,
  });
}