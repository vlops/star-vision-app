import 'package:app_shoe_shop/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AllCategoriesScreen extends StatelessWidget {
  static String routeName = "/all_categories";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white70),
          backgroundColor: kPrimaryColor,
          title: Text(
            "Categorias",
            style: TextStyle(color: Colors.white70),
          ),
        ),
        backgroundColor: Colors.grey[300],
        body: FutureBuilder<QuerySnapshot>(
            future: Firestore.instance.collection("categories").getDocuments(),
            builder: (context, snapshot) {
              if (!snapshot.hasData)
                return Center(
                  child: CircularProgressIndicator(),
                );
              else {
                //return _getCategoryPopular(snapshot.data.documents[0]);
                return Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Text(
                        "Todas as Categorias",
                        style: TextStyle(
                          fontSize: 20.0,
                          color: Colors.black,
                        ),
                      ),
                    ),
                    Flexible(
                        child: Padding(
                      padding: EdgeInsets.only(left: 10.0, right: 10.00),
                      child: Card(
                        elevation: 2,
                        child: ListView.builder(
                          itemCount: snapshot.data.documents.length,
                          itemBuilder: (_, index) {
                            return Padding(
                              padding: EdgeInsets.only(left: 10.0, right: 10.0),
                              child: ListTile(
                                title: Text(
                                  snapshot.data.documents[index]['title'],
                                  style: TextStyle(fontSize: 14.0),
                                ),
                                contentPadding: EdgeInsets.all(5.0),

                                //subtitle:
                                //   Text(snapshot.data.documents[index]['description']),
                                trailing: Icon(Icons.arrow_forward_ios),
                              ),
                            );
                          },
                        ),
                      ),
                    )),
                  ],
                );

                // return ListView.builder(
                //   itemCount: snapshot.data.documents.length,
                //   itemBuilder: (_, index) {
                //     return ListTile(
                //       title: Text(snapshot.data.documents[index]['title']),
                //       contentPadding: EdgeInsets.all(5.0),

                //       //subtitle:
                //       //   Text(snapshot.data.documents[index]['description']),
                //       trailing: Icon(Icons.arrow_forward_ios),
                //     );
                //   },
                // );
              }
            }));
  }
}
