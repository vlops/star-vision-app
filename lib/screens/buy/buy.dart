import 'dart:convert';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/components/lens.dart';
import 'package:http/http.dart' as http;

import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/delivery/delivery_form.dart';
import 'package:app_shoe_shop/screens/login_screen.dart';
import 'package:app_shoe_shop/screens/login_success/login_success_screen.dart';
import 'package:app_shoe_shop/screens/sign_up/components/sign_up_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class BuyScreen extends StatelessWidget {
  static String routeName = "/buy";
  final Product product;
  final Lens lens;
  final String value;

  BuyScreen({@required this.product, @required this.value, this.lens});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white70),
          backgroundColor: kPrimaryColor,
          title: Text(
            "Pagamento",
            style: TextStyle(color: Colors.white70),
          ),
        ),
        backgroundColor: Colors.grey[300],
        body:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          if (model.isLoading)
            return Center(
              child: CircularProgressIndicator(),
            );
          return Column(
            children: [
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Text(
                  "Formas de Pagamento",
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                ),
              ),
              Flexible(
                  child: Padding(
                      padding: EdgeInsets.only(
                          left: 10.0, right: 10.0, bottom: 10.0),
                      child: Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 10.0, right: 10.0, bottom: 10.0),
                              child: Align(
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                    children: [
                                      Text("Pagar com Cartão de Crédito",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black,
                                              fontSize: 15)),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Image.network(
                                        "https://blog.mxcursos.com/wp-content/uploads/2017/12/Pagarme.png",
                                        height: 60,
                                        width: 70,
                                      )
                                    ],
                                  ))),
                          Card(
                              elevation: 2,
                              child: Column(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 10.0, right: 10.0),
                                    child: ListTile(
                                      onTap: () {
                                        Navigator.pushNamed(context,
                                            LoginSuccessScreen.routeName);
                                      },
                                      title: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child: Text(
                                                "Mastercard",
                                                style:
                                                    TextStyle(fontSize: 14.0),
                                              ),
                                            ),
                                            Align(
                                              alignment: Alignment.centerLeft,
                                              child:
                                                  Text("Até 12 vezes sem juros",
                                                      style: TextStyle(
                                                        fontSize: 12.0,
                                                        color: Colors.green,
                                                      )),
                                            ),
                                          ],
                                        ),
                                      ),

                                      contentPadding: EdgeInsets.all(5.0),
                                      leading: Image.network(
                                        "https://logodownload.org/wp-content/uploads/2014/07/mastercard-logo-7.png",
                                        height: 50,
                                        width: 50,
                                      ),
                                      //subtitle:
                                      //   Text(snapshot.data.documents[index]['description']),
                                      trailing: Icon(Icons.arrow_forward_ios),
                                    ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(
                                        left: 10.0, right: 10.0),
                                    child: ListTile(
                                      title: Text(
                                        "Novo Cartão de Crédito",
                                        style: TextStyle(fontSize: 14.0),
                                      ),
                                      contentPadding: EdgeInsets.all(5.0),
                                      leading: Image.network(
                                        "https://www.innroad.com/wp-content/uploads/2014/03/icon-credit-card-blue.png",
                                        height: 50,
                                        width: 50,
                                      ),
                                      //subtitle:
                                      //   Text(snapshot.data.documents[index]['description']),
                                      trailing: Icon(Icons.arrow_forward_ios),
                                    ),
                                  )
                                ],
                              )),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 10.0,
                                  right: 10.0,
                                  bottom: 10.0,
                                  top: 10.0),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text("Com outros meios de pagamento",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 15)),
                              )),
                          GestureDetector(
                            onTap: () {
                              //postRequest(this.value, model);
                              print(model.userData['address']);
                            },
                            child: Card(
                                elevation: 2,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(
                                          left: 10.0, right: 10.0),
                                      child: ListTile(
                                        title: Align(
                                          alignment: Alignment.centerLeft,
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: [
                                              Align(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  "Boleto",
                                                  style:
                                                      TextStyle(fontSize: 14.0),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),

                                        contentPadding: EdgeInsets.all(5.0),
                                        leading: Image.network(
                                          "https://www.scsaudearacatuba.com.br/wp-content/uploads/2016/02/boleto-direto-logo.png",
                                          height: 50,
                                          width: 50,
                                        ),
                                        //subtitle:
                                        //   Text(snapshot.data.documents[index]['description']),
                                        trailing: Icon(Icons.arrow_forward_ios),
                                      ),
                                    ),
                                  ],
                                )),
                          )
                        ],
                      ))),
              Card(
                  elevation: 0,
                  child: Column(
                    children: [
                      ListTile(
                        title: Align(
                          alignment: Alignment.centerLeft,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Align(
                                  alignment: Alignment.centerRight,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Text(
                                        "Você pagará: ",
                                        style: TextStyle(fontSize: 14.0),
                                      ),
                                      Text.rich(
                                        TextSpan(
                                          text: "R\$ ${value}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w600,
                                              color: kPrimaryColor),
                                        ),
                                      )
                                    ],
                                  )),
                            ],
                          ),
                        ),

                        contentPadding: EdgeInsets.all(10.0),

                        //subtitle:
                        //   Text(snapshot.data.documents[index]['description']),
                      ),
                    ],
                  )),
            ],
          );
        }));
  }

  Future<Map<String, dynamic>> postRequest(
      String value, UserModel model) async {
    // todo - fix baseUrl
    var url = 'https://api.pagar.me/1/transactions';
    var body = json.encode({
      "api_key": "ak_test_IlxmT5nslMOJwJg5PcLRxrfZmBp5Bz",
      "amount": "${value}",
      "payment_method": "boleto",
      "customer": {
        "name": "${model.userData['name']}",
        "type": "individual",
        "country": "br",
        "email": "${model.userData['email']}",
        "documents": [
          {"type": "cpf", "number": "00000000000"}
        ],
        "phone_numbers": ["${model.userData['phone']}"],
        "birthday": "1965-01-01"
      },
      "billing": {
        "name": "StarVision",
        "address": {
          "country": "br",
          "state": "pe",
          "city": "Recife",
          "street": "Rua Rosário da Boa Vista",
          "street_number": "52",
          "zipcode": "50060100",
          "neighborhood": "Rio Cotia"
        }
      },
      "shipping": {
        "name": "${model.userData['name']}",
        "fee": 1000,
        "delivery_date": "2000-12-21",
        "expedited": true,
        "address": {
          "country": "br",
          "state": "sp",
          "city": "Cotia",
          "neighborhood": "Rio Cotia",
          "street": "Rua Matrix",
          "street_number": "9999",
          "zipcode": "06714360"
        }
      },
      "items": [
        {
          "id": "r1223",
          "title": this.product.title,
          "unit_price": this.product.price,
          "quantity": 1,
          "tangible": true
        },
        {
          "id": "b1223",
          "title": this.lens.cardTitle,
          "unit_price": this.lens.value.toString(),
          "quantity": 1,
          "tangible": true
        }
      ]
    });

    print('Body: $body');

    var response = await http.post(
      url,
      headers: {
        'Content-Type': 'application/json',
      },
      body: body,
    );

    // todo - handle non-200 status code, etc
    print(response.statusCode);
    return json.decode(response.body);
  }
}
