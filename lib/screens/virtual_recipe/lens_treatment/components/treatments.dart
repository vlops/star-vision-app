import 'package:flutter/material.dart';

class Treatment {
  String cardTitle;
  double value;
  String description;

  Treatment(this.cardTitle, this.value, this.description);
}
