import 'package:flutter/material.dart';

class Endereco {
  final String cep, rua, numero, complemento, cidade, estado;

  Endereco(
      {@required this.cep,
      @required this.rua,
      @required this.numero,
      @required this.complemento,
      @required this.cidade,
      @required this.estado});
}
