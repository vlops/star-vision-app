import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/buy_additional/components/buy_additional_form.dart';
import 'package:flutter/material.dart';
import 'package:app_shoe_shop/components/socal_card.dart';
import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/size_config.dart';
import 'package:scoped_model/scoped_model.dart';

// ignore: must_be_immutable
class Body extends StatefulWidget {
  String url;
  Product product;

  Body(this.url, this.product);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int _clientType = 2;

  Text _buildSubtitle() {
    return Text(
      "Precisamos de algumas informações para efetuar a solicitação do seu produto",
      textAlign: TextAlign.center,
    );
  }

  @override
  Widget build(BuildContext context) {
    print(widget.url);
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );
      return SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: SizeConfig.screenHeight * 0.04), // 4%
                  Text("Dados para compra", style: headingStyle),
                  _buildSubtitle(),
                  SizedBox(height: SizeConfig.screenHeight * 0.04),
                  BuyAdditionalForm(_clientType, widget.url, widget.product),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  Text(
                    'Ao continuar, você confirma que concorda \ncom nossos Termos e Condições',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.caption,
                  ),
                  SizedBox(height: getProportionateScreenHeight(20)),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
