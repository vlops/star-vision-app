import 'package:app_shoe_shop/models/Cart.dart';
import 'package:app_shoe_shop/models/Endereco.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/components/lens.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_treatment/components/treatments.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class CartCard extends StatelessWidget {
  const CartCard(
      {Key key,
      @required this.product,
      @required this.entrega,
      @required this.lens,
      @required this.treatment})
      : super(key: key);

  final Product product;
  final Lens lens;
  final Treatment treatment;
  final String entrega;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              SizedBox(
                width: 88,
                child: AspectRatio(
                  aspectRatio: 0.88,
                  child: Container(
                    padding: EdgeInsets.all(getProportionateScreenWidth(10)),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Image.network(product.images[0]),
                  ),
                ),
              ),
              SizedBox(width: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    product.title,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                  ),
                  SizedBox(height: 10),
                  Text.rich(
                    TextSpan(
                      text: "R\$${product.price}",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: kPrimaryColor),
                      // children: [
                      //   TextSpan(
                      //       text: " x1",
                      //       style: Theme.of(context).textTheme.bodyText1),
                      // ],
                    ),
                  )
                ],
              )
            ],
          ),
          Row(
            children: [
              SizedBox(
                width: 88,
                child: AspectRatio(
                  aspectRatio: 0.88,
                  child: Container(
                    padding: EdgeInsets.all(getProportionateScreenWidth(10)),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: Image.asset("assets/images/transitions.png"),
                  ),
                ),
              ),
              SizedBox(width: 20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    this.treatment.cardTitle,
                    style: TextStyle(color: Colors.black, fontSize: 16),
                    maxLines: 2,
                  ),
                  Text(
                    this.lens.cardTitle,
                    style: TextStyle(color: Colors.grey, fontSize: 13),
                    maxLines: 2,
                  ),
                  SizedBox(height: 10),
                  Text.rich(
                    TextSpan(
                      text:
                          "R\$ ${(this.lens.value + this.treatment.value).toStringAsFixed(2)}",
                      style: TextStyle(
                          fontWeight: FontWeight.w600, color: kPrimaryColor),
                    ),
                  )
                ],
              )
            ],
          ),
          buildEntrega(model)
        ],
      );
    });
  }

  Widget buildEntrega(UserModel model) {
    if (model.userData["address"][0] != null) {
      return Row(
        children: [
          SizedBox(
            width: 88,
            child: AspectRatio(
              aspectRatio: 0.88,
              child: Container(
                padding: EdgeInsets.all(getProportionateScreenWidth(10)),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Image.asset("assets/images/entrega.jpg"),
              ),
            ),
          ),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Entrega",
                style: TextStyle(color: Colors.black, fontSize: 16),
                maxLines: 2,
              ),
              Text(
                "Normal",
                style: TextStyle(color: Colors.grey, fontSize: 13),
                maxLines: 2,
              ),
              SizedBox(height: 10),
              Text.rich(
                TextSpan(
                  text: "R\$ 23.50",
                  style: TextStyle(
                      fontWeight: FontWeight.w600, color: kPrimaryColor),
                ),
              )
            ],
          )
        ],
      );
    } else {
      return Padding(
          padding: EdgeInsets.only(top: 20),
          child: Text(
            "Complete com seus dados para a Entrega",
            style: TextStyle(color: Colors.red),
          ));
    }
  }
}
