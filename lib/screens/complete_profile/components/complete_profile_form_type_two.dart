import 'package:app_shoe_shop/components/custom_surfix_icon.dart';
import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/components/form_error.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/home/home_screen2.dart';
import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../../constants.dart';
import '../../../size_config.dart';
import '../../login_success/login_success_screen.dart';

class CompleteProfileFormTypeTwo extends StatefulWidget {
  String email;
  String password;

  CompleteProfileFormTypeTwo(this.email, this.password);

  @override
  _CompleteProfileFormTypeTwoState createState() =>
      _CompleteProfileFormTypeTwoState();
}

class _CompleteProfileFormTypeTwoState
    extends State<CompleteProfileFormTypeTwo> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];
  String fullName;
  String cpf;
  String phoneNumber;

  TextEditingController _fullNameController = TextEditingController();
  TextEditingController _cpfController = TextEditingController();
  TextEditingController _phoneNumberController = TextEditingController();

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );
      return Form(
        key: _formKey,
        child: Column(
          children: [
            buildFullNameFormField(_fullNameController),
            SizedBox(height: getProportionateScreenHeight(30)),
            buildCPFFormField(_cpfController),
            SizedBox(height: getProportionateScreenHeight(30)),
            buildPhoneNumberFormField(_phoneNumberController),
            FormError(errors: errors),
            SizedBox(height: getProportionateScreenHeight(40)),
            DefaultButton(
              text: "Criar Conta",
              press: () {
                if (_formKey.currentState.validate()) {
                  Map<String, dynamic> userData = {
                    "email": widget.email,
                    "cpf": _cpfController.text,
                    "phone": _phoneNumberController.text,
                    "name": _fullNameController.text,
                    "photoUrl": "",
                    "clientType": 2
                  };
                  model.signUp(
                      userData: userData,
                      pass: widget.password,
                      context: context);
                }
              },
            ),
          ],
        ),
      );
    });
  }

  TextFormField buildPhoneNumberFormField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phoneNumber = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Celular",
          hintText: "Digite seu número de celular",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/Phone.svg"),
          labelStyle: TextStyle(color: kPrimaryColor)),
      inputFormatters: [
        MaskTextInputFormatter(
            mask: "(##)#-####-####", filter: {"#": RegExp(r'[0-9]')})
      ],
    );
  }

  TextFormField buildCPFFormField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      keyboardType: TextInputType.number,
      onSaved: (newValue) => cpf = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kCPFInvalidError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty || !CPF.isValid(value)) {
          addError(error: kCPFInvalidError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "CPF",
          hintText: "Digite seu CPF",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          labelStyle: TextStyle(color: kPrimaryColor)),
      inputFormatters: [
        MaskTextInputFormatter(
            mask: "###.###.###-##", filter: {"#": RegExp(r'[0-9]')})
      ],
    );
  }

  TextFormField buildFullNameFormField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      onSaved: (newValue) => fullName = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kNamelNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Nome",
          hintText: "Digite seu nome completo",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon: CustomSurffixIcon(svgIcon: "assets/icons/User.svg"),
          labelStyle: TextStyle(color: kPrimaryColor)),
    );
  }
}
