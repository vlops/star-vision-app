import 'package:flutter/material.dart';
import 'package:app_shoe_shop/models/Product.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class AccountImage extends StatefulWidget {
  const AccountImage({
    Key key,
  }) : super(key: key);

  @override
  _AccountImageState createState() => _AccountImageState();
}

class _AccountImageState extends State<AccountImage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: SizedBox(
            width: getProportionateScreenWidth(248),
            child: AspectRatio(
              aspectRatio: 1,
              child: Hero(
                tag: "${DateTime.now()}",
                child: Image.asset("assets/images/user_avatar.png"),
              ),
            ),
          ),
        )
      ],
    );
  }
}
