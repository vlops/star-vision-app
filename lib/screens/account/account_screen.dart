import 'package:app_shoe_shop/models/Cart.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:flutter/material.dart';

import 'components/body.dart';
import 'components/account_bottom.dart';

class AccountScreen extends StatelessWidget {
  static String routeName = "/account";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: Body(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Column(
        children: [
          Text(
            "Meu Perfil",
            style: TextStyle(color: Colors.black),
          ),
        ],
      ),
    );
  }
}
