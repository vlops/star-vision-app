import 'package:app_shoe_shop/core/const.dart';
import 'package:flutter/material.dart';

class ShoeModel {
  final String name;
  final double price;
  final String desc;
  final Color color;
  final String brand;
  final String imgPath;

  ShoeModel({
    this.name,
    this.price,
    this.desc,
    this.color,
    this.brand,
    this.imgPath,
  });

  static List<ShoeModel> list = [
    ShoeModel(
      name: "Oculos 1",
      desc:
          "Descricao",
      price: 184,
      color: AppColors.blueColor,
      brand: "Marca",
      imgPath: "glass_placeholder.png",
    ),
    ShoeModel(
      name: "Oculos 2",
      desc:
          "Descricao 2",
      price: 123,
      color: AppColors.yellowColor,
      brand: "Marca",
      imgPath: "glass_placeholder.png",
    ),
    ShoeModel(
      name: "Oculos 3",
      desc:
          "Descricao 3",
      price: 135,
      color: AppColors.redColor,
      brand: "Marca",
      imgPath: "glass_placeholder.png",
    ),
  ];
}
