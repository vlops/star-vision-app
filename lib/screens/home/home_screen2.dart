import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

import '../../size_config.dart';
import 'components/body.dart';

class HomeScreen2 extends StatelessWidget {
  static String routeName = "/home";
  List<DocumentSnapshot> list = [];
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      model.loadCurrentUser();
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );
      return Scaffold(
          key: _scaffoldKey,
          drawer: Drawer(
            child: ListView(
              children: [
                model.userData["name"] != null
                    ? UserAccountsDrawerHeader(
                        decoration: BoxDecoration(
                          color: kPrimaryColor,
                        ),
                        accountName: new Text(model.userData["name"]),
                        accountEmail: new Text(model.userData["email"]),
                        currentAccountPicture: CircleAvatar(
                          backgroundColor:
                              Theme.of(context).platform == TargetPlatform.iOS
                                  ? kPrimaryColor
                                  : Colors.white,
                          child: Text(
                            model.userData["name"][0],
                            style:
                                TextStyle(fontSize: 40.0, color: kPrimaryColor),
                          ),
                        ),
                      )
                    : UserAccountsDrawerHeader(
                        decoration: BoxDecoration(
                          color: kPrimaryColor,
                        ),
                        accountName: new Text(""),
                        accountEmail: new Text(""),
                        currentAccountPicture: CircleAvatar(
                          backgroundColor:
                              Theme.of(context).platform == TargetPlatform.iOS
                                  ? kPrimaryColor
                                  : Colors.white,
                          child: Text(
                            "",
                            style:
                                TextStyle(fontSize: 40.0, color: kPrimaryColor),
                          ),
                        ),
                      ),
                ListTile(
                  title: Text('Início'),
                  leading: Icon(Icons.home),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text('Buscar'),
                  leading: Icon(Icons.search),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text('Notificações'),
                  leading: Icon(Icons.notifications),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text('Minhas Compras'),
                  leading: Icon(Icons.shopping_bag),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text('Favoritos'),
                  leading: Icon(Icons.favorite),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text('Minha Conta'),
                  leading: Icon(Icons.account_circle),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text('Ajuda'),
                  leading: Icon(Icons.help),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text('Sobre a StarVision'),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Text('Sair'),
                  leading: Icon(Icons.exit_to_app),
                  onTap: () {
                    model.signOut(context);
                    // Update the state of the app.
                    // ...
                  },
                ),
              ],
            ),
          ),
          body: FutureBuilder<DocumentSnapshot>(
              future: Firestore.instance
                  .collection("categories")
                  .document("destaques")
                  .get(),
              builder: (context, snapshotItem) {
                if (!snapshotItem.hasData)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                else {
                  //String title = snapshotItem.data.data["glasses"].toString();
                  int qtd = snapshotItem.data.data.length;
                  for (int i = 0; i < qtd; i++) {
                    DocumentReference doc =
                        snapshotItem.data.data["glasses"][i];
                    Firestore.instance
                        .collection("glasses")
                        .document(doc.documentID)
                        .get()
                        .then((e) {
                      this.list.add(e);
                    });
                  }
                  //print("QTD-> ${this.list.length}");
                  return Body();
                }
              }));
    });
  }
}
