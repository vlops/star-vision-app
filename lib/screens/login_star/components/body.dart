import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/screens/complete_profile/components/complete_profile_form_type_one.dart';
import 'package:app_shoe_shop/screens/sign_in/components/logo_image.dart';
import 'package:app_shoe_shop/size_config.dart';
import 'package:flutter/material.dart';

import 'login_star_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.04),
                LogoImage(),
                Column(
                  children: [LoginStarForm()],
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.08),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
