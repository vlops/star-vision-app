import 'package:app_shoe_shop/models/Endereco.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';
import 'package:app_shoe_shop/components/custom_surfix_icon.dart';
import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/components/form_error.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/components/lens.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_treatment/components/treatments.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:search_cep/search_cep.dart';
//import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';

import '../../constants.dart';
import '../../size_config.dart';

// ignore: must_be_immutable
class DeliveryForm extends StatefulWidget {
  final Product product;
  final Lens lens;
  final Treatment treatment;

  DeliveryForm({Key key, this.product, this.lens, this.treatment})
      : super(key: key);
  @override
  _DeliveryFormState createState() => _DeliveryFormState();
}

class _DeliveryFormState extends State<DeliveryForm> {
  TextEditingController _cepController = TextEditingController();
  TextEditingController _stateController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _locationController = TextEditingController();
  TextEditingController _streetController = TextEditingController();
  TextEditingController _numberController = TextEditingController();
  TextEditingController _complementController = TextEditingController();
  String cep, state, city, location, street, number, complement;

  final List<String> errors = [];

  final _formKey = GlobalKey<FormState>();

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
          padding: EdgeInsets.only(left: 20.0, right: 20.0),
          child: ScopedModelDescendant<UserModel>(
              builder: (context, child, model) {
            if (model.isLoading)
              return Center(
                child: CircularProgressIndicator(),
              );
            DocumentReference doc = model.userData['address'][0];
            if (model.userData['address'] != null) {
              Firestore.instance
                  .collection("address")
                  .getDocuments()
                  .then((list) {
                list.documents.forEach((element) {
                  if (element.documentID == doc.documentID) {
                    _cepController.text = element["cep"];
                    _stateController.text = element["state"];
                    _cityController.text = element['city'];
                    _locationController.text = element['location'];
                    _streetController.text = element['street'];
                    _numberController.text = element['number'];
                    _complementController.text = element['complement'];
                  }
                });
              });
            }
            return Form(
              key: _formKey,
              child: Column(
                children: [
                  buildCEPFormField(),
                  buildStateFormField(),
                  buildCityFormField(),
                  buildLocationFormField(),
                  buildStreetFormField(),
                  buildNumberFormField(),
                  buildComplementFormField(),
                  FormError(errors: errors),
                  SizedBox(height: getProportionateScreenHeight(40)),
                  Padding(
                    padding: EdgeInsets.only(right: 10, left: 10),
                    child: DefaultButton(
                      text: model.userData['address'] == null
                          ? "Salvar Endereço"
                          : "Salvar Alterações",
                      press: () {
                        if (_formKey.currentState.validate()) {
                          Map<String, dynamic> addressData = {
                            "cep": _cepController.text,
                            "state": _stateController.text,
                            "city": _cityController.text,
                            "location": _locationController.text,
                            "street": _streetController.text,
                            "number": _numberController.text,
                            "complement": _complementController.text
                          };
                          if (model.userData['address'] == null) {
                            model.createAddress(
                                addressData: addressData,
                                context: context,
                                product: widget.product,
                                lens: widget.lens,
                                treatment: widget.treatment);
                          } else {
                            model.updateAddress(
                                doc,
                                addressData,
                                widget.product,
                                context,
                                widget.lens,
                                widget.treatment);
                          }
                          // Navigator.pushNamed(context, CartScreen.routeName,
                          //     arguments: CheckoutArguments(
                          //         product: widget.product,
                          //         urlDocument: 'urlDocument',
                          //         urlSelfie: 'widget.urlSelfie',
                          //         urlRecipe: 'urlRecipe',
                          //         entrega: "Entrega"));
                        }
                      },
                    ),
                  )
                ],
              ),
            );
          })),
    );
  }

  Widget buildCEPFormField() {
    return TextFormField(
      onSaved: (newValue) => cep = newValue,
      onChanged: (value) async {
        if (value.isNotEmpty) {
          removeError(error: kCEPNullError);
          final viaCepSearchCep = ViaCepSearchCep();
          final infoCepJSON = await viaCepSearchCep.searchInfoByCep(
            cep: value,
          );
          infoCepJSON.fold(
              (_) => null,
              (data) => {
                    _cityController.text = data.localidade,
                    _locationController.text = data.bairro,
                    _stateController.text = data.uf,
                    _complementController.text = data.complemento,
                    _streetController.text = data.logradouro
                  });
        }

        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kCEPNullError);
          return "";
        }
        return null;
      },
      controller: _cepController,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: kPrimaryColor),
          ),
          labelText: 'CEP *',
          labelStyle: TextStyle(color: Colors.grey)),
      inputFormatters: [
        MaskTextInputFormatter(
            mask: "########", filter: {"#": RegExp(r'[0-9]')})
      ],
    );
  }

  TextFormField buildStreetFormField() {
    return TextFormField(
      controller: _streetController,
      onSaved: (newValue) => street = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kLogradouroNullError);
          return "";
        }
        return null;
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kLogradouroNullError);
        }
        return null;
      },
      decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: kPrimaryColor),
          ),
          labelText: 'Logradouro (Rua, Quadra, etc..) *',
          labelStyle: TextStyle(color: Colors.grey)),
    );
  }

  TextFormField buildLocationFormField() {
    return TextFormField(
      controller: _locationController,
      onSaved: (newValue) => location = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kBairroNullError);
          return "";
        }
        return null;
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kBairroNullError);
        }
        return null;
      },
      decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: kPrimaryColor),
          ),
          labelText: 'Bairro *',
          labelStyle: TextStyle(color: Colors.grey)),
    );
  }

  TextFormField buildNumberFormField() {
    return TextFormField(
      controller: _numberController,
      onSaved: (newValue) => number = newValue,
      keyboardType: TextInputType.number,
      decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: kPrimaryColor),
          ),
          labelText: 'Número',
          labelStyle: TextStyle(color: Colors.grey)),
    );
  }

  TextFormField buildComplementFormField() {
    return TextFormField(
      onSaved: (newValue) => complement = newValue,
      controller: _complementController,
      decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: kPrimaryColor),
          ),
          labelText: 'Complemento',
          labelStyle: TextStyle(color: Colors.grey)),
    );
  }

  TextFormField buildCityFormField() {
    return TextFormField(
      controller: _cityController,
      onSaved: (newValue) => city = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kCityNullError);
          return "";
        }
        return null;
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kCityNullError);
        }
        return null;
      },
      decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: kPrimaryColor),
          ),
          labelText: 'Cidade *',
          labelStyle: TextStyle(color: Colors.grey)),
    );
  }

  TextFormField buildStateFormField() {
    return TextFormField(
      controller: _stateController,
      onSaved: (newValue) => state = newValue,
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kStateNullError);
          return "";
        }
        return null;
      },
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kStateNullError);
        }
        return null;
      },
      decoration: const InputDecoration(
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.grey),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: kPrimaryColor),
          ),
          labelText: 'Estado',
          labelStyle: TextStyle(color: Colors.grey)),
    );
  }
}
