import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/screens/complete_profile/components/complete_profile_form_type_one.dart';
import 'package:app_shoe_shop/size_config.dart';
import 'package:flutter/material.dart';

import 'complete_profile_form_type_two.dart';

class Body extends StatelessWidget {
  String email;
  String password;
  int clientType;

  Body(this.email, this.password, this.clientType);

  Widget _builForm() {
    if (clientType == 2) {
      return CompleteProfileFormTypeTwo(this.email, this.password);
    }
    return CompleteProfileFormTypeOne(this.email, this.password);
  }

  Text _buildTitle() {
    if (clientType == 2) {
      return Text("Complete seu Perfil", style: headingStyle);
    }
    return Text("Perfil da Empresa", style: headingStyle);
  }

  Text _buildSubtitle() {
    if (clientType == 2) {
      return Text(
        "Complete com seus dados",
        textAlign: TextAlign.center,
      );
    }
    return Text(
      "Complete com os dados da Empresa",
      textAlign: TextAlign.center,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                _buildTitle(),
                _buildSubtitle(),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                _builForm(),
                SizedBox(height: getProportionateScreenHeight(30)),
                Text(
                  'Ao continuar, você confirma que concorda \ncom nossos Termos e Condições',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
