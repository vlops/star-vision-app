// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:google_sign_in/google_sign_in.dart';
// import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

// class FirebaseProvider {
//   FirebaseAuth auth = FirebaseAuth.instance;

//   Future<bool> isUserLogged() async {
//     bool _retorno;
//     await FirebaseAuth.instance.authStateChanges().listen((User user) {
//       if (user == null) {
//         _retorno = false;
//       } else {
//         _retorno = true;
//       }
//     });
//     return _retorno;
//   }

//   Future<bool> registerUserEmailPassword(String email, String password) async {
//     bool bReturn = false;
//     try {
//       try {
//         UserCredential userCredential = await FirebaseAuth.instance
//             .createUserWithEmailAndPassword(
//                 email: email,
//                 password: password);

//         if (userCredential != null)
//           bReturn = true;
//       } on FirebaseAuthException catch (e) {
//         if (e.code == 'weak-password') {
//           print('The password provided is too weak.');
//         } else if (e.code == 'email-already-in-use') {
//           print('The account already exists for that email.');
//         }
//       } catch (e) {
//         print(e.toString());
//       }
//     } finally {
//       return bReturn;
//     }
//   }

//   Future<UserCredential> signInUserEmailPassword(String _email, String _password) async {
//     try {
//       UserCredential userCredential = await FirebaseAuth.instance
//           .signInWithEmailAndPassword(email: _email, password: _password);
//       return userCredential;
//     } on FirebaseAuthException catch (e) {
//       if (e.code == 'user-not-found') {
//         print('No user found for that email.');
//       } else if (e.code == 'wrong-password') {
//         print('Wrong password provided for that user.');
//       }
//     }
//   }

//   Future<UserCredential> signInWithFacebook() async {
//     // Trigger the sign-in flow
//     final LoginResult result = await FacebookAuth.instance.login();

//     // Create a credential from the access token
//     final FacebookAuthCredential facebookAuthCredential =
//     FacebookAuthProvider.credential(result.accessToken.token);

//     // Once signed in, return the UserCredential
//     return await FirebaseAuth.instance.signInWithCredential(facebookAuthCredential);
//   }

//   Future<bool> signOutFirebaseUser() async {
//     await FirebaseAuth.instance.signOut();
//     return (FirebaseAuth.instance.currentUser == null);
//   }

//   Future<UserCredential> signInWithGoogle() async {
//     if (FirebaseAuth.instance.currentUser != null) return null;
//     // Trigger the authentication flow
//     final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

//     // Obtain the auth details from the request
//     final GoogleSignInAuthentication googleAuth =
//         await googleUser.authentication;

//     // Create a new credential
//     final GoogleAuthCredential credential = GoogleAuthProvider.credential(
//       accessToken: googleAuth.accessToken,
//       idToken: googleAuth.idToken,
//     );

//     // Once signed in, return the UserCredential
//     return await FirebaseAuth.instance.signInWithCredential(credential);
//   }
// }
