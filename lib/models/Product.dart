import 'package:flutter/material.dart';

class Product {
  // final int id;
  final String title, description;
  final List<dynamic> images;
  final List<Color> colors;
  final String price, rating;
  final bool isFavourite;
  final String model;
  final String material;
  final String category;
  final String obj;
  // final double rating, price;
  // final bool isFavourite, isPopular;

  Product(
      {
      // @required this.id,
      @required this.images,
      @required this.colors,
      this.rating = "${0.0}",
      this.isFavourite = false,
      // this.isPopular = false,
      @required this.title,
      @required this.price,
      @required this.description,
      @required this.model,
      @required this.category,
      @required this.material,
      @required this.obj});
}

// Our demo Products

List<Product> demoProducts = [
  Product(
      images: [
        "assets/images/glap.png",
      ],
      colors: [
        Color(0xFFF6625E),
        Color(0xFF836DB8),
        Color(0xFFDECB9C),
        Colors.white,
      ],
      title: "Gloves XC Omega - Polygon",
      price: "${36.55}",
      description: description,
      rating: "${4.1}",
      isFavourite: true,
      model: "model 1",
      category: "category 1",
      material: "material 1"),
  Product(
      images: [
        "assets/images/wireless headset.png",
      ],
      colors: [
        Color(0xFFF6625E),
        Color(0xFF836DB8),
        Color(0xFFDECB9C),
        Colors.white,
      ],
      title: "Logitech Head",
      price: "${20.20}",
      description: description,
      rating: "${4.1}",
      isFavourite: true,
      model: "model 1",
      category: "category 1",
      material: "material 1"),
];

const String description =
    "Wireless Controller for PS4™ gives you what you want in your gaming from over precision control your games to sharing …";
