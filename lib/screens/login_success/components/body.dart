import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/screens/home/home_screen2.dart';
import 'package:flutter/material.dart';

import '../../../size_config.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: SizeConfig.screenHeight * 0.04),
        Align(
          alignment: Alignment.center,
          child: Image.network(
            "https://www.labelsxpress.com/img/products/1542808628_QUALITY-CheckMarkGreen1.5diametercircle_checkmark.jpg",
            height: SizeConfig.screenHeight * 0.4, //40%
          ),
        ),
        SizedBox(height: SizeConfig.screenHeight * 0.08),
        Text(
          "Pagamento Realizado",
          style: TextStyle(
            fontSize: getProportionateScreenWidth(30),
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        Spacer(),
        SizedBox(
          width: SizeConfig.screenWidth * 0.6,
          child: DefaultButton(
            text: "Voltar as compras",
            press: () {
              Navigator.pushNamed(context, HomeScreen2.routeName);
            },
          ),
        ),
        Spacer(),
      ],
    );
  }
}
