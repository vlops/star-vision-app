// import 'package:app_shoe_shop/Provider/firebaseProvider.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_email_sender/flutter_email_sender.dart';
// import 'package:app_shoe_shop/Provider/usersProvider.dart';
// import 'home_page.dart';
// import 'package:flutter/services.dart';
// import 'package:email_validator/email_validator.dart';
// import 'Login.dart';
// import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

// class SingUpPage extends StatefulWidget {
//   @override
//   _SingUpPageState createState() => _SingUpPageState();
// }

// class _SingUpPageState extends State<SingUpPage> {
//   final _controllerCpfCnpj = TextEditingController();
//   final _controllerEndereco = TextEditingController();
//   final _controllerTelefone = TextEditingController();
//   final _controllerEmail = TextEditingController();
//   final _controllerSenha = TextEditingController();

//   int _clientType = 1;

//   Widget _buildFormOptics() {
//     final _formKey = GlobalKey<FormState>();
//     return Container(
//         child: Form(
//       key: _formKey,
//       child: Column(
//         children: [
//           TextFormField(
//             decoration: InputDecoration(hintText: "Digite seu CNPJ"),
//             validator: (value) {
//               if (value.isEmpty) {
//                 return "Esse campo é obrigatório";
//               }
//               return null;
//             },
//             keyboardType: TextInputType.number,
//             inputFormatters: [MaskTextInputFormatter(mask: "##.###.###/####-##", filter: {"#": RegExp(r'[0-9]')})],
//             controller: _controllerCpfCnpj,
//           ),
//           TextFormField(
//             decoration:
//                 InputDecoration(hintText: "Digite seu endereço completo"),
//             validator: (value) {
//               if (value.isEmpty) {
//                 return "Esse campo é obrigatório";
//               }
//               return null;
//             },
//             controller: _controllerEndereco,
//           ),
//           TextFormField(
//             decoration: InputDecoration(hintText: "Digite seu telefone"),
//             validator: (value) {
//               if (value.isEmpty) {
//                 return "Esse campo é obrigatório";
//               }
//               return null;
//             },
//             keyboardType: TextInputType.number,
//             inputFormatters: [MaskTextInputFormatter(mask: "(##)#-####-####", filter: {"#": RegExp(r'[0-9]')})],
//             controller: _controllerTelefone,
//           ),
//           TextFormField(
//             decoration: InputDecoration(hintText: "Digite seu email"),
//             validator: (value) {
//               if (!EmailValidator.validate(value)) {
//                 return "Por favor digite um email valido";
//               }
//               return null;
//             },
//             controller: _controllerEmail,
//           ),
//           Padding(
//             padding: const EdgeInsets.symmetric(vertical: 16.0),
//             child: ElevatedButton(
//               onPressed: () async {
//                 if (_formKey.currentState.validate()) {
//                   UserOptic user = UserOptic(
//                       _clientType,
//                       _controllerCpfCnpj.text,
//                       _controllerEndereco.text,
//                       int.parse(_controllerTelefone.text),
//                       _controllerEmail.text);

//                   await UserProvider().addUser(user);

//                   showDialog(
//                       context: context,
//                       builder: (BuildContext context) {
//                         return AlertDialog(
//                           title: Text("Sucesso"),
//                           content: Column(
//                             mainAxisSize: MainAxisSize.min,
//                             children: [
//                               Center(
//                                 child: Text("Usuário adicionado com sucesso! " +
//                                     " Voce será direcionado de volta para a tela principal."),
//                               ),
//                               Center(
//                                 child: FlatButton(
//                                   child: Text("Continuar"),
//                                   onPressed: () {
//                                     Navigator.push(
//                                         context,
//                                         MaterialPageRoute(
//                                             builder: (context) =>
//                                                 LoginScreen()));
//                                   },
//                                 ),
//                               )
//                             ],
//                           ),
//                         );
//                       });
//                   // final Email email = Email(
//                   //   body: "Novo usuario cadastrado."
//                   //         " CNPJ: " + _controllerCpfCnpj.text  +
//                   //         " Endereco: " + _controllerEndereco.text +
//                   //         " Telefone: " + _controllerTelefone.text +
//                   //         " Email: " + _controllerEmail.text,
//                   //   subject: "Novo usuario ótica",
//                   //   recipients: ["jamisonduarte23@gmail.com"],
//                   //   isHTML: false,
//                   // );

//                   // await FlutterEmailSender.send(email);
//                 }
//               },
//               child: Text('Cadastrar'),
//             ),
//           ),
//         ],
//       ),
//     ));
//   }

//   Widget _buildFormFinalUser() {
//     final _formKey = GlobalKey<FormState>();
//     return Container(
//         child: Form(
//       key: _formKey,
//       child: Column(
//         children: [
//           TextFormField(
//             decoration: InputDecoration(hintText: "Digite seu CPF"),
//             validator: (value) {
//               if (value.isEmpty) {
//                 return "Esse campo é obrigatório";
//               }
//               return null;
//             },
//             keyboardType: TextInputType.number,
//             inputFormatters: [MaskTextInputFormatter(mask: "###.###.###-##", filter: {"#": RegExp(r'[0-9]')})],
//             controller: _controllerCpfCnpj,
//           ),
//           TextFormField(
//             decoration: InputDecoration(hintText: "Digite seu telefone"),
//             validator: (value) {
//               if (value.isEmpty) {
//                 return "Esse campo é obrigatório";
//               }
//               return null;
//             },
//             keyboardType: TextInputType.number,
//             inputFormatters: [MaskTextInputFormatter(mask: "(##)#-####-####", filter: {"#": RegExp(r'[0-9]')})],
//             controller: _controllerTelefone,
//           ),
//           TextFormField(
//             decoration: InputDecoration(hintText: "Digite seu email"),
//             controller: _controllerEmail,
//             validator: (value) {
//               if (!EmailValidator.validate(value)) {
//                 return "Por favor digite um email valido";
//               }
//               return null;
//             },
//           ),
//           TextFormField(
//             decoration: InputDecoration(hintText: "Digite sua senha"),
//             validator: (value) {
//               if (value.isEmpty) {
//                 return "Esse campo é obrigatório";
//               }
//               return null;
//             },
//             controller: _controllerSenha,
//             obscureText: true,
//           ),
//           Padding(
//             padding: const EdgeInsets.symmetric(vertical: 16.0),
//             child: ElevatedButton(
//               onPressed: () async {
//                 if (_formKey.currentState.validate()) {
//                   bool UserRegistered = await FirebaseProvider()
//                       .registerUserEmailPassword(
//                           _controllerEmail.text, _controllerSenha.text);

//                   if (UserRegistered) {
//                     UserCredential _userCredential = await FirebaseProvider()
//                         .signInUserEmailPassword(
//                             _controllerEmail.text, _controllerSenha.text);

//                     if (_userCredential != null) {
//                       UserOptic user = UserOptic(
//                           _clientType,
//                           _controllerCpfCnpj.text,
//                           _controllerEndereco.text,
//                           int.parse(_controllerTelefone.text),
//                           _controllerEmail.text);

//                       await UserProvider().addUser(user);

//                       showDialog(
//                           context: context,
//                           builder: (BuildContext context) {
//                             return AlertDialog(
//                               title: Text("Sucesso"),
//                               content: Column(
//                                 mainAxisSize: MainAxisSize.min,
//                                 children: [
//                                   Center(
//                                     child:
//                                         Text("Usuário cadastrado com sucesso"),
//                                   ),
//                                   Center(
//                                     child: FlatButton(
//                                       child: Text("Continuar"),
//                                       onPressed: () {
//                                         Navigator.push(
//                                             context,
//                                             MaterialPageRoute(
//                                                 builder: (context) =>
//                                                     HomePage()));
//                                       },
//                                     ),
//                                   )
//                                 ],
//                               ),
//                             );
//                           });
//                     }
//                   }
//                 }
//               },
//               child: Text('Cadastrar'),
//             ),
//           ),
//         ],
//       ),
//     ));
//   }

//   Widget _buildSignUpForm(int formType) {
//     if (formType == 1) {
//       return _buildFormOptics();
//     } else
//       return _buildFormFinalUser();
//   }

//   Widget _buildDropdownClientType() {
//     return Container(
//       child: DropdownButton(
//         value: _clientType,
//         items: [
//           DropdownMenuItem(
//             child: Text("Pessoa Jurídica"),
//             value: 1,
//           ),
//           DropdownMenuItem(
//             child: Text("Pessoa Fisica"),
//             value: 2,
//           )
//         ],
//         onChanged: (value) {
//           setState(() {
//             _clientType = value;
//           });
//         },
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Cadastro"),
//       ),
//       body: ListView(
//         children: [
//           Padding(
//             padding: const EdgeInsets.only(top: 20, bottom: 10),
//             child: Center(child: Text(" Selecione o tipo de cliente: ")),
//           ),
//           Center(child: _buildDropdownClientType()),
//           Padding(
//             padding: const EdgeInsets.all(15.0),
//             child: _buildSignUpForm(_clientType),
//           )
//         ],
//       ),
//     );
//   }
// }
