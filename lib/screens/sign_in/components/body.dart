import 'package:app_shoe_shop/components/no_account_text.dart';
import 'package:app_shoe_shop/components/socal_card.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/sign_in/components/logo_image.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import '../../../size_config.dart';
import 'sign_form.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );

      return SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: SizeConfig.screenHeight * 0.04),
                  LogoImage(),
                  Column(
                    children: [SignForm()],
                  ),
                  SizedBox(height: SizeConfig.screenHeight * 0.08),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.center,
                  //   children: [
                  //     SocalCard(
                  //       icon: "assets/icons/google-icon.svg",
                  //       press: () => {model.signInWithGoogle(context, () {})},
                  //     ),
                  //     SocalCard(
                  //       icon: "assets/icons/facebook-2.svg",
                  //       press: () => {model.signInWithFacebook(context, () {})},
                  //     ),
                  //   ],
                  // ),
                  // SizedBox(height: getProportionateScreenHeight(20)),
                  // NoAccountText(),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
