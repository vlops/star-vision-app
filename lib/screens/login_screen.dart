import 'package:app_shoe_shop/animations/FadeAnimation.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/signup_screen.dart';
import 'package:app_shoe_shop/sidebar/sidebar_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginScreen extends StatefulWidget {
  static String routeName = "/login";
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _emailController = TextEditingController();
  final _passController = TextEditingController();

  Widget _buildEditTextEmail() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey[200]))),
      child: TextFormField(
        controller: _emailController,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Email",
            hintStyle: TextStyle(color: Colors.grey)),
        validator: (text) {
          if (text.isEmpty || !text.contains("@")) return "E-mail inválido";
        },
      ),
    );
  }

  Widget _buildEditTextPassword() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller: _passController,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Senha",
            hintStyle: TextStyle(color: Colors.grey)),
        obscureText: true,
        validator: (text) {
          if (text.isEmpty || text.length < 6) return "Senha inválida";
        },
      ),
    );
  }

  Widget _buildTextFieldForgot(UserModel model) {
    return FadeAnimation(
      1.7,
      GestureDetector(
          onTap: () {
            if (_emailController.text.isEmpty) {
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content:
                    Text("Insira seu email no campo acima para recuperação"),
                backgroundColor: Colors.red,
                duration: Duration(seconds: 2),
              ));
            } else {
              model.recoverPass(_emailController.text);
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content:
                    Text("Confira as instruções de recuparação no seu email"),
                backgroundColor: Theme.of(context).primaryColor,
                duration: Duration(seconds: 2),
              ));
            }
          },
          child: Center(
              child: Text(
            "Esqueceu sua senha?",
            style: TextStyle(color: Color(0xFF73AEF5)),
          ))),
    );
  }

  Widget _buildBtnLogin(UserModel model) {
    return FadeAnimation(
        1.9,
        GestureDetector(
          onTap: () {
            // if (_formKey.currentState.validate()) {}
            model.signIn(
              email: _emailController.text,
              pass: _passController.text,
            );
          },
          child: Container(
            height: 50,
            margin: EdgeInsets.symmetric(horizontal: 60),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Color(0xFF73AEF5),
            ),
            child: Center(
              child: Text(
                "Entrar",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ));
  }

  Widget _buildIconGoogle(UserModel model) {
    return GestureDetector(
        onTap: () {
          model.signInWithGoogle(context, _onFail);
        },
        child: Container(
          // () => FirebaseProvider().signInWithGoogle(),
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(width: 2, color: Color(0xFF73AEF5)),
            shape: BoxShape.circle,
          ),
          child: SvgPicture.asset(
            'assets/logos/google-plus.svg',
            width: 20,
            height: 20,
            color: Color(0xFF73AEF5),
          ),
        ));
  }

  Widget _buildIconFacebook(UserModel model) {
    return GestureDetector(
      onTap: () {
        model.signInWithFacebook(context, _onFail);
      },
      child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(width: 2, color: Color(0xFF73AEF5)),
            shape: BoxShape.circle,
          ),
          child: SvgPicture.asset(
            'assets/logos/facebook.svg',
            width: 20,
            height: 20,
            color: Color(0xFF73AEF5),
          )),
    );
  }

  Widget _buildTextFieldCreateAccount() {
    return FadeAnimation(
        2,
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => SignUpScreen(),
            ));
          },
          child: Center(
              child: Text(
            "Criar uma conta",
            style: TextStyle(color: Color(0xFF73AEF5)),
          )),
        ));
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          if (model.isLoading)
            return Center(
              child: CircularProgressIndicator(),
            );

          return Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 300,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          top: -40,
                          height: 300,
                          width: width,
                          child: FadeAnimation(
                              1,
                              Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('assets/images/bg1.png'),
                                        fit: BoxFit.fill)),
                              )),
                        ),
                        Positioned(
                          height: 300,
                          width: width + 20,
                          child: FadeAnimation(
                              1.3,
                              Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('assets/images/bg2.png'),
                                        fit: BoxFit.fill)),
                              )),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        FadeAnimation(
                            1.5,
                            Text(
                              "Entrar",
                              style: TextStyle(
                                  color: Color(0xFF73AEF5),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30),
                            )),
                        SizedBox(
                          height: 30,
                        ),
                        FadeAnimation(
                            1.7,
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFF73AEF5),
                                      blurRadius: 20,
                                      offset: Offset(0, 10),
                                    )
                                  ]),
                              child: Column(
                                children: <Widget>[
                                  _buildEditTextEmail(),
                                  _buildEditTextPassword()
                                ],
                              ),
                            )),
                        SizedBox(
                          height: 20,
                        ),
                        _buildTextFieldForgot(model),
                        SizedBox(
                          height: 30,
                        ),
                        _buildBtnLogin(model),
                        SizedBox(
                          height: 30,
                        ),
                        FadeAnimation(
                            1.9,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                _buildIconGoogle(model),
                                SizedBox(
                                  width: 20,
                                ),
                                _buildIconFacebook(model)
                              ],
                            )),
                        SizedBox(
                          height: 30,
                        ),
                        _buildTextFieldCreateAccount(),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        }));
  }

  void _onSuccess() {
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => SideBarLayout(),
    ));
  }

  void _onFail() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao realizar login"),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 2),
    ));
  }
}
