import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/components/lens.dart';
import 'package:flutter/material.dart';

import 'components/body.dart';

class LensTreatmentScreen extends StatelessWidget {
  static String routeName = "/lens_treatment";
  String parameter = "";

  @override
  Widget build(BuildContext context) {
    final LensTreatmentArguments args =
        ModalRoute.of(context).settings.arguments;

    if (args.urlArgs == null) {
      this.parameter = "empty";
    } else {
      this.parameter = args.urlArgs;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Receita Virtual"),
      ),
      body: Body(parameter, args.product, args.lens),
    );
  }
}

class LensTreatmentArguments {
  final String urlArgs;
  final Product product;
  final Lens lens;

  LensTreatmentArguments(
      {@required this.urlArgs, @required this.product, @required this.lens});
}
