import 'package:app_shoe_shop/models/Product.dart';
import 'package:flutter/material.dart';
import 'lens_suggestion/lens_suggestion.dart';

class ProcessSuggestion {
  double _leftSliderValue;
  double _rightSliderValue;
  double _leftSliderCilValue;
  double _rightSliderCilValue;
  double _axisLeftSliderValue;
  double _axisRightSliderValue;
  double _additionSliderValue;
  double _radioEsfericValue = 0;
  BuildContext context;
  Product product;
  List<bool> recommendations = [];

  ProcessSuggestion(
      this._leftSliderValue,
      this._rightSliderValue,
      this._leftSliderCilValue,
      this._rightSliderCilValue,
      this._axisLeftSliderValue,
      this._axisRightSliderValue,
      this._additionSliderValue,
      this._radioEsfericValue,
      this.context,
      this.product);

  void show() {
    if (this._radioEsfericValue == 1) {
      //PERTO
      double res = (this._leftSliderValue + this._rightSliderValue) / 2;
      if ((res >= -12.00 && res <= -3.00) ||
          (res >= 4.25 && res <= 5.00) ||
          (res >= 7.00 && res <= 8.00)) {
        recommendations.add(true);
        recommendations.add(true);
        recommendations.add(true);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      } else if ((res >= -15.00 && res <= -13.25) ||
          (res >= -1.75 && res <= 2.00)) {
        recommendations.add(true);
        recommendations.add(false);
        recommendations.add(false);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      } else if ((res >= -13.00 && res <= -12.25) ||
          (res >= -2.75 && res <= -2.00) ||
          (res >= 2.25 && res <= 4.00) ||
          (res >= 5.25 && res <= 6.75)) {
        recommendations.add(true);
        recommendations.add(true);
        recommendations.add(false);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      }
    }
    //LONGE
    else if (this._radioEsfericValue == 2) {
      double res = (this._leftSliderValue + this._rightSliderValue) / 2;
      if ((res >= -12.00 && res <= -3.00) ||
          (res >= 4.25 && res <= 5.00) ||
          (res >= 7.00 && res <= 8.00)) {
        recommendations.add(true);
        recommendations.add(true);
        recommendations.add(true);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      } else if ((res >= -15.00 && res <= -13.25) ||
          (res >= -1.75 && res <= 2.00)) {
        recommendations.add(true);
        recommendations.add(false);
        recommendations.add(false);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      } else if ((res >= -13.00 && res <= -12.25) ||
          (res >= -2.75 && res <= -2.00) ||
          (res >= 2.25 && res <= 4.00) ||
          (res >= 5.25 && res <= 6.75)) {
        recommendations.add(true);
        recommendations.add(true);
        recommendations.add(false);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      }
    } else {
      double res = (this._leftSliderValue + this._rightSliderValue) / 2;
      if ((res >= -12.00 && res <= -3.00) ||
          (res >= 4.25 && res <= 5.00) ||
          (res >= 7.00 && res <= 8.00)) {
        recommendations.add(true);
        recommendations.add(true);
        recommendations.add(true);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      } else if ((res >= -15.00 && res <= -13.25) ||
          (res >= -1.75 && res <= 2.00)) {
        recommendations.add(true);
        recommendations.add(false);
        recommendations.add(false);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      } else if ((res >= -13.00 && res <= -12.25) ||
          (res >= -2.75 && res <= -2.00) ||
          (res >= 2.25 && res <= 4.00) ||
          (res >= 5.25 && res <= 6.75)) {
        recommendations.add(true);
        recommendations.add(true);
        recommendations.add(false);
        Navigator.pushNamed(context, LensSuggestionScreen.routeName,
            arguments: LensSuggestionArguments(
                product: this.product,
                urlArgs: 'teste',
                recommendations: recommendations));
      }
    }
  }
}
