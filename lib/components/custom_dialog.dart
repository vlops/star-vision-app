import 'dart:io';

import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/buy_additional/buy_additional_screen.dart';
import 'package:app_shoe_shop/screens/virtual_fitting_room/fitting_room.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scoped_model/scoped_model.dart';

class CustomDialog extends StatelessWidget {
  final String title, description, buttonText1, buttonText2;
  final Image image;
  final Product product;

  CustomDialog({
    @required this.title,
    @required this.description,
    @required this.buttonText1,
    @required this.buttonText2,
    @required this.product,
    this.image,
  });

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        _buildCard(context),
        _buildIcon(context),
        //...bottom card part,
        //...top circlular image part,
      ],
    );
  }

  Container _buildCard(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: 82,
        bottom: 16,
        left: 16,
        right: 16,
      ),
      margin: EdgeInsets.only(top: 66),
      decoration: new BoxDecoration(
        color: Colors.white,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10.0,
            offset: const Offset(0.0, 10.0),
          ),
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min, // To make the card compact
        children: <Widget>[
          Text(
            title,
            style: TextStyle(
              fontSize: 24.0,
              fontWeight: FontWeight.w700,
            ),
          ),
          SizedBox(height: 16.0),
          Text(
            description,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          SizedBox(height: 24.0),
          Align(
            alignment: Alignment.bottomCenter,
            child: FlatButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => UnityDemoScreen(
                    obj: product.obj,
                    product: this.product,
                  ),
                ));
              },
              // onPressed: () async {
              //   File imgFile =
              //       await ImagePicker.pickImage(source: ImageSource.camera);
              //   if (imgFile == null) return Navigator.pop(context);
              //   StorageUploadTask task = FirebaseStorage.instance
              //       .ref()
              //       .child(DateTime.now().millisecondsSinceEpoch.toString())
              //       .putFile(imgFile);
              //   StorageTaskSnapshot taskSnapshot = await task.onComplete;
              //   String url = await taskSnapshot.ref.getDownloadURL();
              //   Navigator.pushNamed(context, BuyAdditionalScreen.routeName,
              //       arguments: BuyAdditionalArguments(
              //           urlArgs: url, product: this.product));
              // },
              child: Text(buttonText1),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: FlatButton(
              // onPressed: () {
              //   Navigator.of(context).pushReplacement(MaterialPageRoute(
              //     builder: (context) => UnityDemoScreen(obj: product.obj),
              //   ));
              // },
              onPressed: () {
                Navigator.pushNamed(context, BuyAdditionalScreen.routeName,
                    arguments: BuyAdditionalArguments(
                        urlArgs: null, product: this.product));
              },
              child: Text(buttonText2),
            ),
          ),
        ],
      ),
    );
  }

  Positioned _buildIcon(BuildContext context) {
    return Positioned(
      left: 16,
      right: 16,
      child: CircleAvatar(
        backgroundColor: kPrimaryColor,
        radius: 66,
        child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(66)),
            child: Icon(
              Icons.face,
              size: 80,
              color: Colors.white,
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      model.loadCurrentUser();

      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );

      return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: dialogContent(context),
      );
    });
  }
}
