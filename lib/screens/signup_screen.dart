import 'package:app_shoe_shop/animations/FadeAnimation.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/sidebar/sidebar_layout.dart';
import 'package:app_shoe_shop/utils/mailer.dart';
import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:scoped_model/scoped_model.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _emailControllerTypeOne = TextEditingController();
  final _passControllerTypeOne = TextEditingController();
  final _passConfirmControllerTypeOne = TextEditingController();
  final _phoneControllerTypeOne = TextEditingController();
  final _cnpjControllerTypeOne = TextEditingController();
  final _addressControllerTypeOne = TextEditingController();
  final _nameControllerTypeOne = TextEditingController();

  final _emailControllerTypeTwo = TextEditingController();
  final _passControllerTypeTwo = TextEditingController();
  final _passConfirmControllerTypeTwo = TextEditingController();
  final _phoneControllerTypeTwo = TextEditingController();
  final _cpfControllerTypeTwo = TextEditingController();
  final _nameControllerTypeTwo = TextEditingController();

  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final Mailer mailer = Mailer();

  int _clientType = 2;

  Widget _buildEditTextName() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey[200]))),
      child: TextFormField(
        controller:
            _clientType == 1 ? _nameControllerTypeOne : _nameControllerTypeTwo,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: _clientType == 1 ? "Nome Fantasia" : "Nome",
            hintStyle: TextStyle(color: Colors.grey)),
        validator: (text) {
          if (text.isEmpty) return "Nome inválido";
        },
      ),
    );
  }

  Widget _buildEditTextEmail() {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: Colors.grey[200]))),
      child: TextFormField(
        controller: _clientType == 1
            ? _emailControllerTypeOne
            : _emailControllerTypeTwo,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Email",
            hintStyle: TextStyle(color: Colors.grey)),
        validator: (text) {
          if (text.isEmpty || !text.contains("@")) return "E-mail inválido";
        },
      ),
    );
  }

  Widget _buildEditTextPassword() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller:
            _clientType == 1 ? _passControllerTypeOne : _passControllerTypeTwo,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Senha",
            hintStyle: TextStyle(color: Colors.grey)),
        obscureText: true,
        validator: (text) {
          if (text.isEmpty || text.length < 6) return "Senha inválida";
          if (_clientType == 2) if (text != _passConfirmControllerTypeTwo.text)
            return "Senhas não correspondem";
          if (_clientType == 1) if (text != _passConfirmControllerTypeOne.text)
            return "Senhas não correspondem";
        },
      ),
    );
  }

  Widget _buildEditTextPasswordConfirm() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller: _clientType == 1
            ? _passConfirmControllerTypeOne
            : _passConfirmControllerTypeTwo,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Confirme sua Senha",
            hintStyle: TextStyle(color: Colors.grey)),
        obscureText: true,
        validator: (text) {
          if (text.isEmpty || text.length < 6) return "Senha inválida";
          if (_clientType == 2) if (text != _passControllerTypeTwo.text)
            return "Senhas não correspondem";
          if (_clientType == 1) if (text != _passConfirmControllerTypeOne.text)
            return "Senhas não correspondem";
        },
      ),
    );
  }

  Widget _buildEditTextCPF() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller: _cpfControllerTypeTwo,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "CPF",
            hintStyle: TextStyle(color: Colors.grey)),
        validator: (text) {
          if ((text.isEmpty) || !CPF.isValid(text)) return "CPF inválido";
        },
        inputFormatters: [
          MaskTextInputFormatter(
              mask: "###.###.###-##", filter: {"#": RegExp(r'[0-9]')})
        ],
      ),
    );
  }

  Widget _buildEditTextCNPJ() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller: _cnpjControllerTypeOne,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "CNPJ",
            hintStyle: TextStyle(color: Colors.grey)),
        validator: (text) {
          if (text.isEmpty) return "CNPJ inválido";
        },
        inputFormatters: [
          MaskTextInputFormatter(
              mask: "##.###.###/####-##", filter: {"#": RegExp(r'[0-9]')})
        ],
      ),
    );
  }

  Widget _buildEditTextPhone() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller: _clientType == 1
            ? _phoneControllerTypeOne
            : _phoneControllerTypeTwo,
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Celular",
            hintStyle: TextStyle(color: Colors.grey)),
        validator: (text) {
          if (text.isEmpty) return "Celular inválido";
        },
        inputFormatters: [
          MaskTextInputFormatter(
              mask: "(##)#-####-####", filter: {"#": RegExp(r'[0-9]')})
        ],
      ),
    );
  }

  Widget _buildEditTextAddress() {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller: _addressControllerTypeOne,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
            border: InputBorder.none,
            hintText: "Endereço",
            hintStyle: TextStyle(color: Colors.grey)),
        validator: (text) {
          if (text.isEmpty) return "Endereço Inválido";
        },
      ),
    );
  }

  Widget _buildBtnCreateAccount(UserModel model) {
    return FadeAnimation(
        1.9,
        GestureDetector(
          onTap: () {
            if (_formKey.currentState.validate()) {
              if (_clientType == 2) {
                Map<String, dynamic> userData = {
                  "email": _emailControllerTypeTwo.text,
                  "cpf": _cpfControllerTypeTwo.text,
                  "phone": _phoneControllerTypeTwo.text,
                  "name": _nameControllerTypeTwo.text,
                  "photoUrl": "",
                  "clientType": 2
                };
                model.signUp(
                    userData: userData,
                    pass: _passControllerTypeTwo.text,
                    context: context);
              } else {
                Map<String, dynamic> userData = {
                  "email": _emailControllerTypeOne.text,
                  "cnpj": _cnpjControllerTypeOne.text,
                  "phone": _phoneControllerTypeOne.text,
                  "name": _nameControllerTypeOne.text,
                  "address": _addressControllerTypeOne.text,
                  "photoUrl": "",
                  "clientType": 1
                };
                model.signUp(
                    userData: userData,
                    pass: _passControllerTypeOne.text,
                    context: context);
                mailer.sender(
                    cnpj: _cnpjControllerTypeOne.text,
                    name: _nameControllerTypeOne.text,
                    email: _emailControllerTypeOne.text,
                    phone: _phoneControllerTypeOne.text,
                    address: _addressControllerTypeOne.text);
              }
            }
          },
          child: Container(
            height: 50,
            margin: EdgeInsets.symmetric(horizontal: 60),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: Color(0xFF73AEF5),
            ),
            child: Center(
              child: Text(
                "Criar Conta",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        ));
  }

  Widget _buildIconGoogle(UserModel model) {
    return GestureDetector(
        onTap: () {
          model.signUpWithGoogle(context, _clientType);
        },
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(width: 2, color: Color(0xFF73AEF5)),
            shape: BoxShape.circle,
          ),
          child: SvgPicture.asset(
            'assets/logos/google-plus.svg',
            width: 20,
            height: 20,
            color: Color(0xFF73AEF5),
          ),
        ));
  }

  Widget _buildIconFacebook(UserModel model) {
    return GestureDetector(
      onTap: () {
        model.signUpwithFacebook(context, _clientType);
      },
      child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(
            border: Border.all(width: 2, color: Color(0xFF73AEF5)),
            shape: BoxShape.circle,
          ),
          child: SvgPicture.asset(
            'assets/logos/facebook.svg',
            width: 20,
            height: 20,
            color: Color(0xFF73AEF5),
          )),
    );
  }

  Widget _buildFormOptics() {
    return Column(children: <Widget>[
      _buildEditTextCNPJ(),
      _buildEditTextName(),
      _buildEditTextPhone(),
      _buildEditTextAddress(),
      _buildEditTextEmail(),
      _buildEditTextPassword(),
      _buildEditTextPasswordConfirm()
    ]);
  }

  Widget _buildFormUser() {
    return Column(children: <Widget>[
      _buildEditTextName(),
      _buildEditTextCPF(),
      _buildEditTextPhone(),
      _buildEditTextEmail(),
      _buildEditTextPassword(),
      _buildEditTextPasswordConfirm()
    ]);
  }

  Widget _buildSignUpForm(int formType) {
    if (formType == 1) {
      return _buildFormOptics();
    } else
      return _buildFormUser();
  }

  Widget _buildDropdownClientType() {
    return FadeAnimation(
        1.9,
        Container(
          child: DropdownButton(
            value: _clientType,
            items: [
              DropdownMenuItem(
                child: Text("Pessoa Jurídica"),
                value: 1,
              ),
              DropdownMenuItem(
                child: Text("Pessoa Fisica"),
                value: 2,
              )
            ],
            onChanged: (value) {
              setState(() {
                _clientType = value;
              });
            },
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body:
            ScopedModelDescendant<UserModel>(builder: (context, child, model) {
          if (model.isLoading)
            return Center(
              child: CircularProgressIndicator(),
            );

          return Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 100,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          height: 100,
                          width: width + 20,
                          child: FadeAnimation(
                              1.3,
                              Container(
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('assets/images/bg2.png'),
                                        fit: BoxFit.fill)),
                              )),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        FadeAnimation(
                            1.5,
                            Text(
                              "Criar Conta",
                              style: TextStyle(
                                  color: Color(0xFF73AEF5),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30),
                            )),
                        SizedBox(
                          height: 30,
                        ),
                        _buildDropdownClientType(),
                        SizedBox(
                          height: 30,
                        ),
                        FadeAnimation(
                            1.7,
                            Container(
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Color(0xFF73AEF5),
                                      blurRadius: 20,
                                      offset: Offset(0, 10),
                                    )
                                  ]),
                              child: Column(
                                children: <Widget>[
                                  _buildSignUpForm(_clientType)
                                ],
                              ),
                            )),
                        SizedBox(
                          height: 20,
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        _buildBtnCreateAccount(model),
                        SizedBox(
                          height: 30,
                        ),
                        FadeAnimation(
                            1.9,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                _clientType == 2
                                    ? _buildIconGoogle(model)
                                    : SizedBox(),
                                SizedBox(
                                  width: 20,
                                ),
                                _clientType == 2
                                    ? _buildIconFacebook(model)
                                    : SizedBox(),
                              ],
                            )),
                        SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        }));
  }

  void _onSuccess() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Usuário criado com sucesso"),
      backgroundColor: Theme.of(context).primaryColor,
      duration: Duration(seconds: 2),
    ));
    Future.delayed(Duration(seconds: 2)).then((_) => {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => SideBarLayout(),
          ))
        });
  }

  void _onFail() {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Falha ao criar usuário"),
      backgroundColor: Colors.red,
      duration: Duration(seconds: 2),
    ));
  }
}
