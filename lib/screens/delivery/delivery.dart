import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/delivery/delivery_form.dart';
import 'package:app_shoe_shop/screens/sign_up/components/sign_up_form.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/components/lens.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_treatment/components/treatments.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../theme.dart';

class DeliveryScreen extends StatelessWidget {
  static String routeName = "/delivery";
  final Product product;
  final Lens lens;
  final Treatment treatment;

  DeliveryScreen(
      {@required this.product, @required this.lens, @required this.treatment});

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        fontFamily: "Muli",
        appBarTheme: appBarTheme(),
        textTheme: textTheme(),
        // inputDecorationTheme: inputDecorationTheme(),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white70),
          backgroundColor: kPrimaryColor,
          title: Text(
            "Adicione um endereço",
            style: TextStyle(color: Colors.white70),
          ),
        ),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: DeliveryForm(
            product: this.product,
            lens: this.lens,
            treatment: this.treatment,
          ),
        ),
      ),
    );
  }
}
