import 'package:app_shoe_shop/screens/account_screen_old_old.dart';
import 'package:app_shoe_shop/screens/home_screen.dart';
import 'package:app_shoe_shop/screens/my_orders_screen.dart';
import 'package:bloc/bloc.dart';

enum NavigationEvents {
  HomePageClickedEvent,
  MyAccountClickedEvent,
  MyOrdersClickedEvent,
}

abstract class NavigationStates {}

class NavigationBloc extends Bloc<NavigationEvents, NavigationStates> {
  @override
  NavigationStates get initialState => HomeScreen();

  @override
  Stream<NavigationStates> mapEventToState(NavigationEvents event) async* {
    switch (event) {
      case NavigationEvents.HomePageClickedEvent:
        yield HomeScreen();
        break;
      case NavigationEvents.MyAccountClickedEvent:
        yield MyAccountScreen();
        break;
      case NavigationEvents.MyOrdersClickedEvent:
        yield MyOrdersScreen();
        break;
    }
  }
}
