// import 'package:app_shoe_shop/core/const.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:app_shoe_shop/models/addressModel.dart';
// import 'package:app_shoe_shop/models/productModel.dart';

// class CheckoutPage extends StatefulWidget {
//   @override
//   _CheckoutPageState createState() => _CheckoutPageState();
// }

// class _CheckoutPageState extends State<CheckoutPage> {
//   AddressModel _endereco = AddressModel(
//       streetName: "rodovia 123",
//       addressNumber: "23",
//       cityName: "Florianopolis",
//       neighborhood: "Saco grande");

//   static List<ProductModel> _listaProdutos = [
//     ProductModel(productName: "Oculos1", quantity: "2", price: "30.00"),
//     ProductModel(productName: "Oculos2", quantity: "5", price: "80.00"),
//     ProductModel(productName: "Oculos2", quantity: "5", price: "80.00"),
//     ProductModel(productName: "Oculos2", quantity: "5", price: "80.00"),
//   ];

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.white,
//       padding: EdgeInsets.only(left: 5, right: 5),
//       child: SingleChildScrollView(
//         child: Container(
//           decoration: BoxDecoration(color: AppColors.blueWhiteColor),
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.stretch,
//             children: [
//               Container(
//                   padding: EdgeInsets.only(left: 30, top: 10, bottom: 20),
//                   child: Text(
//                     "Selecione o endereço",
//                     style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//                   )),
//               Row(
//                 children: [
//                   Container(
//                     child: IconButton(
//                         icon: Icon(
//                       Icons.add_box,
//                       color: Colors.blue,
//                     )),
//                   ),
//                   Container(
//                     child: Container(
//                         padding: EdgeInsets.all(10),
//                         child: Text(" Endereço: ${_endereco.streetName},   "
//                             "Cidade: ${_endereco.cityName}, "
//                             "\n Bairro: ${_endereco.neighborhood},   "
//                             "Numero: ${_endereco.addressNumber}")),
//                     decoration: BoxDecoration(boxShadow: [
//                       BoxShadow(
//                           color: Colors.black,
//                           blurRadius: 10.0,
//                           spreadRadius: 1.0,
//                           offset: Offset(5.0, 3.0))
//                     ], color: Colors.white),
//                   ),
//                 ],
//               ),
//               Container(
//                   padding: EdgeInsets.only(left: 30, top: 30, bottom: 20),
//                   child: Text(
//                     "Produtos",
//                     style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//                   )),
//               buildProductList(_listaProdutos),
//               Container(
//                 child: Center(
//                     child: Row(
//                       children: [
//                         Padding(
//                           padding: const EdgeInsets.only(left: 8),
//                           child: Icon(Icons.monetization_on),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.only(left: 10),
//                           child: RichText(
//                             text: TextSpan(
//                                 style: TextStyle(fontSize: 16, color: Colors.black),
//                                 children: [
//                                   TextSpan(
//                                       text: "Total: ",
//                                       style:
//                                           TextStyle(fontWeight: FontWeight.bold)),
//                                   TextSpan(text: "<Total>")
//                                 ]),
//                           ),
//                         ),
//                         Spacer(),
//                         Padding(
//                           padding: const EdgeInsets.only(right: 20, top: 10),
//                           child: FlatButton(
//                             color: Colors.blue,
//                             textColor: Colors.white,
//                             disabledColor: Colors.grey,
//                             disabledTextColor: Colors.black,
//                             padding: EdgeInsets.all(8.0),
//                             splashColor: Colors.blueAccent,
//                             onPressed: () {

//                             },
//                             child: Row(
//                               children: [
//                                 Text(
//                                   "Avançar",
//                                   style: TextStyle(fontSize: 20.0),
//                                 ),
//                                 Icon(Icons.arrow_forward)
//                               ],
//                             ),
//                           ),
//                         )
//                       ],
//                 )),
//                 height: 50,
//                 decoration: BoxDecoration(boxShadow: [
//                   BoxShadow(
//                       color: Colors.black,
//                       blurRadius: 10.0,
//                       spreadRadius: 1.0,
//                       offset: Offset(5.0, 3.0))
//                 ], color: Colors.white),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }

//   Widget buildProductList(List<ProductModel> productList) {
//     return Container(
//       height: 300,
//       padding: EdgeInsets.only(right: 10, left: 10),
//       child: ListView.separated(
//           separatorBuilder: (context, index) {
//             return Divider(
//               height: 8,
//             );
//           },
//           shrinkWrap: true,
//           itemCount: productList.length,
//           itemBuilder: (BuildContext context, int index) {
//             return Container(
//               padding: EdgeInsets.all(10),
//               decoration: BoxDecoration(
//                   boxShadow: [
//                     BoxShadow(
//                         color: Colors.black,
//                         blurRadius: 1.0,
//                         spreadRadius: 1.0,
//                         offset: Offset(1.0, 0.0))
//                   ],
//                   color: AppColors.blueWhiteColor,
//                   borderRadius: BorderRadius.all(Radius.circular(8))),
//               child: Text(
//                   "${productList[index].productName}                                         "
//                   "Quantidade: ${productList[index].quantity}          "
//                   "Preco: ${productList[index].price}"),
//             );
//           }),
//     );
//   }

//   Widget _buildGridView() {
//     return Container();
//   }
// }
