import 'package:app_shoe_shop/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:app_shoe_shop/components/socal_card.dart';
import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/size_config.dart';
import 'package:scoped_model/scoped_model.dart';

import 'sign_up_form.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int _clientType = 2;

  DropdownButton _buildDropdownClientType() {
    return DropdownButton(
      value: _clientType,
      items: [
        DropdownMenuItem(
          child: Text("Pessoa Jurídica"),
          value: 1,
        ),
        DropdownMenuItem(
          child: Text("Pessoa Física"),
          value: 2,
        )
      ],
      onChanged: (value) {
        setState(() {
          _clientType = value;
        });
      },
    );
  }

  Row _buildSocial(UserModel model, BuildContext context) {
    if (_clientType == 2) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SocalCard(
            icon: "assets/icons/google-icon.svg",
            press: () {
              model.signUpWithGoogle(context, _clientType);
            },
          ),
          SocalCard(
            icon: "assets/icons/facebook-2.svg",
            press: () {
              model.signUpwithFacebook(context, _clientType);
            },
          ),
        ],
      );
    }
    return Row();
  }

  Text _buildSubtitle() {
    if (_clientType == 2) {
      return Text(
        "Complete com seus dados \nou continue com sua Rede Social",
        textAlign: TextAlign.center,
      );
    }
    return Text(
      "Complete com os dados da Empresa",
      textAlign: TextAlign.center,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );
      return SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: SizeConfig.screenHeight * 0.04), // 4%
                  Text("Crie sua Conta", style: headingStyle),
                  _buildSubtitle(),
                  SizedBox(height: SizeConfig.screenHeight * 0.01),
                  _buildDropdownClientType(),
                  SignUpForm(_clientType),
                  SizedBox(height: SizeConfig.screenHeight * 0.08),
                  _buildSocial(model, context),
                  SizedBox(height: getProportionateScreenHeight(20)),
                  Text(
                    'Ao continuar, você confirma que concorda \ncom nossos Termos e Condições',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.caption,
                  )
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
