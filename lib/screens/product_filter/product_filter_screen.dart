import 'package:app_shoe_shop/models/Cart.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import 'components/body.dart';
import 'components/check_out_card.dart';

class ProductFilterScreen extends StatefulWidget {
  static String routeName = "/product_filter";

  @override
  _ProductFilterScreenState createState() => _ProductFilterScreenState();
}

class _ProductFilterScreenState extends State<ProductFilterScreen> {
  static String routeName = "/product_filter";
  int lengthList = 0;
  int grid = 0;

  @override
  Widget build(BuildContext context) {
    final ProductFilterArguments args =
        ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: buildAppBar(context),
      body: Body(args.title, grid),
    );
  }

  Widget buildIconFilter() {
    if (grid == 1) {
      return IconButton(
        icon: Icon(Icons.grid_view),
        color: Colors.white70,
        onPressed: () {
          setState(() {
            this.grid = 0;
          });
        },
      );
    } else {
      return IconButton(
        icon: Icon(Icons.list_alt),
        color: Colors.white70,
        onPressed: () {
          setState(() {
            this.grid = 1;
          });
        },
      );
    }
  }

  AppBar buildAppBar(BuildContext context) {
    final ProductFilterArguments args =
        ModalRoute.of(context).settings.arguments;
    return AppBar(
      iconTheme: IconThemeData(
        color: Colors.white70,
      ),
      elevation: 4.0,
      backgroundColor: kPrimaryColor,
      title: Column(
        children: [
          Text(
            "${args.title}",
            style: TextStyle(color: Colors.white70),
          ),
        ],
      ),
      actions: [
        IconButton(
          icon: Icon(Icons.search),
          color: Colors.white70,
          onPressed: () {},
        ),
        IconButton(
          icon: Icon(Icons.shopping_cart),
          color: Colors.white70,
          onPressed: () {},
        ),
        //buildIconFilter(),
      ],
    );
  }
}

class ProductFilterArguments {
  final String title;

  ProductFilterArguments({@required this.title});
}
