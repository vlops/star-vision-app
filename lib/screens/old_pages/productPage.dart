// import 'package:app_shoe_shop/widgets/app_clipper.dart';
// import 'package:flutter/material.dart';
// import 'package:app_shoe_shop/core/const.dart';
// import 'package:app_shoe_shop/core/flutter_icons.dart';
// import 'package:app_shoe_shop/models/shoe_model.dart';
// import 'package:app_shoe_shop/pages/detail_page.dart';
// import 'package:app_shoe_shop/Provider/categoryProvider.dart';
// import 'package:app_shoe_shop/Provider/productProvider.dart';
// import 'dart:math' as math;

// class ProductPage extends StatefulWidget {
//   @override
//   _ProductPageState createState() => _ProductPageState();
// }

// class _ProductPageState extends State<ProductPage> {
//   var categoryList = List<CategoryOptic>();
//   var productList = List<ProductOptic>();
//   var selectedCategoryIndex = 0;

//   Future<bool> _loadCategories() async{
//     categoryList = await CategoryProvider().getAllCategories();
//     return (categoryList != null);
//   }

//   Future<bool> _loadProductsByCategories(String categoryName) async {
//     var  _productList = await ProductProvider().getProductByCategory(categoryName);
//     if (_productList != null){
//       setState(() {
//         productList = _productList;
//       });

//       return true;
//     }else {
//       return false;
//     }
//   }

//   Widget _buildTopMenu(){
//     return Padding(
//       padding: const EdgeInsets.symmetric(horizontal: 16),
//       child: Row(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: <Widget>[
//           Text(
//             "Categorias",
//             style: TextStyle(
//               fontWeight: FontWeight.bold,
//               fontSize: 32,
//             ),
//           ),
//           IconButton(
//             icon: Icon(FlutterIcons.search, color: Colors.black26),
//             onPressed: null,
//           ),
//         ],
//       ),
//     );
//   }

//   Widget _buildCategoriesMenu(){
//     return FutureBuilder(
//       future: _loadCategories(),
//       builder: (context, snapshot) {
//         if (snapshot.hasData) {
//           return Container(
//             height: 50,
//             margin: EdgeInsets.symmetric(vertical: 16),
//             child: ListView.builder(
//               itemCount: categoryList.length,
//               scrollDirection: Axis.horizontal,
//               physics: BouncingScrollPhysics(),
//               padding: EdgeInsets.symmetric(horizontal: 16),
//               itemBuilder: (context, index) {
//                 return Padding(
//                   padding: const EdgeInsets.only(right: 10),
//                   child: RaisedButton(
//                     color: Colors.blue,
//                     textColor: Colors.white,
//                     child: Text(
//                         categoryList[index].categoryName
//                     ),
//                     onPressed: () {
//                       _loadProductsByCategories(categoryList[index].categoryName);
//                       selectedCategoryIndex = index;
//                     },
//                   ),
//                 );
//               },
//             ),
//           );
//         } else {
//           return Center(
//             child: CircularProgressIndicator(),
//           );
//         }
//       }
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: _loadProductsByCategories(categoryList.length == 0 ? "Principal" : categoryList[selectedCategoryIndex].categoryName),
//       builder: (context, snapshot) {
//         if (snapshot.hasData) {
//           return ListView(
//             children: <Widget>[
//               _buildTopMenu(),
//               _buildCategoriesMenu(),
//               SizedBox(height: 16),
//               Padding(
//                 padding: EdgeInsets.symmetric(horizontal: 16),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: <Widget>[
//                     Text(
//                       "Só para você",
//                       style: TextStyle(
//                         color: Colors.black54,
//                         fontWeight: FontWeight.bold,
//                       ),
//                     ),
//                     Text(
//                       "VER TODOS",
//                       style: TextStyle(
//                         color: AppColors.greenColor,
//                         fontSize: 12,
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               SizedBox(height: 24),
//               ...productList.map((data) {
//                 return GestureDetector(
//                   onTap: () {
//                     Navigator.of(context).push(
//                       MaterialPageRoute(
//                         builder: (_) =>
//                             DetailPage(
//                               data,
//                             ),
//                       ),
//                     );
//                   },
//                   child: Container(
//                     margin: EdgeInsets.only(left: 16, right: 16, bottom: 10),
//                     padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
//                     decoration: BoxDecoration(
//                       color: Colors.white,
//                       borderRadius: BorderRadius.all(
//                         Radius.circular(25),
//                       ),
//                       boxShadow: [
//                         BoxShadow(
//                           color: Colors.black12,
//                           spreadRadius: 1,
//                           blurRadius: 10,
//                         ),
//                       ],
//                     ),
//                     child: Row(
//                       children: <Widget>[
//                         Image.network("${data.image}",
//                           width: 100,
//                           height: 60,
//                         ),
//                         SizedBox(width: 16),
//                         Expanded(
//                           child: Column(
//                             crossAxisAlignment: CrossAxisAlignment.start,
//                             children: <Widget>[
//                               Container(
//                                 width: MediaQuery
//                                     .of(context)
//                                     .size
//                                     .width * .4,
//                                 child: Text(
//                                   "${data.productName}",
//                                   maxLines: 1,
//                                   overflow: TextOverflow.ellipsis,
//                                   style: TextStyle(
//                                     fontWeight: FontWeight.bold,
//                                   ),
//                                 ),
//                               ),
//                               Text(
//                                 "${data.brand}",
//                                 style: TextStyle(
//                                   color: Colors.black26,
//                                   height: 1.5,
//                                 ),
//                               ),
//                             ],
//                           ),
//                         ),
//                         Padding(
//                           padding: const EdgeInsets.symmetric(horizontal: 12),
//                           child: Text(
//                             "\$${data.price}",
//                             style: TextStyle(
//                               fontWeight: FontWeight.bold,
//                               fontSize: 18,
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 );
//               }),
//             ],
//           );
//         } else {
//           return Center(
//             child: CircularProgressIndicator(),
//           );
//         }
//       }
//     );
//   }

//   Widget _buildBackground(int index, double width) {
//     return ClipPath(
//       clipper: AppClipper(cornerSize: 25, diagonalHeight: 100),
//       child: Container(
//         color: Colors.blue,
//         width: width,
//         child: Stack(
//           children: <Widget>[
//             Padding(
//               padding: EdgeInsets.all(16),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Padding(
//                     padding: const EdgeInsets.only(top: 20),
//                     child: Icon(
//                       productList[index].brand == "Nike"
//                           ? FlutterIcons.nike
//                           : FlutterIcons.converse,
//                       size: 30,
//                       color: Colors.white,
//                     ),
//                   ),
//                   Expanded(child: SizedBox()),
//                   Container(
//                     width: 125,
//                     child: Text(
//                       "${productList[index].productName}",
//                       style: TextStyle(
//                         color: Colors.white,
//                       ),
//                     ),
//                   ),
//                   SizedBox(height: 8),
//                   Text(
//                     "${productList[index].price}",
//                     style: TextStyle(
//                       color: Colors.white,
//                       fontWeight: FontWeight.bold,
//                     ),
//                   ),
//                   SizedBox(height: 16),
//                 ],
//               ),
//             ),
//             Positioned(
//               bottom: 0,
//               right: 0,
//               child: Container(
//                 width: 50,
//                 height: 50,
//                 decoration: BoxDecoration(
//                   color: AppColors.greenColor,
//                   borderRadius: BorderRadius.only(
//                     topLeft: Radius.circular(10),
//                   ),
//                 ),
//                 child: Center(
//                   child: Icon(
//                     FlutterIcons.add,
//                     color: Colors.white,
//                   ),
//                 ),
//               ),
//             )
//           ],
//         ),
//       ),
//     );
//   }
// }
