import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_shoe_shop/models/Product.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class ProductDescription extends StatelessWidget {
  const ProductDescription({
    Key key,
    @required this.product,
    this.pressOnSeeMore,
  }) : super(key: key);

  final Product product;
  final GestureTapCallback pressOnSeeMore;

  Widget buildImageFittingRoom() {
    return Expanded(
      flex: 2,
      child: InkWell(
        borderRadius: BorderRadius.circular(50),
        onTap: () {},
        child: Container(
          padding: EdgeInsets.all(getProportionateScreenWidth(8)),
          height: getProportionateScreenWidth(50),
          width: getProportionateScreenWidth(50),
          decoration: BoxDecoration(
            color: product.isFavourite
                ? kPrimaryColor.withOpacity(0.15)
                : kSecondaryColor.withOpacity(0.1),
            shape: BoxShape.circle,
          ),
          child: Image.asset("assets/images/logo_fitting2.png"),
        ),
      ),
    );
  }

  Widget stateProduct() {
    if (product.obj.length != 0) {
      return buildImageFittingRoom();
    }
    return Row();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Text(
            product.title,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Row(
            children: [
              Text(
                "R\$ ${product.price}",
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: kPrimaryColor,
                    fontSize: 15),
              ),
              SizedBox(width: 5),
            ],
          ),
        ),
        SizedBox(height: 5),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 8,
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                        right: getProportionateScreenWidth(0),
                      ),
                      child: Text(
                        product.description,
                        maxLines: 3,
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: EdgeInsets.only(
                        left: getProportionateScreenWidth(20),
                        right: getProportionateScreenWidth(0),
                      ),
                      child: Text(
                        "${product.model} - ${product.material}",
                        maxLines: 3,
                        style: TextStyle(fontSize: 12),
                      ),
                    ),
                  )
                ],
              ),
            ),
            stateProduct(),
          ],
        ),
      ],
    );
  }
}
