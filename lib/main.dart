import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/routes.dart';
import 'package:app_shoe_shop/screens/account/account_screen.dart';
import 'package:app_shoe_shop/screens/all_categories/all_categories.dart';
import 'package:app_shoe_shop/screens/brands/brands_screen.dart';
import 'package:app_shoe_shop/screens/buy_additional/buy_additional_screen.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';
import 'package:app_shoe_shop/screens/delivery/delivery.dart';
import 'package:app_shoe_shop/screens/details/details_screen.dart';
import 'package:app_shoe_shop/screens/forgot_password/forgot_password_screen.dart';
import 'package:app_shoe_shop/screens/home/home_screen2.dart';
import 'package:app_shoe_shop/screens/login_screen.dart';
import 'package:app_shoe_shop/screens/login_star/login_star.dart';
import 'package:app_shoe_shop/screens/login_success/login_success_screen.dart';
import 'package:app_shoe_shop/screens/product_filter/product_filter_screen.dart';
import 'package:app_shoe_shop/screens/sign_in/sign_in_screen.dart';
import 'package:app_shoe_shop/screens/sign_up/sign_up_screen.dart';
import 'package:app_shoe_shop/screens/splash/splash_screen.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/lens_suggestion.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_treatment/lens_treatment.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/recipe/virtual_recipe_screen.dart';
import 'package:app_shoe_shop/sidebar/sidebar_layout.dart';
import 'package:app_shoe_shop/size_config.dart';
import 'package:app_shoe_shop/theme.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:scoped_model/scoped_model.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
        model: UserModel(),
        child: MaterialApp(
          title: 'Star Vision',
          debugShowCheckedModeBanner: false,
          theme: theme(),
          // theme: ThemeData(
          //   primarySwatch: Colors.blue,
          //   textTheme: GoogleFonts.latoTextTheme(
          //     Theme.of(context).textTheme,
          //   ),
          // ),
          home: FutureBuilder(
              future: FirebaseAuth.instance.currentUser(),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return SplashScreen();
                else if (snapshot.data == null)
                  return SplashScreen();
                else
                  return HomeScreen2();
              }),
          //routes: routes,
          onGenerateRoute: (settings) {
            switch (settings.name) {
              case '/cart':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: CartScreen(),
                    settings: settings);

                break;
              case '/home':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: HomeScreen2(),
                    settings: settings);

                break;
              case '/splash':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: SplashScreen(),
                    settings: settings);

                break;
              case '/sign_in':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: SignInScreen(),
                    settings: settings);

                break;
              case '/forgot_password':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: ForgotPasswordScreen(),
                    settings: settings);

                break;
              case '/login':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: LoginScreen(),
                    settings: settings);

                break;
              case '/login_success':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: LoginSuccessScreen(),
                    settings: settings);

                break;
              case '/sign_up':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: SignUpScreen2(),
                    settings: settings);

                break;
              case '/complete_profile':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: CompleteProfileScreen(),
                    settings: settings);

                break;
              case '/details':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: DetailsScreen(),
                    settings: settings);

                break;
              case '/product_filter':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: ProductFilterScreen(),
                    settings: settings);

                break;
              case '/account':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: AccountScreen(),
                    settings: settings);

                break;
              case '/buy_additional':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: BuyAdditionalScreen(),
                    settings: settings);

                break;
              case '/all_categories':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: AllCategoriesScreen(),
                    settings: settings);

                break;
              case '/brands':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: BrandsScreen(),
                    settings: settings);

                break;
              case '/virtual_recipe':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: VirtualRecipeScreen(),
                    settings: settings);

                break;
              case '/lens_suggestion':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: LensSuggestionScreen(),
                    settings: settings);

                break;
              case '/lens_treatment':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: LensTreatmentScreen(),
                    settings: settings);

                break;
              case '/delivery':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: DeliveryScreen(),
                    settings: settings);

                break;
              case '/login_star':
                return PageTransition(
                    type: PageTransitionType.rightToLeftWithFade,
                    child: LoginStarScreen(),
                    settings: settings);

                break;
              default:
                return null;
            }
          },
        ));
  }
}
