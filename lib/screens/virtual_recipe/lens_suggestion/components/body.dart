import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/models/user_model.dart';
import 'package:app_shoe_shop/screens/buy_additional/components/buy_additional_form.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_suggestion/components/lens_suggestion_form.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/lens_treatment/lens_treatment.dart';
import 'package:flutter/material.dart';
import 'package:app_shoe_shop/components/socal_card.dart';
import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/size_config.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:scoped_model/scoped_model.dart';

import 'lens.dart';

// ignore: must_be_immutable
class Body extends StatefulWidget {
  String url;
  Product product;
  List<bool> recommendations;

  Body(this.url, this.product, this.recommendations);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> with TickerProviderStateMixin {
  int _clientType = 2;

  var appColors = [
    Color.fromRGBO(231, 129, 109, 1.0),
    Color.fromRGBO(99, 138, 223, 1.0),
    Color.fromRGBO(111, 194, 173, 1.0)
  ];
  var cardIndex = 0;
  ScrollController scrollController;
  var currentColor = Color.fromRGBO(231, 129, 109, 1.0);

  AnimationController animationController;
  ColorTween colorTween;
  CurvedAnimation curvedAnimation;

  var lensList = [];

  @override
  void initState() {
    super.initState();
    scrollController = new ScrollController();
    lensList = [
      Lens("Muito Fina/Leve", widget.recommendations[0], 0.00),
      Lens("Fina/Leve", widget.recommendations[1], 15.50),
      Lens("Grossa/Pesada", widget.recommendations[2], 59.00)
    ];
  }

  Row _buildSubtitle() {
    return Row(
      children: [
        Container(
            // set width equal to height to make a square
            width: 30,
            height: 30,
            child: RaisedButton(
              color: kPrimaryColor,
              shape: RoundedRectangleBorder(
                // set the value to a very big number like 100, 1000...
                borderRadius: BorderRadius.circular(100),
              ),
              child: Text(
                '1',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {},
            )),
        SizedBox(
          width: 10,
        ),
        Text("Receita", style: TextStyle(fontSize: 12)),
        SizedBox(
          width: 10,
        ),
        Container(
            // set width equal to height to make a square
            width: 30,
            height: 30,
            child: RaisedButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  // set the value to a very big number like 100, 1000...
                  borderRadius: BorderRadius.circular(100),
                  side: BorderSide(color: kPrimaryColor)),
              child: Text(
                '2',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () {},
            )),
        SizedBox(
          width: 10,
        ),
        Text("Espessura", style: TextStyle(fontSize: 12)),
        SizedBox(
          width: 10,
        ),
        Container(
            // set width equal to height to make a square
            width: 30,
            height: 30,
            child: RaisedButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  // set the value to a very big number like 100, 1000...
                  borderRadius: BorderRadius.circular(100)),
              child: Text(
                '3',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.grey),
              ),
              onPressed: () {},
            )),
        SizedBox(
          width: 10,
        ),
        Text("Tratamento", style: TextStyle(fontSize: 12)),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<UserModel>(builder: (context, child, model) {
      if (model.isLoading)
        return Center(
          child: CircularProgressIndicator(),
        );
      return SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Text("Escolha sua lente ", style: headingStyle),
                  SizedBox(height: SizeConfig.screenHeight * 0.01),
                  _buildSubtitle(),
                  SizedBox(height: SizeConfig.screenHeight * 0.06),

                  // SizedBox(height: getProportionateScreenHeight(30)),
                  new Center(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              height: 350.0,
                              child: ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: 3,
                                controller: scrollController,
                                scrollDirection: Axis.horizontal,
                                itemBuilder: (context, position) {
                                  return GestureDetector(
                                    onTap: () {
                                      Navigator.pushNamed(context,
                                          LensTreatmentScreen.routeName,
                                          arguments: LensTreatmentArguments(
                                              product: widget.product,
                                              urlArgs: 'teste',
                                              lens: lensList[position]));
                                    },
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Card(
                                        color: kPrimaryColor,
                                        child: Container(
                                          width: 250.0,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: lensList[position]
                                                              .recommended ==
                                                          true
                                                      ? Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          children: <Widget>[
                                                            Icon(
                                                              Icons.star,
                                                              color:
                                                                  Colors.amber,
                                                            ),
                                                            SizedBox(
                                                              width: 5,
                                                            ),
                                                            Text(
                                                              "Recomendado",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white),
                                                            )
                                                          ],
                                                        )
                                                      : Row()),
                                              Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Image.asset(
                                                        "assets/images/normal.png")
                                                  ]),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: <Widget>[
                                                    Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 8.0,
                                                          vertical: 4.0),
                                                      child: Text(
                                                        "+ R\$ ${lensList[position].value.toStringAsFixed(2)}",
                                                        style: TextStyle(
                                                            color:
                                                                Colors.white),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 8.0,
                                                          vertical: 4.0),
                                                      child: Text(
                                                          "${lensList[position].cardTitle}",
                                                          style: TextStyle(
                                                              fontSize: 25.0,
                                                              color: Colors
                                                                  .white)),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      // child: Text(
                                                      //   "1,74",
                                                      //   style: TextStyle(
                                                      //       color:
                                                      //           Colors.white),
                                                      // )
                                                      //     LinearProgressIndicator(
                                                      //   value:
                                                      //       cardsList[position]
                                                      //           .taskCompletion,
                                                      // ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0)),
                                      ),
                                    ),
                                    onHorizontalDragEnd: (details) {
                                      animationController = AnimationController(
                                          vsync: this,
                                          duration:
                                              Duration(milliseconds: 500));
                                      curvedAnimation = CurvedAnimation(
                                          parent: animationController,
                                          curve: Curves.fastOutSlowIn);
                                      animationController.addListener(() {
                                        setState(() {
                                          currentColor = colorTween
                                              .evaluate(curvedAnimation);
                                        });
                                      });

                                      if (details.velocity.pixelsPerSecond.dx >
                                          0) {
                                        if (cardIndex > 0) {
                                          cardIndex--;
                                          colorTween = ColorTween(
                                              begin: currentColor,
                                              end: appColors[cardIndex]);
                                        }
                                      } else {
                                        if (cardIndex < 2) {
                                          cardIndex++;
                                          colorTween = ColorTween(
                                              begin: currentColor,
                                              end: appColors[cardIndex]);
                                        }
                                      }
                                      setState(() {
                                        scrollController.animateTo(
                                            (cardIndex) * 256.0,
                                            duration:
                                                Duration(milliseconds: 500),
                                            curve: Curves.fastOutSlowIn);
                                      });

                                      colorTween.animate(curvedAnimation);

                                      animationController.forward();
                                    },
                                  );
                                },
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),

                  Card(
                      elevation: 0,
                      child: Column(
                        children: [
                          ListTile(
                            title: Align(
                              alignment: Alignment.centerLeft,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Align(
                                      alignment: Alignment.centerRight,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Text(
                                            "Armação: ",
                                            style: TextStyle(fontSize: 14.0),
                                          ),
                                          Text.rich(
                                            TextSpan(
                                              text: widget.product.title,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  color: kPrimaryColor),
                                            ),
                                          )
                                        ],
                                      )),
                                ],
                              ),
                            ),

                            contentPadding: EdgeInsets.all(10.0),

                            //subtitle:
                            //   Text(snapshot.data.documents[index]['description']),
                          ),
                        ],
                      )),

                  // Card(
                  //   elevation: 5,
                  //   child: InkWell(
                  //     splashColor: Colors.blue.withAlpha(30),
                  //     onTap: () {
                  //       Navigator.pushNamed(
                  //           context, LensTreatmentScreen.routeName,
                  //           arguments: LensTreatmentArguments(
                  //             product: widget.product,
                  //             urlArgs: 'teste',
                  //           ));
                  //     },
                  //     child: Container(
                  //         width: 500,
                  //         height: getProportionateScreenHeight(160),
                  //         child: Row(
                  //           children: [
                  //             Flexible(
                  //               flex: 3,
                  //               child: Image.asset("assets/images/normal.png"),
                  //             ),
                  //             Flexible(
                  //               flex: 7,
                  //               child: Row(
                  //                 children: [
                  //                   Flexible(
                  //                     flex: 7,
                  //                     child: Column(
                  //                       children: [
                  //                         Text(
                  //                           "Miopia\nde 0 a -2 graus",
                  //                           style: TextStyle(fontSize: 13),
                  //                         ),
                  //                         Expanded(
                  //                             child: Divider(
                  //                           color: Colors.black,
                  //                         )),
                  //                         Text("Hipermetropia\nde 0 a +2 graus",
                  //                             style: TextStyle(fontSize: 13)),
                  //                         Expanded(
                  //                             child: Divider(
                  //                           color: Colors.black,
                  //                         )),
                  //                         Text("Astigmatismo\nde 0 a -2 graus",
                  //                             style: TextStyle(fontSize: 13)),
                  //                       ],
                  //                     ),
                  //                   ),
                  //                   Flexible(
                  //                       flex: 4,
                  //                       child: Column(
                  //                         mainAxisAlignment:
                  //                             MainAxisAlignment.center,
                  //                         children: [
                  //                           SvgPicture.asset(
                  //                               "assets/icons/Star Icon.svg"),
                  //                           Text(
                  //                             "Recomendado",
                  //                             style: TextStyle(fontSize: 10),
                  //                           ),
                  //                           Text(
                  //                             "+ R\$ 99,00",
                  //                             style: TextStyle(
                  //                                 fontWeight: FontWeight.w600,
                  //                                 color: kPrimaryColor,
                  //                                 fontSize: 12),
                  //                           ),
                  //                         ],
                  //                       ))
                  //                 ],
                  //               ),
                  //             )
                  //           ],
                  //         )),
                  //   ),
                  // ),
                  // SizedBox(height: getProportionateScreenHeight(20)),
                  // Card(
                  //   elevation: 5,
                  //   child: InkWell(
                  //     splashColor: Colors.blue.withAlpha(30),
                  //     onTap: () {
                  //       Navigator.pushNamed(
                  //           context, LensTreatmentScreen.routeName,
                  //           arguments: LensTreatmentArguments(
                  //             product: widget.product,
                  //             urlArgs: 'teste',
                  //           ));
                  //     },
                  //     child: Container(
                  //         width: 500,
                  //         height: getProportionateScreenHeight(160),
                  //         child: Row(
                  //           children: [
                  //             Flexible(
                  //               flex: 3,
                  //               child: Image.asset("assets/images/fina.png"),
                  //             ),
                  //             Flexible(
                  //               flex: 7,
                  //               child: Row(
                  //                 children: [
                  //                   Flexible(
                  //                     flex: 7,
                  //                     child: Column(
                  //                       children: [
                  //                         Text("Miopia\nde 0 a -4 graus",
                  //                             style: TextStyle(fontSize: 13)),
                  //                         Expanded(
                  //                             child: Divider(
                  //                           color: Colors.black,
                  //                         )),
                  //                         Text("Hipermetropia\nde 0 a +4 graus",
                  //                             style: TextStyle(fontSize: 13)),
                  //                         Expanded(
                  //                             child: Divider(
                  //                           color: Colors.black,
                  //                         )),
                  //                         Text("Astigmatismo\nde 0 a -4 graus",
                  //                             style: TextStyle(fontSize: 13)),
                  //                       ],
                  //                     ),
                  //                   ),
                  //                   Flexible(
                  //                       flex: 4,
                  //                       child: Column(
                  //                         mainAxisAlignment:
                  //                             MainAxisAlignment.center,
                  //                         children: [
                  //                           Text(
                  //                             "+ R\$ 59,00",
                  //                             style: TextStyle(
                  //                                 fontWeight: FontWeight.w600,
                  //                                 color: kPrimaryColor,
                  //                                 fontSize: 12),
                  //                           ),
                  //                         ],
                  //                       ))
                  //                 ],
                  //               ),
                  //             )
                  //           ],
                  //         )),
                  //   ),
                  // ),
                  // SizedBox(height: getProportionateScreenHeight(20)),
                  // Card(
                  //   elevation: 5,
                  //   child: InkWell(
                  //     splashColor: Colors.blue.withAlpha(30),
                  //     onTap: () {
                  //       Navigator.pushNamed(
                  //           context, LensTreatmentScreen.routeName,
                  //           arguments: LensTreatmentArguments(
                  //             product: widget.product,
                  //             urlArgs: 'teste',
                  //           ));
                  //     },
                  //     child: Container(
                  //         width: 500,
                  //         height: getProportionateScreenHeight(160),
                  //         child: Row(
                  //           children: [
                  //             Flexible(
                  //               flex: 3,
                  //               child:
                  //                   Image.asset("assets/images/superfina.png"),
                  //             ),
                  //             Flexible(
                  //               flex: 7,
                  //               child: Row(
                  //                 children: [
                  //                   Flexible(
                  //                     flex: 7,
                  //                     child: Column(
                  //                       children: [
                  //                         Text("Miopia\nde -4 a -9 graus",
                  //                             style: TextStyle(fontSize: 13)),
                  //                         Expanded(
                  //                             child: Divider(
                  //                           color: Colors.black,
                  //                         )),
                  //                         Text("Hipermetropia\nde 0 a +6 graus",
                  //                             style: TextStyle(fontSize: 13)),
                  //                         Expanded(
                  //                             child: Divider(
                  //                           color: Colors.black,
                  //                         )),
                  //                         Text("Astigmatismo\nde 0 a -4 graus",
                  //                             style: TextStyle(fontSize: 13)),
                  //                       ],
                  //                     ),
                  //                   ),
                  //                   Flexible(
                  //                       flex: 4,
                  //                       child: Column(
                  //                         mainAxisAlignment:
                  //                             MainAxisAlignment.center,
                  //                         children: [
                  //                           Text(
                  //                             "+ R\$ 119,00",
                  //                             style: TextStyle(
                  //                                 fontWeight: FontWeight.w600,
                  //                                 color: kPrimaryColor,
                  //                                 fontSize: 12),
                  //                           ),
                  //                         ],
                  //                       ))
                  //                 ],
                  //               ),
                  //             )
                  //           ],
                  //         )),
                  //   ),
                  // ),

                  SizedBox(height: getProportionateScreenHeight(20)),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
