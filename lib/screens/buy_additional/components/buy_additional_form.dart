import 'dart:io';

import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';
import 'package:app_shoe_shop/screens/virtual_recipe/recipe/virtual_recipe_screen.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:app_shoe_shop/components/custom_surfix_icon.dart';
import 'package:app_shoe_shop/components/default_button.dart';
import 'package:app_shoe_shop/components/form_error.dart';
import 'package:image_picker/image_picker.dart';
//import 'package:app_shoe_shop/screens/complete_profile/complete_profile_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

// ignore: must_be_immutable
class BuyAdditionalForm extends StatefulWidget {
  int clientType;
  String urlSelfie;
  Product product;

  BuyAdditionalForm(this.clientType, this.urlSelfie, this.product);
  @override
  _BuyAdditionalFormState createState() => _BuyAdditionalFormState();
}

class _BuyAdditionalFormState extends State<BuyAdditionalForm> {
  final _formKey = GlobalKey<FormState>();
  final List<String> errors = [];

  String urlRecipe;
  String urlDocument;
  int recipe = 0;
  int doc = 0;

  double _result = 1;
  int _radioValue = 0;
  double _leftSliderValue = 0;
  double _rightSliderValue = 0;

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(key: _formKey, child: _statePermissionVirtualRecipe());
  }

  Widget buildRecipeFormField() {
    // return TextFormField(
    //   showCursor: false,
    //   readOnly: true,
    //   onTap: () async {
    //     File imgFile = await ImagePicker.pickImage(source: ImageSource.camera);
    //     if (imgFile == null) return Navigator.pop(context);
    //     StorageUploadTask task = FirebaseStorage.instance
    //         .ref()
    //         .child("Recipe${DateTime.now().millisecondsSinceEpoch.toString()}")
    //         .putFile(imgFile);
    //     StorageTaskSnapshot taskSnapshot = await task.onComplete;
    //     urlRecipe = await taskSnapshot.ref.getDownloadURL();
    //     removeError(error: kRecipeNullError);
    //     if (urlRecipe != null) {
    //       setState(() {
    //         this.recipe = 1;
    //       });
    //     }
    //   },
    //   onSaved: (newValue) => urlRecipe = newValue,
    //   onChanged: (value) {
    //     if (value.isNotEmpty) {
    //       removeError(error: kRecipeNullError);
    //     }
    //     return null;
    //   },
    //   validator: (value) {
    //     if (value.isEmpty) {
    //       addError(error: kRecipeNullError);
    //       return "";
    //     }
    //     return null;
    //   },
    //   decoration: InputDecoration(
    //       labelText: "Receita",
    //       hintText: "Clique para Capturar",
    //       // If  you are using latest version of flutter then lable text and hint text shown like this
    //       // if you r using flutter less then 1.20.* then maybe this is not working properly
    //       floatingLabelBehavior: FloatingLabelBehavior.always,
    //       suffixIcon:
    //           CustomSurffixIcon(svgIcon: "assets/icons/Camera Icon.svg"),
    //       labelStyle: TextStyle(color: kPrimaryColor)),
    // );
    return GestureDetector(
        onTap: () async {
          File imgFile =
              await ImagePicker.pickImage(source: ImageSource.camera);
          if (imgFile == null) return Navigator.pop(context);
          StorageUploadTask task = FirebaseStorage.instance
              .ref()
              .child(
                  "Recipe${DateTime.now().millisecondsSinceEpoch.toString()}")
              .putFile(imgFile);
          StorageTaskSnapshot taskSnapshot = await task.onComplete;
          urlRecipe = await taskSnapshot.ref.getDownloadURL();
          removeError(error: kRecipeNullError);
          if (urlRecipe != null) {
            setState(() {
              this.recipe = 1;
            });
          }
        },
        child: Center(
          child: Material(
            elevation: 10,
            borderRadius: BorderRadius.circular(8.0),
            child: InkWell(
              onTap: () async {
                File imgFile =
                    await ImagePicker.pickImage(source: ImageSource.camera);
                if (imgFile == null) return Navigator.pop(context);
                StorageUploadTask task = FirebaseStorage.instance
                    .ref()
                    .child(
                        "Recipe${DateTime.now().millisecondsSinceEpoch.toString()}")
                    .putFile(imgFile);
                StorageTaskSnapshot taskSnapshot = await task.onComplete;
                urlRecipe = await taskSnapshot.ref.getDownloadURL();
                removeError(error: kRecipeNullError);
                if (urlRecipe != null) {
                  setState(() {
                    this.recipe = 1;
                  });
                }
              },
              child: Container(
                padding: EdgeInsets.all(0.0),
                height: MediaQuery.of(context).size.width * .13,
                width: MediaQuery.of(context).size.width * .8,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Row(
                  children: <Widget>[
                    LayoutBuilder(builder: (context, constraints) {
                      print(constraints);
                      return Container(
                        height: constraints.maxHeight,
                        width: constraints.maxHeight,
                        decoration: BoxDecoration(
                          color: Colors.grey,
                          borderRadius: BorderRadius.circular(8.0),
                        ),
                        child: Icon(
                          Icons.check_box_outline_blank,
                          color: Colors.white,
                        ),
                      );
                    }),
                    Expanded(
                      child: Text(
                        'Toque para Capturar',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  TextFormField buildDocumentFormField() {
    return TextFormField(
      onTap: () async {
        File imgFile = await ImagePicker.pickImage(source: ImageSource.camera);
        if (imgFile == null) return Navigator.pop(context);
        StorageUploadTask task = FirebaseStorage.instance
            .ref()
            .child(
                "Document${DateTime.now().millisecondsSinceEpoch.toString()}")
            .putFile(imgFile);
        StorageTaskSnapshot taskSnapshot = await task.onComplete;
        urlDocument = await taskSnapshot.ref.getDownloadURL();
        removeError(error: kDocumentNullError);
        if (urlDocument != null) {
          setState(() {
            this.doc = 1;
          });
        }
      },
      onSaved: (newValue) => urlRecipe = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kDocumentNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kDocumentNullError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
          labelText: "Documento",
          hintText: "Clique para Capturar",
          // If  you are using latest version of flutter then lable text and hint text shown like this
          // if you r using flutter less then 1.20.* then maybe this is not working properly
          floatingLabelBehavior: FloatingLabelBehavior.always,
          suffixIcon:
              CustomSurffixIcon(svgIcon: "assets/icons/Camera Icon.svg"),
          labelStyle: TextStyle(color: kPrimaryColor)),
    );
  }

  Widget buildRadioRecipe() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        new Text('Receita Virtual', style: TextStyle(fontSize: 12)),
        new Radio(
          activeColor: kPrimaryColor,
          value: 0,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),
        new Text(
          'Apenas foto da Receita',
          style: TextStyle(fontSize: 12),
        ),
        new Radio(
          activeColor: kPrimaryColor,
          value: 1,
          groupValue: _radioValue,
          onChanged: _handleRadioValueChange,
        ),
      ],
    );
  }

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _result = 1;
          break;
        case 1:
          _result = 2;
          break;
      }
    });
  }

  Widget _stateRecipe() {
    if (recipe == 0) {
      return buildRecipeFormField();
    }
    return buildRecipeCheckbox();
  }

  Widget _stateVirtualRecipe() {
    if (_result == 2) {
      return _stateRecipe();
    }
    return buildVirtualRecipe();
  }

  Widget _stateDocument() {
    if (doc == 0) {
      return buildDocumentFormField();
    }
    return buildDocumentCheckbox();
  }

  // Widget _stateTextVirtualRecipe() {
  //   if (_result == 1) {
  //     return buildTextRecipe();
  //   }
  //   return Row();
  // }

  Widget _stateTextVirtualRecipe() {
    if (_result == 1) {
      return Text(
        "A receita virtual irá lhe proporcionar sugestões de lentes e orçamentos em tempo real. Com esta opção marcada, você será redirecionado para o preenchimentos dos dados da receita.",
        style: TextStyle(color: kPrimaryColor, fontSize: 13.0),
        textAlign: TextAlign.justify,
      );
    }
    return Text(
      "Ao marcar esta opção, receberemos a imagem de sua receita, e entraremos em contato para possíveis informações adicionais. Você será redirecionado para a tela de pagamento.",
      style: TextStyle(color: kPrimaryColor, fontSize: 13.0),
      textAlign: TextAlign.justify,
    );
  }

  Widget _stateSelfie() {
    if (widget.urlSelfie != "empty") return buildSelfieCheckbox();
    return Row();
  }

  Widget _statePermissionVirtualRecipe() {
    if (widget.urlSelfie == "empty") {
      return Column(
        children: [
          SizedBox(height: SizeConfig.screenHeight * 0.01),
          _stateSelfie(),
          SizedBox(height: getProportionateScreenHeight(30)),
          _stateDocument(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: "Continuar",
            press: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                if (_result == 1) {
                  Navigator.pushReplacementNamed(
                      context, VirtualRecipeScreen.routeName,
                      arguments: VirtualRecipeArguments(
                          urlArgs: "teste", product: widget.product));
                } else {
                  Navigator.pushNamed(context, CartScreen.routeName,
                      arguments: CheckoutArguments(
                          product: widget.product,
                          urlDocument: urlDocument,
                          urlSelfie: widget.urlSelfie,
                          urlRecipe: urlRecipe));
                }
              }
            },
          ),
        ],
      );
    } else {
      return Column(
        children: [
          SizedBox(height: SizeConfig.screenHeight * 0.01),
          _stateSelfie(),
          SizedBox(height: getProportionateScreenHeight(30)),

          _stateRecipe(),
          SizedBox(height: getProportionateScreenHeight(30)),

          Row(
            children: [
              Text("Selecione o tipo de Receita"),
              SizedBox(
                width: 10.0,
              ),
              Expanded(
                  child: Divider(
                color: Colors.black,
              )),
            ],
          ),
          SizedBox(height: getProportionateScreenHeight(10)),
          buildRadioRecipe(),
          SizedBox(height: getProportionateScreenHeight(30)),
          _stateTextVirtualRecipe(),
          //SizedBox(height: getProportionateScreenHeight(30)),
          //_stateDocument(),
          FormError(errors: errors),
          SizedBox(height: getProportionateScreenHeight(40)),
          DefaultButton(
            text: "Continuar",
            press: () {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                if (_result == 1) {
                  Navigator.pushReplacementNamed(
                      context, VirtualRecipeScreen.routeName,
                      arguments: VirtualRecipeArguments(
                          urlArgs: "teste", product: widget.product));
                } else {
                  Navigator.pushNamed(context, CartScreen.routeName,
                      arguments: CheckoutArguments(
                          product: widget.product,
                          urlDocument: urlDocument,
                          urlSelfie: widget.urlSelfie,
                          urlRecipe: urlRecipe));
                }
              }
            },
          ),
        ],
      );
    }
  }

  Widget buildVirtualRecipe() {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("Esquerdo"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _leftSliderValue,
                min: 0,
                max: 10,
                divisions: 100,
                label: _leftSliderValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _leftSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(_leftSliderValue.toStringAsFixed(2)),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Text("Direito"),
            ),
            Expanded(
              flex: 7,
              child: Slider(
                value: _rightSliderValue,
                min: 0,
                max: 10,
                divisions: 100,
                label: _rightSliderValue.toStringAsFixed(2),
                onChanged: (double value) {
                  setState(() {
                    _rightSliderValue = value;
                  });
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: Text(_rightSliderValue.toStringAsFixed(2)),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildTextRecipe() {
    return Row(
      children: [
        Text("Selecione o grau dos olhos"),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
            child: Divider(
          color: Colors.black,
        )),
      ],
    );
  }

  Widget buildSelfieCheckbox() {
    // return CheckboxListTile(
    //   title: Text("Selfie para medição de lentes"),
    //   secondary: Icon(Icons.face_sharp),
    //   controlAffinity: ListTileControlAffinity.platform,
    //   value: true,
    //   activeColor: Colors.green,
    //   checkColor: Colors.black,
    // );
    return Center(
      child: Material(
        elevation: 10,
        borderRadius: BorderRadius.circular(8.0),
        child: InkWell(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.all(0.0),
            height: MediaQuery.of(context).size.width * .13,
            width: MediaQuery.of(context).size.width * .8,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Row(
              children: <Widget>[
                LayoutBuilder(builder: (context, constraints) {
                  print(constraints);
                  return Container(
                    height: constraints.maxHeight,
                    width: constraints.maxHeight,
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                  );
                }),
                Expanded(
                  child: Text(
                    'Selfie para medidas óticas de suas lentes e armação',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildDocumentCheckbox() {
    return Center(
      child: Material(
        elevation: 10,
        borderRadius: BorderRadius.circular(8.0),
        child: InkWell(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.all(0.0),
            height: MediaQuery.of(context).size.width * .12,
            width: MediaQuery.of(context).size.width * .8,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Row(
              children: <Widget>[
                LayoutBuilder(builder: (context, constraints) {
                  print(constraints);
                  return Container(
                    height: constraints.maxHeight,
                    width: constraints.maxHeight,
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                  );
                }),
                Expanded(
                  child: Text(
                    'Foto do Documento',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildRecipeCheckbox() {
    return Center(
      child: Material(
        elevation: 10,
        borderRadius: BorderRadius.circular(8.0),
        child: InkWell(
          onTap: () {},
          child: Container(
            padding: EdgeInsets.all(0.0),
            height: MediaQuery.of(context).size.width * .13,
            width: MediaQuery.of(context).size.width * .8,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Row(
              children: <Widget>[
                LayoutBuilder(builder: (context, constraints) {
                  print(constraints);
                  return Container(
                    height: constraints.maxHeight,
                    width: constraints.maxHeight,
                    decoration: BoxDecoration(
                      color: kPrimaryColor,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                  );
                }),
                Expanded(
                  child: Text(
                    'Foto da Receita',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
