import 'package:app_shoe_shop/models/Product.dart';
import 'package:flutter/material.dart';

import 'components/body.dart';

class BuyAdditionalScreen extends StatelessWidget {
  static String routeName = "/buy_additional";
  String parameter = "";

  @override
  Widget build(BuildContext context) {
    final BuyAdditionalArguments args =
        ModalRoute.of(context).settings.arguments;

    if (args.urlArgs == null) {
      this.parameter = "empty";
    } else {
      this.parameter = args.urlArgs;
    }

    return Scaffold(
      appBar: AppBar(
        title: Text("Informações Adicionais"),
      ),
      body: Body(parameter, args.product),
    );
  }
}

class BuyAdditionalArguments {
  final String urlArgs;
  final Product product;

  BuyAdditionalArguments({@required this.urlArgs, @required this.product});
}
