// import 'package:app_shoe_shop/core/const.dart';
// import 'package:app_shoe_shop/core/flutter_icons.dart';
// import 'package:app_shoe_shop/pages/checkoutPage.dart';
// import 'package:app_shoe_shop/pages/productPage.dart';
// import 'package:app_shoe_shop/pages/userProfilePage.dart';
// import 'package:flutter/material.dart';

// class HomePage extends StatefulWidget {
//   @override
//   _HomePageState createState() => _HomePageState();
// }

// class _HomePageState extends State<HomePage> {
//   int _navigationIndex = 0;
//   String _tituloPagina = "Produtos";

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.transparent,
//         elevation: 0,
//         title: Text(
//           _tituloPagina,
//           style: TextStyle(color: Colors.black),
//         ),
//         leading: Icon(
//           FlutterIcons.menu,
//           color: Colors.black,
//         ),
//       ),
//       body: buildNavigationBody(context, _navigationIndex),
//       bottomNavigationBar: Container(
//         decoration: BoxDecoration(
//           borderRadius: BorderRadius.only(
//             topLeft: Radius.circular(30),
//             topRight: Radius.circular(30),
//           ),
//           color: Colors.white,
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black12,
//               spreadRadius: 1,
//               blurRadius: 10,
//             )
//           ],
//         ),
//         child: BottomNavigationBar(
//           selectedItemColor: AppColors.blueColor,
//           unselectedItemColor: Colors.black26,
//           currentIndex: _navigationIndex,
//           type: BottomNavigationBarType.fixed,
//           showSelectedLabels: false,
//           showUnselectedLabels: false,
//           backgroundColor: Colors.transparent,
//           elevation: 0,
//           onTap: navigationOnTapped,
//           items: [
//             BottomNavigationBarItem(
//               icon: Icon(FlutterIcons.list),
//               title: Text("data"),
//             ),
//             BottomNavigationBarItem(
//               icon: Icon(Icons.shopping_cart),
//               title: Text("data"),
//             ),
//             BottomNavigationBarItem(
//               icon: Icon(FlutterIcons.person_outline),
//               title: Text("data"),
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget buildNavigationBody(BuildContext context, int index) {
//     if (index == 0) {
//       return ProductPage();
//     } else if (index == 1) {
//       return CheckoutPage();
//     } else {
//       return UserProfile();
//     }
//   }

//   void navigationOnTapped(int index) {
//     if (index == 0) {
//       setTitle("Produtos");
//     } else if (index == 1) {
//       setTitle("Carrinho");
//     } else {
//       setTitle("Meus dados");
//     }

//     setState(() {
//       _navigationIndex = index;
//     });
//   }

//   void setTitle(String title) {
//     setState(() {
//       _tituloPagina = title;
//     });
//   }
// }
