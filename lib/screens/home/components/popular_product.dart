import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/components/product_card.dart';
import '../../../size_config.dart';
import 'section_title.dart';

class PopularProducts extends StatelessWidget {
  List<Product> filterListProducts = [];
  Product product;
  // List<Product> popularListProducts = [];
  // Product product;

  // Widget _buildCategoryPopular(DocumentSnapshot documentSnapshot) {
  //   // List<Product> popularListProducts = [];
  //   // Product product;
  //   return FutureBuilder<QuerySnapshot>(
  //       future: Firestore.instance
  //           .collection("products")
  //           .document(documentSnapshot.documentID)
  //           .collection("items")
  //           .getDocuments(),
  //       builder: (context, snapshotItem) {
  //         if (!snapshotItem.hasData)
  //           return Center(
  //             child: CircularProgressIndicator(),
  //           );
  //         else
  //           for (var e in snapshotItem.data.documents) {
  //             product = Product(
  //                 images: e.data['images'],
  //                 colors: [
  //                   Color(0xFFF6625E),
  //                   Color(0xFF836DB8),
  //                   Color(0xFFDECB9C),
  //                   Colors.white
  //                 ],
  //                 rating: "${0.0}",
  //                 isFavourite: false,
  //                 title: e.data['title'],
  //                 price: e.data['price'],
  //                 description: e.data['description'],
  //                 material: e.data['material'],
  //                 category: e.data['category'],
  //                 model: e.data['model']);

  //             popularListProducts.add(product);
  //           }

  //         return Row(
  //           children: [
  //             ...List.generate(
  //               popularListProducts.length,
  //               (index) {
  //                 // if (demoProducts[index].isPopular)
  //                 return ProductCard(product: popularListProducts[index]);

  //                 // return SizedBox
  //                 //     .shrink(); // here by default width and height is 0
  //               },
  //             ),
  //             SizedBox(width: getProportionateScreenWidth(20)),
  //           ],
  //         );
  //       });
  // }

  Widget _getProductForCategory(AsyncSnapshot<DocumentSnapshot> snapshotItem) {
    return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("glasses").getDocuments(),
        builder: (context, snap) {
          if (!snap.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            int qtd = snapshotItem.data.data["glasses"].length;

            for (var e in snap.data.documents) {
              for (int i = 0; i < qtd; i++) {
                DocumentReference doc = snapshotItem.data.data["glasses"][i];
                bool contain = false;
                if (e.documentID == doc.documentID) {
                  product = Product(
                      images: e.data['images'],
                      colors: [
                        Color(0xFFF6625E),
                        Color(0xFF836DB8),
                        Color(0xFFDECB9C),
                        Colors.white
                      ],
                      rating: "${0.0}",
                      isFavourite: false,
                      title: e.data['title'],
                      price: e.data['price'],
                      model: e.data['model'],
                      description: e.data['description'],
                      material: e.data['material'],
                      category: "A fazer",
                      obj: e.data['obj']);

                  for (int i = 0; i < filterListProducts.length; i++) {
                    if (product.title == filterListProducts[i].title) {
                      contain = true;
                    }
                  }
                  if (contain == false) {
                    filterListProducts.add(product);
                  }
                }
              }
            }
            if (filterListProducts.isEmpty) {
              return Center(
                child: Text("Esta categoria não possui produtos."),
              );
            }

            return Row(
              children: [
                ...List.generate(
                  filterListProducts.length,
                  (index) {
                    // if (demoProducts[index].isPopular)
                    return ProductCard(product: filterListProducts[index]);

                    // return SizedBox
                    //     .shrink(); // here by default width and height is 0
                  },
                ),
                SizedBox(width: getProportionateScreenWidth(20)),
              ],
            );
          }
        });
  }

  Widget _getCategory(String category) {
    return FutureBuilder<DocumentSnapshot>(
        future: Firestore.instance
            .collection("categories")
            .document(category)
            .get(),
        builder: (context, snapshotItem) {
          if (!snapshotItem.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            return _getProductForCategory(snapshotItem);
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("products").getDocuments(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: getProportionateScreenWidth(20)),
                  child:
                      SectionTitle(title: "Produtos em destaque", press: () {}),
                ),
                SizedBox(height: getProportionateScreenWidth(20)),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: _getCategory("destaques"),
                )
              ],
            );
          }
        });
  }
}
