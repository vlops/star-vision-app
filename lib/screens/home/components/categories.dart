import 'package:app_shoe_shop/components/product_card.dart';
import 'package:app_shoe_shop/constants.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/all_categories/all_categories.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:app_shoe_shop/screens/product_filter/product_filter_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../size_config.dart';

class Categories extends StatelessWidget {
  List<Product> popularListProducts = [];

  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> categories = [
      {"icon": "assets/icons/Flash Icon.svg", "text": "Destaques"},
      {"icon": "assets/icons/Bill Icon.svg", "text": "Outros"},
      {"icon": "assets/icons/Game Icon.svg", "text": "Promoções"},
      {"icon": "assets/icons/Gift Icon.svg", "text": "Retangular"},
      {"icon": "assets/icons/Discover.svg", "text": "Ver mais"},
    ];
    return Padding(
      padding: EdgeInsets.all(getProportionateScreenWidth(20)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: List.generate(
          categories.length,
          (index) => CategoryCard(
            icon: categories[index]["icon"],
            text: categories[index]["text"],
            press: () {},
          ),
        ),
      ),
    );
  }
}

class CategoryCard extends StatelessWidget {
  const CategoryCard({
    Key key,
    @required this.icon,
    @required this.text,
    @required this.press,
  }) : super(key: key);

  final String icon, text;
  final GestureTapCallback press;

  Widget goToCategory(
    BuildContext context,
  ) {
    List<Product> popularListProducts = [];
    return GestureDetector(
      onTap: this.text != "Ver mais"
          ? () => Navigator.pushNamed(context, ProductFilterScreen.routeName,
              arguments: ProductFilterArguments(title: this.text))
          : (() => Navigator.pushNamed(
                context,
                AllCategoriesScreen.routeName,
              )),
      child: SizedBox(
        width: getProportionateScreenWidth(55),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(getProportionateScreenWidth(15)),
              height: getProportionateScreenWidth(55),
              width: getProportionateScreenWidth(55),
              decoration: BoxDecoration(
                color: kSecondaryColor.withOpacity(0.1),
                borderRadius: BorderRadius.circular(10),
              ),
              child: this.text != "Ver mais"
                  ? SvgPicture.asset("assets/icons/glasses.svg")
                  : SvgPicture.asset(
                      "assets/icons/Plus Icon.svg",
                    ),
            ),
            SizedBox(height: 5),
            Text(
              text,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 9),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<QuerySnapshot>(
        future: Firestore.instance.collection("products").getDocuments(),
        builder: (context, snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          else {
            //return _getCategoryPopular(snapshot.data.documents[0]);
            return goToCategory(context);
          }
        });
  }
}
