import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/account/account_screen.dart';
import 'package:app_shoe_shop/screens/cart/cart_screen.dart';
import 'package:flutter/material.dart';
//import 'package:app_shoe_shop/screens/cart/cart_screen.dart';

import '../../../size_config.dart';
import 'icon_btn_with_counter.dart';
import 'search_field.dart';

class HomeHeader extends StatelessWidget {
  const HomeHeader({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final product = Product(
      images: [
        "assets/images/glap.png",
      ],
      colors: [
        Color(0xFFF6625E),
        Color(0xFF836DB8),
        Color(0xFFDECB9C),
        Colors.white,
      ],
      title: "Gloves XC Omega - Polygon",
      price: "${36.55}",
      description: description,
      rating: "${4.1}",
      isFavourite: true,
    );

    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(15)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          IconBtnWithCounter(
            svgSrc: "assets/icons/menu.svg",
            press: () => Scaffold.of(context).openDrawer(),
          ),
          SearchField(),
          IconBtnWithCounter(
            svgSrc: "assets/icons/Cart Icon.svg",
            numOfitem: 1,
            press: () {},
          ),
        ],
      ),
    );
  }
}
