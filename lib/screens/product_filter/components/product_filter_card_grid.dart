import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:app_shoe_shop/models/Product.dart';
import 'package:app_shoe_shop/screens/details/details_screen.dart';

import '../../../constants.dart';
import '../../../size_config.dart';

class ProductCardGrid extends StatelessWidget {
  const ProductCardGrid({
    Key key,
    @required this.product,
  }) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    Random random = new Random();
    return Container(
      height: 500,
      child: Card(
        semanticContainer: true,
        clipBehavior: Clip.antiAliasWithSaveLayer,
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: InkWell(
          splashColor: Colors.blue.withAlpha(30),
          onTap: () {
            Navigator.pushNamed(
              context,
              DetailsScreen.routeName,
              arguments: ProductDetailsArguments(product: product),
            );
          },
          child: Column(
            children: [
              Flexible(
                flex: 5,
                child: Stack(
                  children: <Widget>[
                    Container(
                        decoration: new BoxDecoration(color: Colors.white),
                        alignment: Alignment.center,
                        height: 240,
                        child:
                            Image.network(product.images[0], fit: BoxFit.fill)),
                    product.obj.length != 0
                        ? Align(
                            alignment: Alignment.bottomRight,
                            child: Padding(
                              padding: EdgeInsets.only(right: 10.0),
                              child: Image.asset(
                                "assets/images/logo_fitting2.png",
                                height: 25,
                                width: 25,
                              ),
                            ))
                        : Row()
                  ],
                ),
              ),
              Flexible(
                flex: 1,
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                      padding: EdgeInsets.only(left: 10.0),
                      child: Container(
                          color: Colors.blue,
                          child: Padding(
                            padding: EdgeInsets.only(left: 5.0, right: 5.0),
                            child: Text(
                              "OFERTA DO DIA",
                              style:
                                  TextStyle(fontSize: 10, color: Colors.white),
                            ),
                          ))),
                ),
              ),
              Flexible(
                flex: 4,
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "R\$ 250.00",
                            style: TextStyle(
                              fontSize: 10.0,
                              decoration: TextDecoration.lineThrough,
                            ),
                          )),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Row(
                            children: [
                              Flexible(
                                flex: 5,
                                child: Text(
                                  "R\$ ${product.price}",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: Colors.black87,
                                      fontSize: 15),
                                ),
                              ),
                              SizedBox(
                                width: 8,
                              ),
                              Flexible(
                                flex: 5,
                                child: Text(
                                  "${random.nextInt(20)}% OFF",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      color: kPrimaryColor,
                                      fontSize: 12.0),
                                ),
                              )
                            ],
                          )),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            "Frete Grátis",
                            style:
                                TextStyle(color: kPrimaryColor, fontSize: 10),
                          )),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            product.title,
                          )),
                    ),
                  ],
                ),
              )
              // Flexible(
              //   flex: 1,
              //   child: Align(
              //       alignment: Alignment.centerLeft,
              //       child: Padding(
              //           padding: EdgeInsets.only(left: 10.0),
              //           child: Container(
              //             color: Colors.blue,
              //             child: Padding(
              //               padding: EdgeInsets.only(left: 5.0, right: 5.0),
              //               child: Text(
              //                 "OFERTA DO DIA",
              //                 style:
              //                     TextStyle(fontSize: 10, color: Colors.white),
              //               ),
              //             ),
              //           ))),
              // ),
              // Flexible(
              //   flex: 1,
              //   child: Align(
              //       alignment: Alignment.centerLeft,
              //       child: Padding(
              //         padding: EdgeInsets.only(left: 10.0),
              //         child: Text(
              //           "Frete Grátis",
              //           style: TextStyle(
              //             fontSize: 10.0,
              //             decoration: TextDecoration.lineThrough,
              //           ),
              //         ),
              //       )),
              // ),
              // Flexible(
              //   flex: 1,
              //   child: Align(
              //       alignment: Alignment.centerLeft,
              //       child: Padding(
              //         padding: EdgeInsets.only(left: 10.0),
              //         child: Row(
              //           children: [
              //             Flexible(
              //               flex: 5,
              //               child: Text(
              //                 "R\$ ${product.price}",
              //                 style: TextStyle(
              //                     fontWeight: FontWeight.w600,
              //                     color: Colors.black87,
              //                     fontSize: 15),
              //               ),
              //             ),
              //             SizedBox(
              //               width: 8,
              //             ),
              //             Flexible(
              //               flex: 5,
              //               child: Text(
              //                 "${random.nextInt(20)}% OFF",
              //                 style: TextStyle(
              //                     fontWeight: FontWeight.w600,
              //                     color: kPrimaryColor,
              //                     fontSize: 12.0),
              //               ),
              //             )
              //           ],
              //         ),
              //       )),
              // ),
              // Flexible(
              //   flex: 1,
              //   child: Align(
              //       alignment: Alignment.centerLeft,
              //       child: Padding(
              //         padding: EdgeInsets.only(left: 10.0),
              //         child: Text(
              //           "Frete Grátis",
              //           style: TextStyle(color: kPrimaryColor, fontSize: 10),
              //         ),
              //       )),
              // ),
              // Flexible(
              //   flex: 1,
              //   child: Align(
              //       alignment: Alignment.topLeft,
              //       child: Padding(
              //         padding: EdgeInsets.only(left: 10.0),
              //         child: Text(
              //           product.title,
              //         ),
              //       )),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
